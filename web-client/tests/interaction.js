import { PointerInteraction, Cursor, ZoomAndPanHandler, ToolType} from "/public/js/interaction.js";
import { ActionType } from "/public/js/drawingElement.js";
import { Viewport } from "/public/js/viewport.js";

class DummyCursor
{
	constructor(size, color, cursorContainer)
	{
	}
	setPos(x,y)
	{
	}
	setRim(flag)
	{
	}
	setSize(size)
	{
	}
	setColor(color)
	{
	}
	setHintColor(color)
	{
	}
	hideHint()
	{
	}
	showHint()
	{
	}
	redraw()
	{
	}
	hide()
	{
	}
}

class DummyScreenPainter
{
	constructor(id)
	{
	}
	resize(width, height)
	{
	}
	start(whiteboardContainer, viewport)
	{
	}
	applyZoom(zoomDiff, worldX, worldY)
	{
	}
	redrawEverything(lowQuality)
	{
	}

	redrawAuxCanvas()
	{
	}

	add(drawingElement)
	{
	}

	undoWhiteboard(user_id)
	{
	}

	bufferReady()
	{
	}

	transformContext()
	{
	}    
	auxTransformContext(buffer)
	{
	}    
	eraseRec(fromX, fromY, width, height)
	{
	}
	draw_aux(el)
	{
	}
	draw(el)
	{
	}
	drawAuxCanvas()
	{
	}
	drawPenLine(fromX, fromY, toX, toY, color, thickness)
	{
	}
	drawEraserPath(points,color,thickness)
	{
	}
	drawPath(points, color, thickness, compositeOperation)
	{
	}
	drawCurve(points, controlPoints, color, thickness, compositeOperation)
	{
	}
	drawEraserLine(fromX, fromY, toX, toY, thickness)
	{
	}
	drawRec (fromX, fromY, toX, toY, color, thickness)
	{
	}
	drawCircle (fromX, fromY, radius, color, thickness)
	{
	}
	clearWhiteboard()
	{
	}
}

class DummyUser
{
	constructor(user_id, user_name, viewport, whiteboard)
	{
		this.drawings = [];
	}

	updateDrawId(draw_id)
	{
	}

	setPosition(world)
	{
	}

	eraseAroundLine(p0, p1, thickness)
	{
	}

	hideDrawings(drawings)
	{
	}

	insertDrawing(el)
	{
		this.drawings.push(el);
	}

  moveDrawings(drawings, dx,dy)
  {
		return({});
  }

	duplicateDrawings(drawings)
	{
		return({});
	}

	clear()
	{
	}

	undo()
	{
	}
}

class DummyWhiteboard
{
	constructor()
	{
		this.drawingContainer = null;
	}
}

window.debug = {logValues : function(){}};

QUnit.module("path-tool",function() {
	
	QUnit.test("simplify path 1", function(assert) {
		var cursor = new DummyCursor(1.0, "#000000", null);
		var screenPainter = new DummyScreenPainter(0,0,100,100,1.0);
		var user = new DummyUser();
		var viewport = new Viewport(50,50,100,100,1.0);
		var pointers = [];
		var whiteboard = new DummyWhiteboard();
		var zoomAndPan = new ZoomAndPanHandler(viewport, pointers, {x0 : -50, y0 : -50, x1 : 50, y1 : 50});
		//var tool = new PathTool(ActionType.curve, 1.0, cursor, screenPainter, viewport, user);
		var tool = new PointerInteraction(ActionType.curve, pointers, zoomAndPan, viewport, screenPainter, null, null, cursor, user, whiteboard);
		tool.setTool(ToolType.pen);
		var pos = {offsetX : 0, offsetY : 0, pointerId: 0, clientX : 0, clientY : 0 };
		tool.pointerDown(pos, pos, 1.0, 1.0);
		pos.clientX = 1.0;
		tool.pointerMove(pos, pos, 1.0, 1.0);
		pos.clientX = 2.0;
		tool.pointerMove(pos, pos, 1.0, 1.0);
		tool.pointerUp(pos, pos);
		// tool.commit();
		// var els = tool.getElements();
		var els = user.drawings;
		assert.equal(els[0].points.join(','), "0,0,2,0,2,0", "points");
	});
});
