import
	{ ActionType
	, DrawingElement
	, makePathElement
	, makeCircleElement
	, circleIntersectsLine
	, circleWithinRectangle
	, intersectsLine
	, intersectsLineDistance
	, intersectsRectangle
	, pointLineDistance
	, GridPartition
	, DrawingContainer
	} from "/public/js/drawingElement.js";

QUnit.module("grid-partition",function() {
	QUnit.test("line intersects line", function(assert) {
		var dr = makePathElement(ActionType.path, [[0,0],[1,1]], "#000", 1);
		assert.true(intersectsRectangle(dr,0,0,10,10), "");
		assert.true(intersectsLine([0,0],[1,1],[0,0],[0,10]), "");
		var dr2 = makePathElement(ActionType.path, [[11,0],[11,1]], "#000", 1);
		assert.true(intersectsRectangle(dr2,10,0,10,10), "");
	});
	QUnit.test("point line distance", function(assert) {
		assert.true(pointLineDistance([0,0],[2,0],[5,5]) > 1,"");
		var d = pointLineDistance([0,0],[0.5,-2],[0,4]);
		assert.true(d > 0.1 && d < 0.6,"");
	});
	QUnit.test("add one line", function(assert) {
		var grid = new GridPartition(10);
		var dr = makePathElement(ActionType.path, [[0,0],[1,1]], "#000", 1);
		grid.add({drawing : dr, hidden : false, id:0});
		assert.equal(Object.keys(grid.grid)[0],"0", 'column 0');
		assert.equal(Object.keys(grid.grid[0])[0],"0", 'row 0');
		assert.equal(grid.grid[0][0][0].id,0, 'id 0');
	});
	QUnit.test("add three lines", function(assert) {
		var grid = new GridPartition(10);
		var dr1 = makePathElement(ActionType.path, [[0,0],[1,1]], "#000", 1);
		var dr2 = makePathElement(ActionType.path, [[11,0],[11,1]], "#000", 1);
		var dr3 = makePathElement(ActionType.path, [[11,11],[11,12]], "#000", 1);
		grid.add({drawing : dr1, hidden : false, id:0});
		grid.add({drawing : dr2, hidden : false, id:1});
		grid.add({drawing : dr3, hidden : false, id:2});
		assert.equal(Object.keys(grid.grid).join(","),"0,1", 'columns');
		assert.equal(Object.keys(grid.grid[0]).join(","),"0", 'rows column 0');
		assert.equal(Object.keys(grid.grid[1]).join(","),"0,1", 'rows column 1');
		assert.equal(grid.grid[0][0][0].id,0, 'id 0');
		assert.equal(grid.grid[1][0][0].id,1, 'id 1');
		assert.equal(grid.grid[1][1][0].id,2, 'id 2');
	});
	QUnit.test("crossing border", function(assert) {
		var grid = new GridPartition(10);
		var dr1 = makePathElement(ActionType.path, [[1,1],[11,14]], "#000", 1);
		grid.add({drawing : dr1, hidden : false, id:0});
		assert.equal(Object.keys(grid.grid).join(","),"0,1", 'columns');
		assert.equal(Object.keys(grid.grid[0]).join(","),"0,1", 'column 0');
		assert.equal(Object.keys(grid.grid[1]).join(","),"1", 'column 1');
		assert.equal(grid.grid[0][0][0].id,0, 'id 0');
		assert.equal(grid.grid[0][1][0].id,0, 'id 0');
		assert.equal(grid.grid[1][1][0].id,0, 'id 0');
	});
	QUnit.test("add circles", function(assert) {
		var grid = new GridPartition(10);
		var dr1 = makeCircleElement([5,5], 1, "#000", 1);
		grid.add({drawing : dr1, hidden : false, id:0});
		assert.equal(Object.keys(grid.grid).join(","),"0", 'columns');
		assert.equal(Object.keys(grid.grid[0]).join(","),"0", 'column 0');
		assert.equal(grid.grid[0][0][0].id,0, 'id 0');
		var dr2 = makeCircleElement([5,5], 10, "#000", 1);
		grid.add({drawing : dr2, hidden : false, id:1});
		assert.equal(Object.keys(grid.grid).join(","),"0,1,-1", 'columns');
		assert.equal(Object.keys(grid.grid[-1]).join(","),"0,1,-1", 'column -1');
		assert.equal(Object.keys(grid.grid[0]).join(","),"0,1,-1", 'column 0');
		assert.equal(Object.keys(grid.grid[1]).join(","),"0,1,-1", 'column 1');
		for(var x = -1 ; x <= 1 ; ++x)
			for(var y = -1 ; y <= 1 ; ++y)
			{
				var found = false;
				for(var i = 0 ; i < grid.grid[x][y].length && !found ; ++i)
				{
					if(grid.grid[x][y][i].id === 1)
						found = true;
				}
				assert.true(found, '(' + x + ',' + y + ')');
			}
	});
	QUnit.test("circle intersects line", function(assert) {
		assert.true(circleIntersectsLine(5,5,1,[5.5,5.5],[0,2]));
		assert.true(circleIntersectsLine(5,5,1,[5.5,5.5],[2,0]));
		assert.false(circleIntersectsLine(0,0,1,[0,0],[0.1,0.1]));
	});
	QUnit.test("circle within rectangle", function(assert) {
		var grid = new GridPartition(10);
		var dr1 = makeCircleElement([5,5], 1, "#000", 1);
		grid.add({drawing : dr1, hidden : false, id:0});
		var els = grid.withinRectangle(0,0,10,10);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr1, 'Elements');
		els = grid.withinRectangle(5.5,5.5,2,2);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr1, 'Elements');
	});
	QUnit.test("line within rectangle", function(assert) {
		var grid = new GridPartition(10);
		var dr = makePathElement(ActionType.path, [[0,0],[5,5]], "#000", [1]);
		grid.add({drawing : dr, hidden : false, id:0});
		var els;
		els = grid.withinRectangle(0,0,10,10);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr, 'Elements');
		els = grid.withinRectangle(1,0,1,5);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr, 'Elements');
		els = grid.withinRectangle(5,0,1,1);
		assert.equal(els.length,0, 'Elements');
	});
	QUnit.test("line within line distance", function(assert) {
		var grid = new GridPartition(10);
		var dr = makePathElement(ActionType.path, [[0,0],[5,5]], "#000", [1]);
		grid.add({drawing : dr, hidden : false, id:0});
		var els;
		els = grid.withinLineDistance(1,0,6,5,2);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr, 'Elements');
		els = grid.withinLineDistance(2,0,-2,2,1);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr, 'Elements');
		els = grid.withinLineDistance(0,4,5,9,1);
		assert.equal(els.length,0, 'Elements');
		grid.clear();

		var dr1 = makePathElement(ActionType.path, [[0,0],[0,10]], "#000", [1]);
		grid.add({drawing : dr1, hidden : false, id:1});
		els = grid.withinLineDistance(-5,5,5,5,2);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr1, 'Elements');
	});
	QUnit.test("circle within line distance", function(assert) {
		var grid = new GridPartition(10);
		var dr = makeCircleElement([0,0], 1, "#000", 1);
		grid.add({drawing : dr, hidden : false, id:0});
		var els;
		els = grid.withinLineDistance(1,0,6,5,2);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr, 'Elements');
		els = grid.withinLineDistance(2,0,2,2,1.5);
		assert.equal(els.length,1, 'Elements');
		assert.equal(els[0],dr, 'Elements');
		els = grid.withinLineDistance(0,0,0.1,0.1,0.1);
		assert.equal(els.length,0, 'Elements');
	});
	QUnit.test("intersects line distance", function(assert) {
		var el = makePathElement(ActionType.curve,
			[ [ 271, 214 ]
			, [ 288, 270 ]
			], "black", [3]);
    var q3 = [ 271, 214 ];
    var q4 = [ 288, 270 ];
		var p0 = [202,187];
		var p1 = [203,188];
		var v = [p1[0] - p0[0], p1[1] - p0[1]];
		var vq = [q4[0] - q3[0], q4[1] - q3[1]];
		var distance = 3;
		assert.false(pointLineDistance(q3,p0,v) < 3);
		var u = [-v[1], v[0]];
		var lu = Math.sqrt(u[0]**2 + u[1]**2);
		u[0] = u[0] / lu;
		u[1] = u[1] / lu;
		var result = {};
		intersectsLine(p0,v,q4,u, result);
		assert.false(result.t < 3);
		assert.false(pointLineDistance(q4,p0,v) < 3);
		assert.false(pointLineDistance(p0,q3,vq) < 3);
		assert.false(pointLineDistance(p1,q3,vq) < 3);
		assert.false(intersectsLineDistance(makePathElement(ActionType.curve,[q3,q4],"black",[3])
		  ,p0[0],p0[1],p1[0],p1[1],v, distance));
		assert.false(intersectsLineDistance(el,p0[0],p0[1],p1[0],p1[1],v, distance));
		var el1 = makePathElement(ActionType.curve, [[0,0], [0,10]], "black", [3]);
		assert.true(intersectsLine([0,0], [0,10], [-5,5], [10,0]));
		assert.true(intersectsLineDistance(el1, -5,5,5,5,[10,0],2));
		var el2 = makePathElement(ActionType.curve, [[0,0], [10,0]], "black", [3]);
		assert.true(intersectsLine([0,0], [10,0], [5,-5], [0,10]));
		assert.true(intersectsLineDistance(el2, 5,-5,5,5,[0,10],2));
		var el3 = makePathElement(ActionType.path,
			[ [ 455, 328 ]
			, [ 455, 462 ]
			, [ 601, 462 ]
			, [ 601, 328 ]
			, [ 455, 328 ]
			], "black", [3]);
		assert.true(intersectsLineDistance(el3, 538,326,538,331,[0,5],2));
		assert.true(intersectsLine([601,328], [455-601,0], [538,326], [0,5]));
		//
		el1 = makePathElement(ActionType.path,
			[ [ 227, 219 ]
      , [ 241, 278 ]
      , [ 243, 289 ]
      , [ 243, 294 ]
      , [ 243, 294 ]
			], "black", [3]);
		var p0 = [258,295];
		var p1 = [259,294];
		var v = [p1[0] - p0[0], p1[1] - p0[1]];
		var distance = 3;
		for(var i = 0 ; i < el1.points.length - 1 ; ++i)
		{
			assert.false(intersectsLine(p0,v, el1.points[i], [el1.points[i+1][0] - el1.points[i][0], el1.points[i+1][1] - el1.points[i][1]]), "Segment " + i);
		}
		assert.false(intersectsLineDistance(el1, p0[0], p0[1], p1[0], p1[1], [p1[0] - p0[0], p1[1] - p0[1]], distance));
		//
		var th = 10.5;
		el1 = makePathElement(ActionType.path,
			[ [ 902, 336]
			, [ 889, 384]
			, [ 887, 401]
			], "black", [th]);
		p0 = [904.7344804490866,407];
		p1 = [904.4045223717983,407];
		distance = 10.5/2;
		for(var i = 0 ; i < el1.points.length - 1 ; ++i)
		{
			var v = [el1.points[i+1][0] - el1.points[i][0], el1.points[i+1][1] - el1.points[i][1]];
			assert.false(intersectsLine(p0,v, el1.points[i], v), "Segment " + i);
			assert.false(pointLineDistance(p0,el1.points[i],v) < distance + th, "Distance segment " + i);
			assert.false(pointLineDistance(p1,el1.points[i],v) < distance + th, "Distance segment " + i);
		}
		assert.false(intersectsLineDistance(el1, p0[0], p0[1], p1[0], p1[1], [p1[0] - p0[0], p1[1] - p0[1]], distance));
	});
	QUnit.test("hide within line distance", function(assert) {
		var container = new DrawingContainer();
		var el = makePathElement(ActionType.curve,
			[
				[ 261, 170 ],
				[ 262, 175 ],
				[ 266, 186 ],
				[ 271, 214 ],
				[ 288, 270 ],
				[ 291, 297 ],
				[ 301, 345 ],
				[ 301, 345 ]
			], "black", [3]);
		container.add(el);
		var p0 = [202,187];
		var p1 = [203,188];
		var distance = 3;
		var hidden = container.hideWithinLine(p0,p1,distance);
		assert.equal(hidden.length, 0, "Hidden elements");
		//
		container.clear();
		var el1 = makePathElement(ActionType.path,
			[ [ 455, 328 ]
			, [ 455, 462 ]
			, [ 601, 462 ]
			, [ 601, 328 ]
			, [ 455, 328 ]
			], "black", [3]);
		container.add(el1);
		var p0 = [538,326];
		var p1 = [538,331];
		var distance = 3;
		var hidden = container.hideWithinLine(p0,p1,distance);
		assert.equal(hidden.length, 1, "Hidden rectangle");
		assert.equal(hidden[0], el1, "Hidden rectangle");
		//
		container.clear();
		el1 = makePathElement(ActionType.path,
			[ [ 227, 219 ]
      , [ 241, 278 ]
      , [ 243, 289 ]
      , [ 243, 294 ]
      , [ 243, 294 ]
			], "black", [3]);
		container.add(el1);
		p0 = [258,295];
		p1 = [259,294];
		distance = 3;
		hidden = container.hideWithinLine(p0,p1,distance);
		assert.equal(hidden.length, 0, "No hidden path");
		//
		container.clear();
		el1 = makePathElement(ActionType.path,
			[ [ 902, 336]
			, [ 889, 384]
			, [ 887, 401]
			], "black", [10.5]);
		container.add(el1);
		p0 = [904.7344804490866,407];
		p1 = [904.4045223717983,407];
		distance = 10.5;
		hidden = container.hideWithinLine(p0,p1,distance);
		assert.equal(hidden.length, 0, "No hidden path");
	});
	QUnit.test("circle within box 2", function(assert) {
		var c0 = makeCircleElement([0,0],1,"#000",0.1);
		assert.false(circleWithinRectangle(c0,0,0,0.1,0.1), "");
		assert.true(circleWithinRectangle(c0,-2,-2,4,4), "");
		assert.true(circleWithinRectangle(c0,0.5,0.5,4,4), "");
		assert.true(circleWithinRectangle(c0,0.5,-2,4,4), "");
		assert.true(circleWithinRectangle(c0,-3.5,-2,4,4), "");
	});
});
