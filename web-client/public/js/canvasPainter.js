import {ActionType, DrawingElement} from "./drawingElement.js";

export class CanvasPainter
{
	constructor(id, canvas_classes)
	{
		// whiteboard canvas
		this.canvas = document.createElement('canvas');
		this.canvasDiv = document.createElement('div');
		if(typeof canvas_classes === 'undefined')
		{
			this.setCanvasElementDefaultAttributes(this.canvas, id);
			this.setCanvasContainerDefaultAttributes(this.canvasDiv);
		}
		else
		{
			if(typeof canvas_classes.canvas_container === 'undefined')
			{
				this.setCanvasContainerDefaultAttributes(this.canvasDiv);
			}
			else
			{
				this.canvasDiv.setAttribute('class', canvas_classes.canvas_container);
			}
			if(typeof canvas_classes.canvas_element === 'undefined')
			{
				this.setCanvasElementDefaultAttributes(this.canvas);
			}
			else
			{
				this.canvas.setAttribute('class', canvas_classes.canvas_element);
			}

		}
		this.canvasDiv.append(this.canvas);

		this.lineCap = "round";
		this.id = id;
	}

	setCanvasElementDefaultAttributes(canvasElement, id)
	{
		this.canvas.setAttribute('style', 'position: absolute; left:0px; top:0px; cursor:crosshair;');
	}

	setCanvasContainerDefaultAttributes(canvas_container)
	{
		canvas_container.setAttribute('class', 'drawing-container');
	}

	resize(width, height)
	{
		this.canvas.width = width;
		this.canvas.height = height;
	}
	start(whiteboardContainer, viewport)
	{
		whiteboardContainer.append(this.canvasDiv);
		var date = new Date();
		this.viewport = viewport;
		this.buffers = 
			[ { canvas: document.createElement('canvas')
			  , zoom: this.viewport.zoom
			  , camera: {x : this.viewport.x, y : this.viewport.y}
			  }
			, { canvas: document.createElement('canvas')
			  , zoom: this.viewport.zoom
			  , camera: {x : this.viewport.x, y : this.viewport.y}
			  }
			];
		this.current_buffer = 0;
		//this.canvas.height = window.height;
		//this.canvas.width = window.width;
		this.canvas.height = this.canvasDiv.clientHeight;
		this.canvas.width  = this.canvasDiv.clientWidth;
		this.buffer_draw_i = 0;
		this.buffer_block_size = 100;
		this.buffer_draw_time = 10;
		this.aux_canvas_factor = 1.25;
		this.buffers[0].canvas.height = this.canvas.height * this.aux_canvas_factor;
		this.buffers[1].canvas.height = this.canvas.height * this.aux_canvas_factor;
		this.buffers[0].canvas.width = this.canvas.width * this.aux_canvas_factor;
		this.buffers[1].canvas.width = this.canvas.width * this.aux_canvas_factor;
		this.ctx = this.canvas.getContext("2d");
		this.oldGCO = this.ctx.globalCompositeOperation;
		this.transformContext();
		var oldCtx = this.ctx;
		this.ctx = this.buffers[0].canvas.getContext('2d');
		this.auxTransformContext(this.buffers[0]);
		this.ctx = this.buffers[1].canvas.getContext('2d');
		this.auxTransformContext(this.buffers[1]);
		this.ctx = oldCtx;
		this.drawBuffer = [];
	}
	applyZoom(zoomDiff, worldX, worldY)
	{
		var oldZoomLog = this.viewport.zoom_log;
		this.viewport.zoom_log = this.viewport.zoom_log + zoomDiff;
		if(this.viewport.zoom_log < -3)
			this.viewport.zoom_log = -3;
		else if(this.viewport.zoom_log > 3)
			this.viewport.zoom_log = 3;
		zoomDiff = this.viewport.zoom_log - oldZoomLog;
		this.viewport.zoom = Math.pow(2, this.viewport.zoom_log);
		var zoomRatio = Math.pow(2, -zoomDiff);
		this.viewport.x = (this.viewport.x - worldX) * zoomRatio + worldX;
		this.viewport.y = (this.viewport.y - worldY) * zoomRatio + worldY;
	}
	redrawEverything(lowQuality)
	{
		this.canvas.height = this.canvas.height;
		this.ctx.setTransform(1,0,0,1,0,0);
		this.redrawAuxCanvas();
		if(this.buffer_draw_i >= this.drawBuffer.length)
		{
			this.buffer_draw_i = 0;
			var i = this.current_buffer;
			this.current_buffer = 1 - i;			
			this.buffers[i].canvas.height = this.canvas.height * this.aux_canvas_factor;
			this.buffers[i].canvas.width = this.canvas.width * this.aux_canvas_factor;
			this.buffers[i].zoom = this.viewport.zoom;
			this.buffers[i].camera = {x:this.viewport.x, y:this.viewport.y};
			var oldCtx = this.ctx;
			this.ctx = this.buffers[i].canvas.getContext('2d');
			this.ctx.setTransform(1,0,0,1,0,0);
			this.auxTransformContext(this.buffers[i]);
			this.ctx = oldCtx;
		}
		this.ctx.clearRect(0,0, this.canvas.width, this.canvas.height);
		this.drawAuxCanvas();
	}

	redrawAuxCanvas()
	{
		var oldCtx = this.ctx;
		this.ctx = this.buffers[1 - this.current_buffer].canvas.getContext('2d');
		var len = Math.min(this.drawBuffer.length, this.buffer_block_size + this.buffer_draw_i);
		var i;
		var t0 = new Date();
		var t1 = new Date();
		while(t1 - t0 < this.buffer_draw_time && this.buffer_draw_i < this.drawBuffer.length)
		{
			for(var i = 0; i < this.buffer_block_size && this.buffer_draw_i < this.drawBuffer.length ; ++i)
			{
				this.draw(this.drawBuffer[this.buffer_draw_i]);
				this.buffer_draw_i+=1
			}
			t1 = new Date();
		}
		this.ctx = oldCtx;
	}

	add(drawingElement)
	{
		this.drawBuffer.push(drawingElement);
		this.draw_aux(drawingElement);
	}

	undoWhiteboard(user_id)
	{
		for (var i = this.drawBuffer.length - 1; i >= 0; i--)
		{
			if (this.drawBuffer[i].user_id === user_id && this.drawBuffer[i].draw_id !== undefined)
			{
				var drawId = this.drawBuffer[i].draw_id;
				for (var i = this.drawBuffer.length - 1; i >= 0; i--)
				{
					if (this.drawBuffer[i].draw_id === drawId && this.drawBuffer[i].user_id === user_id)
					{
						this.drawBuffer.splice(i, 1);
					}
				}
				break;
			}
		}
		this.buffer_draw_i = 0;
		this.redrawAuxCanvas();
	}

	bufferReady()
	{
		return (this.buffer_draw_i === 0);
	}

	transformContext()
	{
		this.ctx.setTransform(1,0,0,1,0,0);
		this.ctx.translate(this.canvas.width/2, this.canvas.height/2);
		this.ctx.scale(this.viewport.zoom, this.viewport.zoom);
		this.ctx.translate(-this.viewport.x, -this.viewport.y);
	}    
	auxTransformContext(buffer)
	{
		this.ctx.setTransform(1,0,0,1,0,0);
		this.ctx.translate(buffer.canvas.width/2, buffer.canvas.height/2);
		this.ctx.scale(this.viewport.zoom, this.viewport.zoom);
		this.ctx.translate(-this.viewport.x, -this.viewport.y);
	}    
	eraseRec(fromX, fromY, width, height)
	{
		this.ctx.beginPath();
		this.ctx.rect(fromX, fromY, width, height);
		this.ctx.fillStyle = "rgba(0,0,0,1)";
		this.ctx.globalCompositeOperation = "destination-out";
		this.ctx.fill();
		this.ctx.closePath();
		this.ctx.globalCompositeOperation = this.oldGCO;
	}
	draw_aux(el)
	{
		var oldCtx = this.ctx;
		this.ctx = this.buffers[this.current_buffer].canvas.getContext('2d');
		this.draw(el);
		this.ctx = oldCtx;
	}
	draw(el)
	{
		switch(el.type)
		{
			case ActionType.eraser:
				this.drawPath(el.points, el.color, el.thickness, 'destination-out');
				break;
			case ActionType.path:
				this.drawPath(el.points, el.color, el.thickness);
				break;
			case ActionType.curve:
				this.drawCurve(el.points, el.controlPoints, el.color, el.thickness);
				break;
			case ActionType.circle:
				this.drawCircle(el.points[0][0], el.points[0][1], el.radius, el.color, el.thickness);
				break;
		}
	}
	drawAuxCanvas()
	{
		var buffer = this.buffers[this.current_buffer];
		var zoomRatio = this.viewport.zoom / buffer.zoom;
		var aux_w = buffer.canvas.width    / buffer.zoom;
		var aux_h = buffer.canvas.height   / buffer.zoom;
		this.transformContext();
		this.ctx.drawImage(buffer.canvas, Math.floor(buffer.camera.x - aux_w/2) , Math.floor(buffer.camera.y - aux_h/2), Math.floor(aux_w), Math.floor(aux_h));
	}
	drawPenLine(fromX, fromY, toX, toY, color, thickness)
	{
		this.ctx.beginPath();
		this.ctx.moveTo(fromX, fromY);
		this.ctx.lineTo(toX, toY);
		this.ctx.strokeStyle = color;
		this.ctx.lineWidth = thickness;
		this.ctx.lineCap = this.lineCap;
		this.ctx.stroke();
		this.ctx.closePath();
	}
	drawEraserPath(points,color,thickness)
	{
		this.drawPath(points,color,thickness, "destination-out");
	}
	drawPath(points, color, thickness, compositeOperation)
	{
		var len = points.length;
		this.ctx.strokeStyle = color;
		this.ctx.lineCap = this.lineCap;
		var oldGCO; 
		if(typeof compositeOperation === 'string')
		{
			oldGCO = this.ctx.globalCompositeOperation;
			this.ctx.globalCompositeOperation = compositeOperation;
		}
		for(var i = 0; i < len-1 ; ++i)
		{
			// this.drawCircle(points[i][0], points[i][1], 2, color, thickness); // useful for debugging
			this.ctx.beginPath();
			this.ctx.lineWidth = thickness[i % thickness.length];
			this.ctx.moveTo(points[i][0], points[i][1]);
			this.ctx.lineTo(points[i+1][0], points[i+1][1]);
			this.ctx.stroke();
			this.ctx.closePath();
		}
		if(typeof compositeOperation === 'string')
			this.ctx.globalCompositeOperation = oldGCO;
	}
	drawCurve(points, controlPoints, color, thickness, compositeOperation)
	{
		var len = points.length;
		this.ctx.strokeStyle = color;
		this.ctx.lineCap = this.lineCap;
		var oldGCO;
		if(typeof compositeOperation === 'string')
		{
			oldGCO = this.ctx.globalCompositeOperation;
			this.ctx.globalCompositeOperation = compositeOperation;
		}
		for(var i = 0, j = 0; i < len - 2 ; ++i, j+=2)
		{
			this.ctx.beginPath();
			this.ctx.lineWidth = thickness[i % thickness.length];
			this.ctx.moveTo(points[i][0], points[i][1]);
			//this.ctx.lineTo(points[i+1][0], points[i+1][1]);
			this.ctx.bezierCurveTo(
				  controlPoints[j][0], controlPoints[j][1]
				, controlPoints[j+1][0], controlPoints[j+1][1]
				, points[i+1][0], points[i+1][1]);
			this.ctx.stroke();
			this.ctx.closePath();
			/*
			this.drawCircle(points[i+1][0], points[i+1][1], 0.25, "#ff00ff", 0.15*thickness); // useful for debugging
			this.drawCircle(controlPoints[j][0], controlPoints[j][1], 0.25, "#ff0000", 0.15*thickness); // useful for debugging
			this.drawCircle(controlPoints[j+1][0], controlPoints[j+1][1], 0.25, "#0000ff", 0.15*thickness); // useful for debugging
			this.drawCircle(points[i][0], points[i][1], 0.25, "#ff00ff", 0.15*thickness); // useful for debugging
			this.drawCircle(points[i][0], points[i][1], 0.15, color, 0.125*thickness); // useful for debugging
			*/
						
		}
		if(typeof compositeOperation === 'string')
			this.ctx.globalCompositeOperation = oldGCO;
	}
	drawEraserLine(fromX, fromY, toX, toY, thickness)
	{
		this.ctx.beginPath();
		this.ctx.moveTo(fromX, fromY);
		this.ctx.lineTo(toX, toY);
		this.ctx.strokeStyle = "rgba(0,0,0,1)";
		this.ctx.lineWidth = thickness;
		this.ctx.lineCap = this.lineCap;
		this.ctx.globalCompositeOperation = "destination-out";
		this.ctx.stroke();
		this.ctx.closePath();
		this.ctx.globalCompositeOperation = this.oldGCO;
	}
	drawRec (fromX, fromY, toX, toY, color, thickness)
	{
		toX = toX - fromX;
		toY = toY - fromY;
		this.ctx.beginPath();
		this.ctx.rect(fromX, fromY, toX, toY);
		this.ctx.strokeStyle = color;
		this.ctx.lineWidth = thickness;
		this.ctx.lineCap = this.lineCap;
		this.ctx.stroke();
		this.ctx.closePath();
	}
	drawCircle (fromX, fromY, radius, color, thickness)
	{
		this.ctx.beginPath();
		this.ctx.arc(fromX, fromY, radius, 0, 2 * Math.PI, false);
		this.ctx.lineWidth = thickness;
		this.ctx.strokeStyle = color;
		this.ctx.stroke();
	}
	clearWhiteboard()
	{
		this.canvas.height = this.canvas.height; // This actually clears the canvas. Don't ask.
		this.ctx.setTransform(1,0,0,1,0,0);
		this.transformContext();
		this.drawBuffer = [];
	}
}

