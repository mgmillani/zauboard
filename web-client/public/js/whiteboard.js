import {CanvasPainter} from "./canvasPainter.js";
import {SVGPainter} from "./svgPainter.js";
import {ActionType, DrawingElement, makePathElement, DrawingContainer, translateDrawing} from "./drawingElement.js";
import {Smoother1D, Smoother2D} from "./smoother.js";
import {EventHandler, ZoomAndPanHandler, PointerInteraction, ToolType, Cursor, RemoteUser , LocalUser} from "./interaction.js";
import {Viewport} from "./viewport.js";

function stepAndRedraw(whiteboard)
{
	var now = new Date();
	whiteboard.cameraControl.step();
	var new_camera = whiteboard.cameraControl.getNewCamera();
	if(new_camera === undefined)
		return;
	var dzlog = new_camera.zoom_log - whiteboard.viewport.zoom_log;
	var redraw = false;
	var dx = new_camera.dx;
	var dy = new_camera.dy;
	if((dzlog > 0.001 || dzlog < -0.001) && (new_camera.pinch !== undefined))
	{
		redraw = true;
		whiteboard.painter.applyZoom(dzlog, new_camera.pinch.x, new_camera.pinch.y);
	}
	if(Math.abs(dx) + Math.abs(dy) > 0.1)
	{
		redraw = true;
	}
	if(Math.abs(dx + dy) > 0.1)
	{
		window.debug.logValues("viewport", {dx : dx, dy : dy , 'view-x' : whiteboard.viewport.x, 'view-y' : whiteboard.viewport.y});
	}
	whiteboard.viewport.x -= dx;
	whiteboard.viewport.y -= dy;
	if(!redraw && !whiteboard.painter.bufferReady())
	{
		redraw = true;
	}
	if(redraw)
	{
		whiteboard.dirty = false;
		window.requestAnimationFrame(
			function () {whiteboard.redrawEverything(true);});
	}
	else
	{
		//window.clearInterval(whiteboard.redrawTimer);
		//whiteboard.redrawTimer = undefined;
	}
}

function sendDrawing(whiteboard)
{
	if(whiteboard.pointerInteraction.isReady())
	{
		var els = whiteboard.pointerInteraction.getElements();
		for(var i = 0 ; i < els.length ; ++i)
		{
			els[i].draw_id = whiteboard.drawId;
			whiteboard.drawId++;
			whiteboard.communication.sendDrawing(els[i].pack());
			els[i].user_id = whiteboard.settings.user_id;
			whiteboard.painter.draw(els[i]);
		}
		whiteboard.dirty = true;
	}
	else
	{
		window.clearInterval(whiteboard.sendTimer);
		whiteboard.sendTimer = undefined;
	}
}

class RemoteWhiteboard
{
	constructor(whiteboard, communication)
	{
		this.whiteboard = whiteboard;
		this.communication = communication;
	}

	restoreDrawings(drawings)
	{
		this.whiteboard.restoreDrawings(drawings);
		this.communication.restore(drawings);
	}

	eraseDrawings(drawings)
	{
		this.whiteboard.eraseDrawings(drawings);
		this.communication.erase(drawings);
	}

	eraseAroundLine(p0, p1, thickness)
	{
		var els = this.whiteboard.eraseAroundLine(p0,p1,thickness);
		if(els.length > 0)
			this.communication.erase(els);
		return(els);
	}
	insertDrawing(drawing)
	{
		this.whiteboard.insertDrawing(drawing);
		this.communication.sendDrawings([drawing]);
	}
	insertDrawings(drawings)
	{
		this.whiteboard.insertDrawings(drawings);
		this.communication.sendDrawings(drawings);
	}
	cursorMove(cx, cy)
	{
		this.communication.cursorMove(cx,cy);
	}
	clearWhiteboard()
	{
		this.whiteboard.clear();
		this.communication.clear();
	}

}

export var whiteboard = {
	canvas: null,
	eraserThicknessFactor: 10,
	ctx: null,
	painter: new SVGPainter(),
	screenPainter: new CanvasPainter(2),
	drawcolor: "black",
	tool: ToolType.pen,
	thickness: 2,
	send_delay: 500,
	dirty: false,
	pointers: [],
	mouseover: false,
	lineCap: "round", //butt, square
	backgroundGrid: null,
	canvasElement: null,
	cursorContainer: null,
	imgContainer: null,
	svgContainer: null, //For draw prev
	mouseOverlay: null,
	ownCursor: null,
	startCoords: [],
	svgLine: null,
	svgRect: null,
	svgCirle: null,
	drawId: 0, //Used for undo function
	imgDragActive: false,
	latestActiveTextBoxId: false, //The id of the latest clicked Textbox (for font and color change)
	pressedKeys: {},
	users: {},
	settings: {
		username: "unknown",
		user_id: undefined,
		sendFunction: null,
	  prefix: ".",
		backgroundGridUrlBase: '/images/KtEBa2.png',
		backgroundGridUrl: './images/KtEBa2.png'
	},
	redrawEverything : function(lowQuality)
	{
		this.painter.redrawEverything(lowQuality);
		this.screenPainter.clearWhiteboard();
		this.pointerInteraction.updateCursor();
		for(const user_id in this.users)
		{
			this.repositionUserCursor(user_id);
		}
	},
	clearInactiveCursors()
	{
		var now = new Date();
		var t = now.getTime();
		var maxDelta = 3 * 1000;
		var deleted = [];
		for(const user_id in this.users)
		{
			var user = this.users[user_id];
			if(t - user.last_activity > maxDelta)
			{
				user.hideCursor();
			}
		}
	},
	cursorMove : function () {},
	clear : function ()
	{
		this.painter.clearWhiteboard();
		this.drawingContainer.clear();
		this.imgContainer.empty();
		this.textContainer.empty();
	},
	repositionUserCursor : function (user_id) {
		let user = this.users[user_id];
		user.redraw();
	},
	centerAtCursor(cursor_pos)
	{
		var dx = this.viewport.x - cursor_pos.x;
		var dy = this.viewport.y - cursor_pos.y;
		this.cameraControl.applyMove(dx,dy);
	},
	setUserId(user_id)
	{
		this.settings.user_id = user_id;
		this.localUser.user_id = user_id;
		if(user_id in this.users)
		{
			this.localUser.updateDrawId(this.users[user_id].draw_id - 1);
		}
	},
	loadWhiteboard: function (whiteboardContainerId, newSettings) {
		var svgns = "http://www.w3.org/2000/svg";
		var _this = this;
		for (var i in newSettings) {
			this.settings[i] = newSettings[i];
		}
		this.settings["username"] = this.settings["username"].replace(/[^0-9a-z]/gi, '');
		//this.settings["whiteboardId"] = this.settings["whiteboardId"].replace(/[^0-9a-z]/gi, '');
	  this.settings["backgroundGridUrl"] = this.settings["prefix"] + this.settings["backgroundGridUrlBase"];

		//background grid (repeating image) and smallest screen indication
		//_this.backgroundGrid = $('<div style="position: absolute; left:0px; top:0; opacity: 0.2; background-image:url(\'' + _this.settings["backgroundGridUrl"] + '\'); height: 100%; width: 100%;"></div>');
		// container for background images
		this.imgContainer = $('<div style="position: absolute; left:0px; top:0; height: 100%; width: 100%;"></div>');
		// SVG container holding drawing or moving previews
		this.svgContainer = $('<svg id="svgData"  style="position: absolute; top:0px; left:0px;" width="100%" height="100%"></svg>');
		// drag and drop indicator, hidden by default
		this.dropIndicator = $('<div style="position:absolute; height: 100%; width: 100%; border: 7px dashed gray; text-align: center; top: 0px; left: 0px; color: gray; font-size: 23em; display: none;"></div>')
		// container for other users cursors
		this.cursorContainer = document.createElement('div');
		this.cursorContainer.style = "position: absolute; left:0px; top:0; height: 100%; width: 100%;";
		// container for texts by users
		this.textContainer = $('<div class="textcontainer" style="position: absolute; left:0px; top:0; height: 100%; width: 100%; cursor:text;"></div>');
		// mouse overlay for draw callbacks
		this.mouseOverlay = $('<div id="mouseoverlay" style="cursor:none; touch-action:none; position: absolute; left:0px; top:0; height: 100%; width: 100%;"></div>');

		var whiteboardContainer = '#' + whiteboardContainerId;
		$(whiteboardContainer).append(_this.backgroundGrid)
			.append(_this.imgContainer)
			//.append(_this.canvasElement)
		this.viewport = new Viewport($(window).width()/2,$(window).height()/2, $(window).width(), $(window).height(), 1);
		var whiteboard_container_el = document.getElementById(whiteboardContainerId);
		this.painter.start(whiteboard_container_el, this.viewport);
		this.screenPainter.start(whiteboard_container_el, this.viewport);
		$(whiteboardContainer).append(_this.svgContainer)
			.append(_this.dropIndicator)
			.append(_this.cursorContainer)
			.append(_this.textContainer);
		$(whiteboardContainer).append(_this.mouseOverlay);
		this.drawingContainer = new DrawingContainer();
		this.ownCursor = new Cursor(1,"#000", this.cursorContainer);
		this.pointers = [];
		this.users = {};
		this.cameraControl = new ZoomAndPanHandler(this.viewport,this.pointers, this.drawingContainer.bounding_box);
		this.cameraControl.start();
		this.communication = this.settings.communication; 
		var remoteWhiteboard = new RemoteWhiteboard(this, this.communication);
		this.localUser = new LocalUser(this.settings.user_id, this.settings.username, this.viewport, remoteWhiteboard);
		this.pointerInteraction = new PointerInteraction(this.tool, this.pointers, this.cameraControl, this.viewport, this.screenPainter, this.svgContainer, this.painter.overlayElement , this.ownCursor, this.localUser, this);
		this.pointerInteraction.setColor(this.drawcolor);
		this.pointerInteraction.setThickness(this.thickness);
		this.pointerInteraction.setTool(this.tool);
		this.eventHandler = new EventHandler(this, this.viewport, this.pointerInteraction, this.cameraControl, this.communication);
		this.eventHandler.setupListeners();
		this.svgData = $('#svgData')[0];
		this.settings["backgroundGridUrl"] = this.settings["prefix"] + this.settings["backgroundGridUrlBase"];
		// redraw what is necessary every 30ms
		if(this.redrawTimer !== undefined)
			window.clearInterval(this.redrawTimer);
		this.redrawTimer = window.setInterval(stepAndRedraw, 30, this);
		// clear inactive cursors from time to time
		if(this.clearInactiveTimer !== undefined)
			window.clearInterval(this.clearInactiveTimer);
		this.clearInactiveTimer = window.setInterval(
			((w) => {return (() => {w.clearInactiveCursors();});})(this), 500);
		//this.resizeCursor();

		$(window).resize(function () { //Handle resize
			_this.viewport.resize($(window).width(), $(window).height());
			_this.painter.resize($(window).width(), $(window).height());
			_this.screenPainter.resize($(window).width(), $(window).height());
			window.requestAnimationFrame(function () {
							_this.redrawEverything(); }); //draw old content again
		});

		/*
		_this.textContainer.on("mousemove touchmove", function (e) {
			e.preventDefault();

			if (_this.imgDragActive || !$(e.target).hasClass("textcontainer")) {
				return;
			}

			let {screen1, world1} = _this.viewport.getMouseCoords(e);
			var currX = world1.x;
			var currY = world1.y;

			_this.communication.sendCursorMove(currX, currY);

		});*/ // TODO: Move to event handler

		//On textcontainer click (Add a new textbox)
		_this.textContainer.on("click", function (e) {
			currX = (e.offsetX || e.pageX - $(e.target).offset().left);
			currY = (e.offsetY || e.pageY - $(e.target).offset().top);
			var fontsize = _this.thickness * 0.5;
			var txId = 'tx' + (+new Date());
			_this.communication.sendAction(ActionType.addTextBox, [_this.drawcolor, fontsize, currX, currY, txId]);
			_this.addTextBox(_this.drawcolor, fontsize, currX, currY, txId, true);
		}, false);
	},
	toggleTracking()
	{
		return(this.viewport.toggleTracking());
	},
	setTracking(track)
	{
		this.viewport.tracking = track;
	},
	toggleZooming()
	{
		return(this.cameraControl.toggleZooming());
	},
	setZooming(zooming)
	{
		this.cameraControl.zooming = zooming;
	},
	toggleCenterOnDrawing()
	{
		return(this.cameraControl.toggleCenterOnDrawing());
	},
	setCenterOnDrawing(flag)
	{
		this.cameraControl.center_on_drawing = flag;
	},
	getDrawings: function(ids)
	{
		var drawings = this.drawingContainer.getDrawings(ids)
		var pkgs = [];
		for(var i = 0; i < drawings.length ; ++i)
		{
			var pkg = drawings[i].pack();
			pkg.user_id = drawings[i].user_id;
			pkgs.push(pkg);
		}
		return(pkgs);
	},
	insertDrawing: function(drawing)
	{
		this.drawingContainer.add(drawing);
		this.painter.draw(drawing);
		if(this.viewport.tracking === true)
		{
			var pos = drawing.center();
			if(drawing.user_id !== this.localUser.user_id)
			{
				var track_diff = this.viewport.track(pos, drawing.user_id)
				if(track_diff !== false)
				{
					if(track_diff.dx !== 0 || track_diff.dy !== 0)
						this.cameraControl.moveTo(track_diff.dx, track_diff.dy);
				}
			}
			else
			{
				this.viewport.track(pos, drawing.user_id);
			}
		}
	},
	insertDrawings: function(drawings)
	{
		for(let i in drawings)
		{
			this.insertDrawing(drawings[i]);
		}
	},
	eraseDrawings: function(drawings)
	{
		this.painter.removeElements(drawings);
		this.drawingContainer.hideElements(drawings);
	},
	restoreDrawings: function(drawings)
	{
		var missing = [];
		for(var i = 0 ; i < drawings.length ; ++i)
		{
			var els = this.drawingContainer.reveal(drawings[i].user_id, drawings[i].draw_id);
			if(els.length === 0)
			{
				missing.push(drawings[i].user_id);
				missing.push(drawings[i].draw_id);
			}
			else
			{
				for(var j = 0 ; j < els.length ; ++j)
				{
					this.painter.draw(els[j].drawing);
				}
			}
		}
		if(missing.length > 0)
		{
			this.communication.requestDrawings(missing);
		}
	},
	redrawMouseCursor: function () {
		var _this = this;
		_this.eventHandler.triggerMouseOut();
		_this.eventHandler.triggerMouseOver();
		_this.eventHandler.triggerMouseMove({ currX: whiteboard.prevX, currY: whiteboard.prevY });
	},
	pointerMove()
	{
		if (this.pointerInteraction.hasElements()) {
			if(this.sendTimer === undefined)
			{
				this.sendTimer = window.setInterval(sendDrawing, this.send_delay, this);
			}
		}
	},
	eraseAroundLine(p0,p1,distance)
	{
		var hidden = this.drawingContainer.hideWithinLine(p0,p1,distance);
		this.painter.removeElements(hidden);
		return(hidden);
	},
	//hideDrawings(user_id, hidden)
	//{
	//	this.drawingContainer.hideElements(hidden);
	//	this.painter.removeElements(hidden);
	//},
	setRedrawTimer()
	{
		if(this.redrawTimer === undefined)
		{
			this.redrawTimer = window.setInterval( this.stepAndRedraw, 10, this);
		}
	},
	dragCanvasRectContent: function (xf, yf, xt, yt, width, height) {
		var tempCanvas = document.createElement('canvas');
		tempCanvas.width = width;
		tempCanvas.height = height;
		var tempCanvasContext = tempCanvas.getContext('2d');
		tempCanvasContext.drawImage(this.painter.canvas, xf, yf, width, height, 0, 0, width, height);
		this.painter.eraseRec(xf, yf, width, height);
		this.painter.ctx.drawImage(tempCanvas, xt, yt);
	},
	setStrokeThickness(thickness) {
		var _this = this;
		_this.thickness = thickness;
		_this.pointerInteraction.setThickness(thickness);

		if (_this.tool === "text" && _this.latestActiveTextBoxId) {
			_this.communication.sendAction(ActionType.setTextboxFontSize, [_this.latestActiveTextBoxId, thickness]);
			_this.setTextboxFontSize(_this.latestActiveTextBoxId, thickness);
		}
		//_this.resizeCursor();
	},
	addImgToCanvasByUrl: function (url) {
		var _this = this;
		var wasTextTool = false;
		if (_this.tool === "text") {
			wasTextTool = true;
			_this.setTool("mouse"); //Set to mouse tool while dropping to prevent errors
		}
		_this.imgDragActive = true;
		_this.mouseOverlay.css({ "cursor": "default" });
		var imgDiv = $('<div class="dragMe" style="border: 2px dashed gray; position:absolute; left:200px; top:200px; min-width:160px; min-height:100px; cursor:move;">' +
			'<img style="width:100%; height:100%;" src="' + url + '">' +
			'<div style="position:absolute; right:5px; top:3px;">' +
			'<button draw="1" style="margin: 0px 0px; background: #03a9f4; padding: 5px; margin-top: 3px; color: white;" class="addToCanvasBtn btn btn-default">Draw to canvas</button> ' +
			'<button draw="0" style="margin: 0px 0px; background: #03a9f4; padding: 5px; margin-top: 3px; color: white;" class="addToBackgroundBtn btn btn-default">Add to background</button> ' +
			'<button style="margin: 0px 0px; background: #03a9f4; padding: 5px; margin-top: 3px; color: white;" class="xCanvasBtn btn btn-default">x</button>' +
			'</div>' +
			'<i style="position:absolute; bottom: -4px; right: 2px; font-size: 2em; color: gray; transform: rotate(-45deg);" class="fas fa-sort-down" aria-hidden="true"></i>' +
			'</div>');
		imgDiv.find(".xCanvasBtn").click(function () {
			_this.imgDragActive = false;
			_this.refreshCursorAppearance();
			imgDiv.remove();
			if (wasTextTool) {
				_this.setTool("text");
			}
		});
		imgDiv.find(".addToCanvasBtn,.addToBackgroundBtn").click(function () {
			var draw = $(this).attr("draw");
			_this.imgDragActive = false;
			_this.refreshCursorAppearance();
			var width = imgDiv.width();
			var height = imgDiv.height();
			var p = imgDiv.position();
			var left = Math.round(p.left * 100) / 100;
			var top = Math.round(p.top * 100) / 100;
			if (draw == "1") { //draw image to canvas
				_this.drawImgToCanvas(url, width, height, left, top);
			} else { //Add image to background
				_this.drawImgToBackground(url, width, height, left, top);
			}
			_this.communication.sendBG(draw, url, width, height, left, top);
			_this.drawId++;
			imgDiv.remove();
			if (wasTextTool) {
				_this.setTool("text");
			}
		});
		_this.mouseOverlay.append(imgDiv);
		imgDiv.draggable();
		imgDiv.resizable();
	},
	drawImgToBackground(url, width, height, left, top) {
		this.imgContainer.append('<img crossorigin="anonymous" style="width:' + width + 'px; height:' + height + 'px; position:absolute; top:' + top + 'px; left:' + left + 'px;" src="' + url + '">')
	},
	addTextBox(textcolor, fontsize, left, top, txId, newLocalBox) {
		var _this = this;
		var textBox = $('<div id="' + txId + '" class="textBox" style="font-family: Monospace; position:absolute; top:' + top + 'px; left:' + left + 'px;">' +
			'<div contentEditable="true" spellcheck="false" class="textContent" style="outline: none; font-size:' + fontsize + 'em; color:' + textcolor + '; min-width:50px; min-height:50px;"></div>' +
			'<div title="remove textbox" class="removeIcon" style="position:absolute; cursor:pointer; top:-4px; right:2px;">x</div>' +
			'<div title="move textbox" class="moveIcon" style="position:absolute; cursor:move; top:1px; left:2px; font-size: 0.5em;"><i class="fas fa-expand-arrows-alt"></i></div>' +
			'</div>');
		_this.latestActiveTextBoxId = txId;
		textBox.click(function (e) {
			e.preventDefault();
			_this.latestActiveTextBoxId = txId;
			return false;
		})
		textBox.on("mousemove touchmove", function (e) {
			e.preventDefault();
			if (_this.imgDragActive) {
				return;
			}
			let {screenX, screenY, worldX, worldY} = _this.getPointersCoords();
			var currX = worldX;
			var currY = worldY;
			var textBoxPosition = textBox.position();
			if ($(e.target).hasClass("removeIcon")) {
				currX += textBox.width() - 4;
			}
			_this.communication.sendCursorMove(currX, currY);
		})
		this.textContainer.append(textBox);
		textBox.draggable({
			handle: ".moveIcon",
			stop: function () {
				var textBoxPosition = textBox.position();
				_this.communication.sendAction(ActionType.setTextboxPosition, [txId, textBoxPosition.top, textBoxPosition.left]);
			},
			drag: function () {
				var textBoxPosition = textBox.position();
				_this.communication.sendAction(ActionType.setTextboxPosition, [txId, textBoxPosition.top, textBoxPosition.left]);
			}
		});
		textBox.find(".textContent").on("input", function () {
			var text = btoa(unescape(encodeURIComponent($(this).html()))); //Get html and make encode base64 also take care of the charset
			_this.communication.sendAction(ActionType.setTextboxText, [txId, text]);
		});
		textBox.find(".removeIcon").click(function (e) {
			$("#" + txId).remove();
			_this.communication.sendAction(ActionType.removeTextbox, [txId]);
			e.preventDefault();
			return false;
		});
		if (newLocalBox) {
			textBox.find(".textContent").focus();
		}
		if (this.tool === "text") {
			textBox.addClass("active");
		}
	},
	setTextboxText(txId, text) {
		$("#" + txId).find(".textContent").html(decodeURIComponent(escape(atob(text)))); //Set decoded base64 as html
	},
	removeTextbox(txId) {
		$("#" + txId).remove();
	},
	setTextboxPosition(txId, top, left) {
		$("#" + txId).css({ "top": top + "px", "left": left + "px" });
	},
	setTextboxFontSize(txId, fontSize) {
		$("#" + txId).find(".textContent").css({ "font-size": fontSize + "em" });
	},
	setTextboxFontColor(txId, color) {
		$("#" + txId).find(".textContent").css({ "color": color });
	},
	drawImgToCanvas(url, width, height, left, top, doneCallback) { // TODO: This function is broken
		var _this = this;
		doneCallback();
		return; // TODO: Quick fix to avoid this function
		var img = document.createElement('img');
		img.onload = function () {
			_this.ctx.drawImage(img, left, top, width, height);
			if (doneCallback) {
				doneCallback();
			}
		}
		img.src = url;
	},
	undoWhiteboard : function(user_id)
	{
		var _this = this;
		this.imgContainer.empty();
		window.requestAnimationFrame(function () {
			if(user_id === _this.localUser.user_id)
				_this.localUser.undo();
			else if(user_id in _this.users)
				_this.users[user_id].undo();
		});
	},
	undoWhiteboardClick: function () {
		//this.communication.sendAction(ActionType.undo);
		this.undoWhiteboard(this.settings.user_id);
	},
	setTool: function (tool) {
		this.tool = ToolType[tool];
		this.pointerInteraction.setTool(this.tool);
		if (this.tool === "text") {
			$(".textBox").addClass("active");
			this.textContainer.appendTo($(whiteboardContainer)); //Bring textContainer to the front
		} else {
			$(".textBox").removeClass("active");
			this.mouseOverlay.appendTo($(whiteboardContainer));
		}
		this.refreshCursorAppearance();
		this.mouseOverlay.find(".xCanvasBtn").click();
		this.latestActiveTextBoxId = null;
	},
	setDrawColor(color) {
		var _this = this;
		_this.drawcolor = color;
		_this.pointerInteraction.setColor(color);
		if (_this.tool == "text" && _this.latestActiveTextBoxId) {
			_this.communication.sendAction(ActionType.setTextboxFontColor, [_this.latestActiveTextBoxId, color]);
			_this.setTextboxFontColor(_this.latestActiveTextBoxId, color);
		}
		else if(_this.tool === ToolType.eraser || _this.tool === ToolType.mouse)
		{
			_this.setTool("pen");
		}
	},
	drawContent: function (content, doneCallback)
	{
		var _this = this;
		var tool = content["t"];
		var data = content["d"];
		var color = content["c"];
		var user_id = content["user_id"];
		var thickness = content["th"];
		switch(tool)
		{
			case ActionType.addImgBG:
				if (content["draw"] === "1")
					_this.drawImgToCanvas(content["url"], data[0], data[1], data[2], data[3], doneCallback)
				else
					_this.drawImgToBackground(content["url"], data[0], data[1], data[2], data[3]);
				break;
			case ActionType.recSelect:
				_this.dragCanvasRectContent(data[0], data[1], data[2], data[3], data[4], data[5]);
				break;
			case ActionType.eraseRec:
				_this.painter.eraseRec(data[0], data[1], data[2], data[3]);
				break;
			case ActionType.addTextBox:
				_this.addTextBox(data[0], data[1], data[2], data[3], data[4]);
				break;
			case ActionType.setTextboxText:
				_this.setTextboxText(data[0], data[1]);
				break;
			case ActionType.removeTextBox:
				_this.removeTextbox(data[0]);
				break;
			case ActionType.setTextboxPosition:
				_this.setTextboxPosition(data[0], data[1], data[2]);
				break;
			case ActionType.setTextboxFontSize:
				_this.setTextboxFontSize(data[0], data[1]);
				break;
			case ActionType.setTextboxFontColor:
				_this.setTextboxFontColor(data[0], data[1]);
				break;
			case ActionType.cursor:
				if(typeof _this.settings !== undefined)
				{
					var user_id = content["user_id"];
					if (content["event"] === "move")
					{
						if(user_id in _this.users)
						{
							_this.users[user_id].setPosition(data[0], data[1]);
						}
						else
						{
							_this.users[user_id] = new RemoteUser(user_id, content["username"], _this.viewport, _this,content["badge_fg"], content["badge_bg"], _this.cursorContainer);
						}
					}
					else if(user_id in _this.users)
					{
						_this.users.hideCursor();
						delete _this.users[user_id];
					}
				}
				break;
		}
	}
  , cursorUpdate : function(cx, cy, user_id)
  {
    if(!(user_id in this.users))
    {
      this.users[user_id] = new RemoteUser(user_id, "guest", this.viewport, this, "#000000", "#FFFFFF", this.cursorContainer);
    }
    this.users[user_id].setPosition({x:cx, y:cy});
    
    /*else if(user_id in this.users)
    {
      this.users[user_id].hideCursor();
    }*/
  }
  , setTag : function(user_id, tag)
  {
    if(!(user_id in this.users))
    {
      this.users[user_id] = new RemoteUser(user_id, tag.username, this.viewport, this, tag.fg, tag.bg, this.cursorContainer);
    }
    this.users[user_id].setTag(tag);
  }
  , setOwnTag : function (tag)
  {
    this.ownCursor.setHintColor(tag.badge_fg);
  }
  , incomingDrawing(drawing)
  {
    if(!(drawing.user_id in this.users))
    {
      this.users[drawing.user_id] = new RemoteUser(drawing.user_id, "guest", this.viewport, this,undefined, undefined, this.cursorContainer);
    }
    this.users[drawing.user_id].updateDrawId(drawing.draw_id);
    if(drawing.user_id === this.localUser.user_id)
      this.localUser.updateDrawId(drawing.draw_id);
    this.insertDrawing(drawing);
  }
	/*, handleIncomingEventsAndData: function (content) {
			this.handleEventsAndData(content, true);
	} */
	, handleEventsAndData: function (content, isNewData, doneCallback) {
		var tool = content["t"];
		var user_id = content["user_id"];
		if(tool === ActionType.cursor)
		{
		}
		else
		{
			switch(content.t)
			{
				case ActionType.remove:
					var targets = [];
					for(var i = 0 ; i < content.d.length ; i+=2)
					{
						targets.push({user_id : content.d[i], draw_id : content.d[i+1]});
					}
					this.eraseDrawings(targets);
					break;
				case ActionType.restore:
					var targets = [];
					for(var i = 0 ; i < content.d.length ; i+=2)
					{
						targets.push({user_id : content.d[i], draw_id : content.d[i+1]});
					}
					this.restoreDrawings(targets);
					break;
				case ActionType.clear:
					this.clear();
					break;
				case ActionType.curve:
				case ActionType.path:
				case ActionType.circle:
					var dr = new DrawingElement(content);
					break;
			}
		}
	},
	userLeftWhiteboard(username) {
		this.cursorContainer.find("." + username).remove();
	},
	refreshUserBadges() {
		this.cursorContainer.find(".userbadge").remove();
	},
	getImageDataBase64() {
		var _this = this;
		var width = this.mouseOverlay.width();
		var height = this.mouseOverlay.height();
		var copyCanvas = document.createElement('canvas');
		copyCanvas.width = width;
		copyCanvas.height = height;
		var ctx = copyCanvas.getContext("2d");

		$.each(_this.imgContainer.find("img"), function () { //Draw Backgroundimages to the export canvas
			var width = $(this).width();
			var height = $(this).height();
			var p = $(this).position();
			var left = Math.round(p.left * 100) / 100;
			var top = Math.round(p.top * 100) / 100;
			ctx.drawImage(this, left, top, width, height);
		});

		var destCtx = copyCanvas.getContext('2d'); //Draw the maincanvas to the exportcanvas
		destCtx.drawImage(this.painter.canvas, 0, 0);

		$.each($(".textBox"), function () { //Draw the text on top
			var textContainer = $(this)
			var textEl = $(this).find(".textContent");
			var text = textEl.text();
			var fontSize = textEl.css('font-size');
			var fontColor = textEl.css('color');
			var p = textContainer.position();
			var left = Math.round(p.left * 100) / 100;
			var top = Math.round(p.top * 100) / 100;
			top += 25; //Fix top position
			ctx.font = fontSize + " Monospace";
			ctx.fillStyle = fontColor;
			ctx.fillText(text, left, top);
		});

		var url = copyCanvas.toDataURL();
		return url;
	},
	getImageDataJson() {
		var sendObj = [];
		for (var i = 0; i < this.painter.drawBuffer.length; i++) {
			sendObj.push(JSON.parse(JSON.stringify(this.painter.drawBuffer[i])));
			delete sendObj[i]["username"];
			delete sendObj[i]["user_id"];
			delete sendObj[i]["wid"];
			delete sendObj[i]["draw_id"];
		}
		return JSON.stringify(sendObj);
	},
	loadData: function (content) {
		var _this = this;
		_this.loadDataInSteps(content, true, function (stepData) {
			if (stepData["user_id"] === _this.settings.user_id && _this.drawId < stepData["draw_id"]) {
				_this.drawId = stepData["draw_id"] + 1;
			}
		});
	},
	/*loadIncomingData: function (content) {
			this.loadData(content);
      //window.requestAnimationFrame(function() { _this.loadData(content); } );
	},*/
	/*loadDataInSteps(content, isNewData, callAfterEveryStep) {
		var _this = this;

		function lData(index) {
			for (var i = index; i < content.length; i++) {
				if (content[i]["t"] === ActionType.addImgBG && content[i]["draw"] == "1") {
					_this.handleEventsAndData(content[i], isNewData, function () {
						callAfterEveryStep(content[i], i);
						lData(i + 1);
					});
					break;
				} else {
					_this.handleEventsAndData(content[i], isNewData);
					callAfterEveryStep(content[i], i);
				}
			}
		}
		lData(0);
	},*/
	/*loadJsonData(content, doneCallback) {
		var _this = this;
		window.requestAnimationFrame( function () {
          _this.loadDataInSteps(content, false, function (stepData, index) {
			_this.sendPackage(stepData);
			if (index >= content.length - 1) { //Done with all data
				_this.drawId++;
				if (doneCallback) {
					doneCallback();
				}
			}
		});});
	},*/
	refreshCursorAppearance() { //Set cursor depending on current active tool
		var _this = this;
		if (_this.tool === ToolType.pen || _this.tool === ToolType.eraser) {
			_this.mouseOverlay.css({ "cursor": "none" });
		} else if (_this.tool === "mouse") {
			this.mouseOverlay.css({ "cursor": "default" });
		} else { //Line, Rec, Circle, Cutting
			_this.mouseOverlay.css({ "cursor": "crosshair" });
		}
	},
	delKeyAction: function()
	{
		var _this = this;
		$.each(_this.mouseOverlay.find(".dragOutOverlay"), function () {
			var width = $(this).width();
			var height = $(this).height();
			var p = $(this).position();
			var left = Math.round(p.left * 100) / 100;
			var top = Math.round(p.top * 100) / 100;
			_this.drawId++;
			_this.communication.sendAction(ActionType.eraseRec, [left, top, width, height]);
			_this.painter.eraseRec(left, top, width, height);
		});
		_this.mouseOverlay.find(".xCanvasBtn").click(); //Remove all current drops
		_this.textContainer.find("#" + _this.latestActiveTextBoxId).find(".removeIcon").click();
	},
	escKeyAction: function()
	{
		var _this = this;
		if (!_this.drawFlag) {
			_this.svgContainer.empty();
		}
		_this.mouseOverlay.find(".xCanvasBtn").click(); //Remove all current drops
	},
	pointerUp: function()
	{
		sendDrawing(this);
		if(this.dirty)
		{
			this.screenPainter.clearWhiteboard();
			this.dirty = false;
		}
	}

}
