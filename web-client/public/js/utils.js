export function setNewUrl(base_url, key, page_number)
{
	if(typeof key === 'string')
	{
  	history.pushState(null, null, getNewUrl(base_url, key, page_number));
	}
}
export function getNewUrl(base_url, key, page_number)
{
	return (base_url + '/?board_key=' + encodeURIComponent(key) + '&page=' + String(page_number));
}
export function escapeHtml(string)
{
	let entityMap =
		{ "&": "&amp;"
		,	"<": "&lt;"
		,	">": "&gt;"
		,	'"': '&quot;'
		,	"'": '&#39;'
		,	"/": '&#x2F;'
		};
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
		});
}

export function checkPageRanges(string, number_of_pages)
{
	var ranges = string.split(",");
	var result = {};
	for(var i = 0 ; i < ranges.length ; ++i)
	{
		var range = ranges[i].split('-');
		if(range.length === 1)
		{
			if(Number(range[0]) > number_of_pages || Number(range[0]) < 1)
			{
				result.ok = false;
				result.message = range[0] + " is not a valid page."
			}
		}
		if(range.length === 2)
		{
			var p0;
			if(range[0].trim() === '')
				p0 = 1;
			else
				p0 = Number(range[0]);
			var p1;
			if(range[1].trim() === '')
				p1 = number_of_pages;
			else
				p1 = Number(range[1]);
			if(p0 > p1)
			{
				result.ok = false;
				result.message = ranges[i] + " is not a valid page range, since " + p0 + " > " + p1 + ".";
				return result;
			}
			if(p0 > number_of_pages || p0 < 1)
			{
				result.ok = false;
				result.message = p0 + " is not a valid page."
				return result;
			}
			if(p1 > number_of_pages || p1 < 1)
			{
				result.ok = false;
				result.message = p1 + " is not a valid page."
				return result;
			}
			if(range.length > 2)
			{
				result.ok = false;
				result.message = ranges[i] + " is not a valid page range."
				return result;
			}
		}

	}
	result.ok = true;
	result.message = "";
	return result;
}
