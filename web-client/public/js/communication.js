export class Communication
{
	connect(url, protocol)
	{
		this.url = url;
		var _this = this;
		this.protocol = protocol;
		if(protocol === "https:")
			this.socket = new WebSocket("wss://" + url);
		else
			this.socket = new WebSocket("ws://" + url);
		this.socket.onopen = this.onOpen;
		this.socket.onmessage = (event) => {_this.onMessage(event)};
		this.socket.onerror   = (event) => {_this.onError(event)};
		this.socket.onclose   = (event) => {_this.onClose(event)};
	}

	onMessage(event)
	{
		var str = event.data;
		var header = str;
		for(var i = 0 ; i < str.length ; ++i)
		{
			if(event.data[i] === '\n')
			{
				header = str.slice(0,i);
				str = str.slice(i+1,str.length);
				break;
			}
		}
		switch(header.toLowerCase())
		{
			case "switchpage":
				this.onSwitchPage(parseInt(str, 10));
				break;
			case "drawings":
				var version = str;
				for(var i = 0 ; i < str.length ; ++i)
				{
					if(str[i] === '\n')
					{
						version = str.slice(0,i);
						str = str.slice(i+1,str.length);
						break;
					}
				}
				var drawings = JSON.parse(str);
				this.onDrawings(drawings);
				break;
			case "cursorupdate":
				var coords = str.split(" ");
				this.onCursorUpdate(parseInt(coords[0], 10), parseInt(coords[1], 10), parseInt(coords[2], 10));
				break;
			case "restore":
				var lns = str.split("\n");
				for(var i = 0; i < lns.length ; ++i)
				{
					var ln = lns[i];
					var cs = ln.split(" ");
					lns[i] = {"user_id" : parseInt(cs[0], 10), "draw_id":parseInt(cs[1], 10)};
				}
				this.onRestore(lns);
				break;
			case "erase":
				var lns = str.split("\n");
				for(var i = 0; i < lns.length ; ++i)
				{
					var ln = lns[i];
					var cs = ln.split(" ");
					lns[i] = {"user_id" : parseInt(cs[0], 10), "draw_id":parseInt(cs[1], 10)};
				}
				this.onErase(lns);
				break;
			case "request":
				var lns = str.split("\n");
				for(var i = 0; i < lns.length ; ++i)
				{
					var ln = lns[i];
					var cs = ln.split(" ");
					lns[i] = {"user_id" : parseInt(cs[0], 10), "draw_id":parseInt(cs[1], 10)};
				}
				this.onRequest(lns);
				break;
			case "provide":
				var drawings = JSON.parse(str);
				this.onProvide(drawings);
				break;
			case "setuserid":
				this.user_id = parseInt(str, 10);
				this.onSetUserID(this.user_id);
				break;
			case "accessdenied":
				this.onAccessDenied();
				break;
			case "accessgranted":
				this.onAccessGranted();
				break;
			case "clear":
				this.onClear();
				break;
			case "provideusertoken":
				this.onProvideUserToken(str);
				break
			case "whiteboards":
				var whiteboards = parseWhiteboards(str);
				this.onWhiteboards(whiteboards);
				break;
			case "settag":
				var lns = str.split("\n");
				this.onSetTag({username : lns[0], fg: lns[1], bg:lns[2], id:lns[3]});
				break;
			case "pdfsummary":
				var lns = str.split("\n");
				this.onPdfSummary({pdfID : lns[0], numberOfPages : Number(lns[1])});
			case "fileready":
				var lns = str.split("\n");
				this.onFileReady(Number(lns[0]), lns[1]);
			case "success":
				this.onSuccess();
			case "error":
				var lns = str.split("\n");
				if(lns[0] === 'incorrectpassword')
				{
					this.onIncorrectPassword();
				}
		}

	}

	pointerOut()
	{
		// this.sendPackage({ "t": ActionType.cursor, "event": "out" });
	}

	sendPackage(content)
	{
		if(this.socket.readyState === 1) // Open state
			this.socket.send(content);
		else if(this.socket.readyState === 3) // Closed state
			this.connect(this.url, this.protocol);
	}

	switchPage(page)
	{
		this.sendPackage("switchpage\n" + page);
	}
	sendDrawings(drawings)
	{
		var packed = [];
		for(var i = 0; i < drawings.length ; ++i)
		{
			var d = drawings[i];
			var p = d.pack();
			p.u = this.user_id;
			packed.push(p);
		}
		this.sendPackage("drawings\n1\n" + JSON.stringify(packed));
	}
	cursorUpdate(cx,cy)
	{
		this.sendPackage("cursorupdate\n" + cx + " " + cy);
	}
	idList(ids, name)
	{
		var nids = [];
		for(var i = 0; i < ids.length ; ++i)
		{
			var eids = ids[i];
			nids.push(eids.user_id + " " + eids.draw_id);
		}
		var str = nids.join("\n");
		this.sendPackage(name + "\n" + str);
	}
	erase(ids)
	{
		this.idList(ids, "erase");
	}
	restore(ids)
	{
		this.idList(ids, "restore");
	}
	request(ids)
	{
		this.idList(ids, "request");
	}
	provide(drawings)
	{
		for(var i = 0; i < drawings.length ; ++i)
		{
			var d = drawings[i];
			drawings[i] = d.pack();
		}
		this.sendPackage("provide\n1\n" + JSON.stringify(drawings));
	}
	joinWhiteboard(whiteboardKey, page)
	{
		this.sendPackage(["joinwhiteboard",whiteboardKey,page].join("\n"));
	}
	loginWithGlobalPassword(passowrd)
	{
		this.sendPackage("loginwithglobalpassword\n" + password);
	}
	loginWithUserToken(token)
	{
		this.sendPackage("loginwithusertoken\n" + token);
	}
	loginWithUserPassword(username, password)
	{
		this.sendPackage("loginwithuserpassword\n" + username + "\n" + password);
	}
	loginAsGuest(page)
	{
		this.sendPackage("loginasguest");
	}
	requestUserToken()
	{
		this.sendPackage("requestusertoken");
	}
	setTag(username, tagFG, tagBG)
	{
		this.sendPackage(["settag", username, tagFG, tagBG].join("\n"));
	}
	clear()
	{
		this.sendPackage("clear");
	}
	getWhiteboardInfo(key)
	{
		this.sendPackage("getwhiteboardinfo\n" + key);
	}
	listWhiteboards()
	{
		this.sendPackage("listwhiteboards");
	}
	createWhiteboard(name, is_public)
	{
		this.sendPackage("createwhiteboard\n" + name + "\n" + is_public);
	}
	removeWhiteboard(whiteboard_key)
	{
		this.sendPackage("deletewhiteboard\n" + whiteboard_key)
	}
	restoreWhiteboard(whiteboard_key)
	{
		this.sendPackage("restorewhiteboard\n" + whiteboard_key)
	}
	uploadPDF(pdf)
	{
		this.sendPackage("uploadpdf\n" + pdf);
	}
	importPDF(pdfID, toWhiteboardPage, pdfPages, scale, singlePage)
	{
		this.sendPackage(["importpdf", pdfID, pdfPages, toWhiteboardPage, scale, singlePage ? "true" : "false" ].join("\n"));
	}
	exportPDF(pages)
	{
		this.sendPackage(["exportpdf", 0, pages].join("\n"));
	}
	changePassword(current_pass, new_pass)
	{
		this.sendPackage(["changepassword",current_pass,new_pass].join("\n"));
	}

	init()
	{
		if(this.state === "open")
		{
			this.onInit();
		}
		else
		{
			var _this = this;
			this.onOpen = function(event)
			{
				_this.state = "open";
				_this.onInit();
			}
		}
	}

	close()
	{
		this.socket.close();
	}

	onInit()
	{
	}

	onError(error)
	{
		console.error("Socket error:");
		console.error(error);
	}
	onClose(event)
	{
	}

	onIncorrectPassword()
	{
	}

	onSuccess()
	{
	}
}

function parseWhiteboards(str)
{
	var whiteboards = [];
	var lines = str.split("\n");
	if(lines.length > 1)
	{
		for(var i = 0 ; i < lines.length ; i+=2)
		{
			var info = {};
			var ln = lines[i].split(" ");
			info.key = ln[0];
			info.pages = ln[1];
			info.last_modified = ln[2];
			info.is_public = ln[3].toLowerCase() === "true";
			info.is_archived = ln[4].toLowerCase() === "true";
			info.name = ln.slice(5, ln.length).join(" ");
			info.owner = lines[i+1];
			whiteboards.push(info);
		}
	}
	return whiteboards;
}
