
export const ActionType = Object.freeze({"circle":0, "curve":1, "path":2, "rawSvg":3});

export function makePathElement(type, points, color, thickness)
{
	var el = new DrawingElement();
	el.type = type;
	el.canvasElement = true;
	el.color = color;
	el.points = points;
	el.thickness = thickness;
	if(type === ActionType.curve)
	{
		el.computeControlPoints();
	}
	return el;
}

export function makeCircleElement(center, radius, color, thickness)
{
	var el = new DrawingElement();
	el.type = ActionType.circle;
	el.canvasElement = true;
	el.color = color;
	el.radius = radius;
	el.points = [center];
	el.thickness = thickness;
	return el;
}

export function translateDrawing(drawing, dx, dy)
{
	var points = [];
	switch(drawing.type)
	{
		case ActionType.circle:
			return( makeCircleElement([drawing.points[0][0] + dx, drawing.points[0][1] + dy], drawing.radius,drawing.color, drawing.thickness) );
			break;
		case ActionType.curve:
			points.push([drawing.controlPoints[0][0] + dx, drawing.controlPoints[0][1] + dy]);
		case ActionType.path:
			for(let i in drawing.points)
			{
				points.push([drawing.points[i][0] + dx, drawing.points[i][1] + dy]);
			}
			return (makePathElement(drawing.type, points, drawing.color, drawing.thickness));
			break;
	}
}

export class DrawingElement
{
	constructor(content, draw_id)
	{
		if(content === undefined)
			return;
		if(typeof content.d === 'number')
		{
			if(draw_id === undefined)
				draw_id = content.i;
			this.draw_id = draw_id;
			this.type = content.d;
			this.user_id = content.u;
			this.color     = content.c;
			this.thickness = content.h;
			this.canvasElement = true;
			switch(this.type)
			{
				case ActionType.curve:
				case ActionType.path:
					this.points = [];
					var points = content.p;
					if(this.type === ActionType.curve)
					{
						this.points.push([content.o[0], content.o[1]]);
					}
					var len = points.length;
					for(var i = 0; i < len ; i+=2)
					{
						this.points.push([points[i], points[i+1]]);
					}
					if(this.type === ActionType.curve)
					{
						this.points.push([content.o[2], content.o[3]]);
						this.computeControlPoints();
					}
					break;
				case ActionType.circle:
					this.points    = [[content.x, content.y]];
					this.radius    = content.r;
					break;
				case ActionType.rawSvg:
					this.points    = [[content.x, content.y]];
					this.scale     = content.c;
					this.width     = content.w;
					this.height    = content.h;
					this.svg       = content.s;
			}
		}
	}

	pack()
	{
		var result = {};
		result.d = this.type;
		result.i = this.draw_id;
		result.c = this.color;
		result.h = this.thickness;
		switch(this.type)
		{
			case ActionType.curve:
				this.canvasElement = true;
				result.p = [];
				result.o = [];
				result.o.push(this.controlPoints[0][0]);
				result.o.push(this.controlPoints[0][1]);
				var clen = this.controlPoints.length;
				result.o.push(this.controlPoints[clen - 1][0]);
				result.o.push(this.controlPoints[clen - 1][1]);
				//result.p.push(this.controlPoints[0][0]);
				//result.p.push(this.controlPoints[0][1]);
				var len = this.points.length;
				for(var i = 0; i < len - 1 ; ++i)
				{
					result.p.push(this.points[i][0]);
					result.p.push(this.points[i][1]);
				}
				break;
			case ActionType.path:
				this.canvasElement = true;
				result.p = [];
				var len = this.points.length
				for(var i = 0; i < len ; ++i)
				{
					result.p.push(this.points[i][0]);
					result.p.push(this.points[i][1]);
				}
				break;
			case ActionType.circle:
				result.x = this.points[0][0];
				result.y = this.points[0][1];
				result.r = this.radius;
				break;
		}
		return result;
	}

	computeControlPoints()
	{
		this.controlPoints = [];
		this.controlPoints.push(this.points[0]);
		this.points.splice(0,1);
		var n = this.points.length;
		for(var i = 1 ; i < n - 1; i++)
		{
			var p1 = this.points[i-1];
			var p2 = this.points[i];
			var p3 = this.points[i+1];
			// v is the tangential vectors of the curve at p2
			var vx = (p3[0] - p1[0])/2;
			var vy = (p3[1] - p1[1])/2;
			//
			var ux = -vx;
			var uy = -vy;
			var wx = vx;
			var wy = vy;
			// get the length of the segments p1p2 and p2p3
			var dx1 = p2[0] - p1[0];
			var dy1 = p2[1] - p1[1];
			var dx2 = p2[0] - p3[0];
			var dy2 = p2[1] - p3[1];
			var l1 = Math.sqrt(dx1*dx1 + dy1*dy1);
			var l2 = Math.sqrt(dx2*dx2 + dy2*dy2);
			var l12 = l1 + l2;
			if(l12 === 0.0)
			{
				this.controlPoints.push([p2[0], p2[1]]);
				this.controlPoints.push([p2[0], p2[1]]);
			}
			else
			{
				// compute control points
				this.controlPoints.push([p2[0] + ux * l1 / l12, p2[1] + uy * l1 / l12]);
				this.controlPoints.push([p2[0] + wx * l2 / l12, p2[1] + wy * l2 / l12]);
			}
		}
	}

	center()
	{
		switch(this.type)
		{
			case ActionType.circle:
				return(this.points[0]);
			case ActionType.curve:
			case ActionType.path:
				var sx = 0;
				var sy = 0;
				for(var i = 0 ; i < this.points.length ; ++i)
				{
					sx += this.points[i][0];
					sy += this.points[i][1];
				}
				return({x : sx / this.points.length, y : sy / this.points.length});
			default:
				return({x:0, y:0});
		}
	}

	copy()
	{
		var points = [];
		switch(this.type)
		{
			case ActionType.circle:
				return( makeCircleElement([this.points[0][0], this.points[0][1]], this.radius,this.color, this.thickness) );
				break;
			case ActionType.curve:
				points.push([this.controlPoints[0][0], this.controlPoints[0][1]]);
			case ActionType.path:
				for(let i in this.points)
				{
					points.push([this.points[i][0], this.points[i][1]]);
				}
				return (makePathElement(this.type, points, this.color, this.thickness));
				break;
		}
	}
}

export class GridPartition
{
	constructor(side)
	{
		this.grid = {};
		this.side = side;
	}

	clear()
	{
		this.grid = {};
	}

	add(el)
	{
		var dr = el.drawing;
		switch(dr.type)
		{
			case ActionType.curve:
			case ActionType.path:
				for(var i = 0 ; i < dr.points.length - 1 ; ++i)
				{
					var p0 = dr.points[i];
					var p1 = dr.points[i+1];
					var x_min, x_max, y_min, y_max;
					if(p0[0] < p1[0])
					{
						x_min = p0[0];
						x_max = p1[0];
					}
					else
					{
						x_min = p1[0];
						x_max = p0[0];
					}
					if(p0[1] < p1[1])
					{
						y_min = p0[1];
						y_max = p1[1];
					}
					else
					{
						y_min = p1[1];
						y_max = p0[1];
					}
					var x_min = Math.floor(x_min / this.side);
					var x_max = Math.floor(x_max / this.side);
					var y_min = Math.floor(y_min / this.side);
					var y_max = Math.floor(y_max / this.side);
					for(var x = x_min ; x <= x_max ; ++x)
					{
						if(!(x in this.grid))
							this.grid[x] = {};
						var column = this.grid[x];
						for(var y = y_min ; y <= y_max ; ++y)
						{
							if(!intersectsRectangle(dr, x*this.side, y*this.side, this.side, this.side))
								continue;
							if(!(y in column))
								column[y] = [];
							column[y].push(el);
						}
					}
				}
				break;
			case ActionType.circle:
				var x_min = Math.floor((dr.points[0][0] - dr.radius) / this.side);
				var x_max = Math.floor((dr.points[0][0] + dr.radius) / this.side);
				var y_min = Math.floor((dr.points[0][1] - dr.radius) / this.side);
				var y_max = Math.floor((dr.points[0][1] + dr.radius) / this.side);
				for(var x = x_min ; x <= x_max ; ++x)
				{
					if(!(x in this.grid))
						this.grid[x] = {};
					var column = this.grid[x];
					for(var y = y_min ; y <= y_max ; ++y)
					{
						if(!(y in column))
							column[y] = [];
						column[y].push(el);
					}
				}
				break;
		}
	}

	withinRectangle(x_rect,y_rect,width,height)
	{
		var x0 = Math.floor(x_rect/this.side);
		var y0 = Math.floor(y_rect/this.side);
		var x1 = Math.floor((x_rect + width)/this.side);
		var y1 = Math.floor((y_rect + height)/this.side);
		var els = {};
		for(var x = x0 ; x <= x1 ; ++x)
		{
			for(var y = y0 ; y <= y1 ; ++y)
			{
				if(x in this.grid)
				{
					var col = this.grid[x];
					if(y in col)
					{
						var cell = col[y];
						// return everything in middle of the region
						if(x > x0 && x < x1 && y > y0 && y < y1)
						{
							this.withinCell(cell, x * this.side,y * this.side,els);
						}
						else
						{
							this.withinCellRectangle(cell, els, x_rect, y_rect, width, height);
						}
					}
				}
			}
		}
		var result = [];
		for(let id in els)
			result.push(els[id]);
		return(result);
	}

	withinLineDistance(x0,y0,x1,y1,distance)
	{
		var v = [x1 - x0, y1 - y0];
		var u = [-v[1], v[0]];
		var lu = Math.sqrt(u[0] * u[0] + u[1] * u[1]);
		u[0] = u[0] / lu;
		u[1] = u[1] / lu;
		var bbox_x = [x0 + u[0]*distance, x0 - u[0]*distance, x1 + u[0]*distance, x1 + u[0]*distance];
		var bbox_y = [y0 + u[1]*distance, y0 - u[1]*distance, y1 + u[1]*distance, y1 + u[1]*distance];
		var x_min = bbox_x[0], y_min = bbox_y[0];
		var x_max = bbox_x[0], y_max = bbox_y[0];
		for(var i = 1 ; i < bbox_x.length ; ++i)
		{
			if(bbox_x[i] < x_min)
				x_min = bbox_x[i];
			else if(bbox_x[i] > x_max)
				x_max = bbox_x[i];
			if(bbox_y[i] < y_min)
				y_min = bbox_y[i];
			else if(bbox_y[i] > y_max)
				y_max = bbox_y[i];
		}
		x_min = Math.floor(x_min / this.side);
		y_min = Math.floor(y_min / this.side);
		x_max = Math.floor(x_max / this.side);
		y_max = Math.floor(y_max / this.side);
		//
		var els = {};
		for(var x = x_min ; x <= x_max ; ++x)
		{
			if(x in this.grid)
			{
				var col = this.grid[x];
				for(var y = y_min ; y <= y_max ; ++y)
				{
					if(y in col)
					{
						var cell = col[y];
						this.withinCellLineDistance(cell, els, x0, y0, x1, y1, v, distance);
					}
				}
			}
		}
		var result = [];
		for(let id in els)
			result.push(els[id]);
		return(result);
	}

	withinCellLineDistance(cell, result, x0,y0,x1,y1,v,distance)
	{
		var removed = [];
		for(var i = 0 ; i < cell.length ; ++i)
		{
			var el = cell[i];
			if(el.hidden)
				removed.push(i);
			else if(intersectsLineDistance(el.drawing, x0,y0,x1,y1,v,distance))
				result[el.id] = el.drawing;
		}
		for(var i = removed.length - 1 ; i > 0 ; --i)
		{
			cell.splice(removed[i],1);
		}
	}

	withinCell(cell, cx,cy, result)
	{
		var removed = [];
		for(var i = 0 ; i < cell.length ; ++i)
		{
			var el = cell[i];
			if(el.hidden)
				removed.push(i);
			else if(el.drawing.type === ActionType.circle)
			{
				if(circleWithinRectangle(el.drawing, cx,cy,this.side,this.side))
					result[el.id] = el.drawing;
			}
			else
				result[el.id] = el.drawing;
		}
		for(var i = removed.length - 1 ; i > 0 ; --i)
		{
			cell.splice(removed[i],1);
		}
	}

	withinCellRectangle(cell, result, x, y, width, height)
	{
		var removed = [];
		for(var i = 0 ; i < cell.length ; ++i)
		{
			var el = cell[i];
			if(el.hidden)
				removed.push(i);
			else if(intersectsRectangle(el.drawing, x,y,width,height))
				result[el.id] = el.drawing;
		}
		for(var i = removed.length - 1 ; i > 0 ; --i)
		{
			cell.splice(removed[i],1);
		}
	}
}

export function intersectsLine(p0,pv,q0,qv, result)
{
	// degenerate cases
	if(pv[1] === 0)
	{
		if(qv[1] === 0)
			return false;
		else
			return intersectsLine(q0,qv,p0,pv, result);
	}
	var tq;
	var tp;
	var dy = q0[1] - p0[1];
	var point = false;
	if(qv[1] !== 0)
	{
		var aq = qv[0] / qv[1];
		var a = pv[0] / pv[1];
		if(Math.abs(aq - a) < 0.00001)
			return false;
		var b = qv[1] * a - qv[0];
		var c = q0[0] - p0[0] - dy * a;
		tq = c / b;
		tp = (dy + tq * qv[1]) / pv[1];
	}
	else
	{
		tp = dy / pv[1];
		if(qv[0] === 0)
		{
			tq = 0;
			point = true;
		}
		else
		{
			tq = (p0[0] - q0[0] + pv[0] * tp) / qv[0];
		}
	}
	if(typeof result !== 'undefined')
	{
		result.t0 = tp;
		result.t1 = tq;
	}
	if(tq >= 0 && tq <= 1)
	{
		return (!point && tp >= 0 && tp <= 1);
	}
	return false;
}

export function isPointInsideRectangle(x0,y0, rect_x, rect_y, rect_w, rect_h)
{
	if(x0 < rect_x || y0 < rect_y)
		return false;
	if(x0 > rect_x + rect_w || y0 > rect_y + rect_h)
		return false;
	return true;
}

export function intersectsRectangle(drawing, x,y,width,height)
{
	switch(drawing.type)
	{
		case ActionType.curve:
		case ActionType.path:
			for(var i = 0 ; i < drawing.points.length - 1 ; ++i)
			{
				var p0 = drawing.points[i];
				var p1 = drawing.points[i+1];
				var pv = [p1[0] - p0[0], p1[1] - p0[1]];
				if(p0[0] >= x && p0[0] <= x + width && p0[1] >= y && p0[1] <= y+height)
					return true;
				if(p1[0] >= x && p1[0] <= x + width && p1[1] >= y && p1[1] <= y+height)
					return true;
				if(  intersectsLine(p0,pv,[x,y],[width,0])
					|| intersectsLine(p0,pv,[x,y],[0,height])
					|| intersectsLine(p0,pv,[x+width,y],[0,height])
					|| intersectsLine(p0,pv,[x,y+height],[width,0]))
				{
					return true;
				}
			}
			break;
		case ActionType.circle:
			var r = drawing.radius;
			var cx = drawing.points[0][0];
			var cy = drawing.points[0][1];
			// circle center inside rectangle
			if(cy >= y && cy <= y + height && cx >= x && cx <= x + width)
			{
				var r2 = r*r;
				var ans = 
					(  (x - cx)*(x - cx) + (y - cy)*(y - cy) > r2
					|| (x - cx)*(x - cx) + (y + height - cy)*(y + height - cy) > r2
					|| (x + width - cx)*(x + width - cx) + (y - cy)*(y - cy) > r2
					|| (x + width - cx)*(x + width - cx) + (y + height - cy)*(y + height - cy) > r2
					);
				return ans;
			}
			else
			{
				var ans =
					(  circleIntersectsLine(cx,cy,r,[x,y],[width,0])
					|| circleIntersectsLine(cx,cy,r,[x,y+height],[width,0])
					|| circleIntersectsLine(cx,cy,r,[x,y],[0,height])
					|| circleIntersectsLine(cx,cy,r,[x+width,y],[0,height])
					);
				return ans;
			}
			break;
	}
	return false;
}

export function circleIntersectsLine(cx,cy,r,p,v)
{
	var dx = p[0] - cx;
	var dy = p[1] - cy;
	var a = v[0]*v[0] + v[1]*v[1];
	var b = 2*(v[0]*dx + v[1]*dy);
	var c = dx*dx + dy*dy - r*r;
	var q = b*b - 4*a*c;
	if(a === 0)
	{
		if(b === 0)
		{
			return (c === 0);
		}
		else
		{
			var t = -c / b;
			return (t >=0 && t <= 1);
		}
	}
	else if(q < 0)
	{
		return false;
	}
	else
	{
		var q2 = Math.sqrt(q);
		var t0 = (q2 - b) / (2*a);
		var t1 = (-q2 - b) / (2*a);
		var ans =
			( (t0 >= 0 && t0 <= 1)
			||(t1 >= 0 && t1 <= 1)
			);
		return ans;
	}
}

export function circleWithinRectangle(el,x,y,width,height)
{
	var points = 
		[ [x,y]
		, [x + width, y]
		, [x + width, y + height]
		, [x, y + height]
		];
	var distances2 = [0,0,0,0];
	var innerR2 = (el.radius - el.thickness); 
	innerR2 = innerR2 * innerR2;
	var outerR2 = (el.radius + el.thickness); 
	outerR2 = outerR2 * outerR2;
	var inside = 0;
	for(var i = 0 ; i < 4 ; ++i)
	{
		var dx = points[i][0] - el.points[0][0];
		var dy = points[i][1] - el.points[0][1];
		var d2 = dx*dx + dy*dy;
		if(d2 < innerR2)
		{
			inside += 1;
		}
		else if(d2 >= innerR2 && d2 <= outerR2) // circle intersects point
		{
			return(true);
		}
	}
	if(inside === 4)
	{
		return(false);
	}
	if(inside > 0)
	{
		return(true);
	}

	// check in which quadrant the circle lies
	// inside
	if(el.points[0][0] >= x && el.points[0][0] <= x + width && el.points[0][1] <= y + height && el.points[0][1] >= y)
	{
		return(true);
	}
	// left
	else if(el.points[0][0] <= x && el.points[0][1] >= y && el.points[0][1] <= y + height)
	{
		var d = pointLineDistance(el.points[0], [x,y], [0,height]);
		if(d*d <= outerR2)
		{
			return(true);
		}
	}
	// right
	else if(el.points[0][0] >= x + width && el.points[0][1] >= y && el.points[0][1] <= y + height)
	{
		var d = pointLineDistance(el.points[0], [x + width,y], [0,height]);
		if(d*d <= outerR2)
		{
			return(true);
		}

	}
	// below
	else if(el.points[0][1] <= y && el.points[0][0] >= x && el.points[0][0] <= x + width)
	{
		var d = pointLineDistance(el.points[0], [x,y+height], [width,0]);
		if(d*d <= outerR2)
		{
			return(true);
		}
	}
	// above
	else if(el.points[0][1] >= y + height && el.points[0][0] >= x && el.points[0][0] <= x + width)
	{
		var d = pointLineDistance(el.points[0], [x,y], [width,0]);
		if(d*d <= outerR2)
		{
			return(true);
		}
	}

	return(false);
}

export function pointLineDistance(p0, p1, v)
{
	var u = [-v[1], v[0]];
	var lu = Math.sqrt(u[0]**2 + u[1]**2);
	u[0] = u[0] / lu;
	u[1] = u[1] / lu;
	var result = {};
	intersectsLine(p1,v,p0,u, result);
	var d0 = (p0[0] - p1[0]) ** 2 + (p0[1] - p1[1]) ** 2;
	var d1 = (p0[0] - p1[0] - v[0]) ** 2 + (p0[1] - p1[1] - v[1]) ** 2;
	var d = Math.min(d0,d1);
	if(result.t0 >= 0 && result.t0 <= 1 && result.t1 * result.t1 < d)
		return Math.abs(result.t1);
	else
		return Math.sqrt(d);
}

export function intersectsLineDistance(drawing, x0,y0,x1,y1,v,distance)
{
	var points = [];
	switch(drawing.type)
	{
		case ActionType.curve:
			points.push(drawing.controlPoints[0]);
		case ActionType.path:
			points = points.concat(drawing.points);
			for(var i = 0 ; i < points.length - 1 ; ++i)
			{
				var p0 = points[i];
				var p1 = points[i+1];
				var pv = [p1[0] - p0[0], p1[1] - p0[1]];
				var thickness = 0;
				if(drawing.thickness.length === 1)
					thickness = drawing.thickness[0]/2;
				else
					thickness = drawing.thickness[i]/2;
				if(  pointLineDistance(p0,[x0,y0],v) < distance + thickness
					|| pointLineDistance(p1,[x0,y0],v) < distance + thickness
					|| pointLineDistance([x0,y0],p0,pv) < distance + thickness
					|| pointLineDistance([x1,y1],p0,pv) < distance + thickness
					|| intersectsLine(p0,pv,[x0,y0],v))
				{
					return true;
				}
			}
			break;
		case ActionType.circle:
			var d = pointLineDistance(drawing.points[0],[x0,y0],v);
			var r = drawing.radius;
			if(  circleIntersectsLine(drawing.points[0][0], drawing.points[0][1], drawing.radius, [x0,y0], v )
			  || (d + distance > r && d - distance < r)
			)
			{
				return true;
			}
			break;
	}
	return false;
}

export function boundingBoxList(els)
{
	if(els.length === 0)
	{
		return undefined;
	}
	else
	{
		var box0 = boundingBox(els[0]);
		for(var i = 1 ; i < els.length ; ++i)
		{
			var box1 = boundingBox(els[i]);
			if(box1.x0 < box0.x0)
				box0.x0 = box1.x0;
			if(box1.x1 > box0.x1)
				box0.x1 = box1.x1;
			if(box1.y0 < box0.y0)
				box0.y0 = box1.y0;
			if(box1.y1 > box0.y1)
				box0.y1 = box1.y1;
		}
		return box0;
	}
}

export function boundingBox(el)
{
	switch(el.type)
	{
		case ActionType.curve:
		case ActionType.path:
			if(el.points.length === 0)
				return {x0 : 0, y0 : 0, x1 : 0, y1 : 0};
			var bbox = {x0 : el.points[0][0], y0 : el.points[0][1], x1 : el.points[0][0], y1 : el.points[0][1]};
			for(var i = 1 ; i < el.points.length ; ++i)
			{
				var p = el.points[i];
				var t = el.thickness[i % el.thickness.length] / 2;
				if(p[0] - t < bbox.x0)
					bbox.x0 = p[0] - t;
				else if(p[0] + t > bbox.x1)
					bbox.x1 = p[0] + t;
				if(p[1] - t < bbox.y0)
					bbox.y0 = p[1] - t;
				else if(p[1] + t > bbox.y1)
					bbox.y1 = p[1] + t;
			}
			return bbox;
			break;
		case ActionType.circle:
			return (
				{ x0 : el.points[0][0] - el.radius
				, y0 : el.points[0][1] - el.radius
				, x1 : el.points[0][0] + el.radius
				, y1 : el.points[0][1] + el.radius
				});
		case ActionType.rawSvg:
			return (
				{ x0 : el.points[0][0]
				, y0 : el.points[0][1]
				, x1 : el.points[0][0] + el.width 
				, y1 : el.points[0][1] + el.height 
				});
	}
	return {x0 : 0, y0 : 0, x1 : 0, y1 : 0};
}

export class DrawingContainer
{
	constructor()
	{
		this.hidden = {};
		this.visible= {};
		this.grid = new GridPartition(100);
		this.numElements = 0;
		this.bounding_box = {x0 : 0, y0 : 0, x1 : 0, y1 : 0};
	}

	// Add element to container.
	add(el)
	{
		var draw_id = el.draw_id;
		var user_id = el.user_id;
		var el_wrap = {drawing : el, id : this.numElements , hidden :false};
		this.grid.add(el_wrap);
		this.numElements++;
		if(!(user_id in this.visible))
		{
			var s = {};
			s[draw_id] = [el_wrap];
			this.visible[user_id] = s;
		}
		else if(!(draw_id in this.visible[user_id]))
		{
			this.visible[user_id][draw_id] = [el_wrap];
		}
		else
		{
			this.visible[user_id][draw_id].push(el_wrap);
		}

		this.updateBoundingBox(el);
	}

	updateBoundingBox(el)
	{
		var bbox = boundingBox(el);
		if(this.numElements === 1)
		{
			this.bounding_box.x0 = bbox.x0;
			this.bounding_box.y0 = bbox.y0;
			this.bounding_box.x1 = bbox.x1;
			this.bounding_box.y1 = bbox.y1;
		}
		else
		{
			if(bbox.x0 < this.bounding_box.x0)
				this.bounding_box.x0 = bbox.x0;
			if(bbox.y0 < this.bounding_box.y0)
				this.bounding_box.y0 = bbox.y0;
			if(bbox.x1 > this.bounding_box.x1)
				this.bounding_box.x1 = bbox.x1;
			if(bbox.y1 > this.bounding_box.y1)
				this.bounding_box.y1 = bbox.y1;
		}
	}

	// Permanently remove an element.
	// Cannot be undone.
	remove(user_id, draw_id)
	{
		if(user_id in this.hidden)
			if(draw_id in this.hidden[user_id])
			{
				this.hidden[user_id][draw_id].hidden = true;
				delete this.hidden[user_id][draw_id];
			}
		if(user_id in this.visible)
			if(draw_id in this.visible[user_id])
			{
				this.visible[user_id][draw_id].hidden = true;
				delete this.visible[user_id][draw_id];
			}
	}

	// Restore hidden elements.
	// Returns the restored elements, if they exist. Otherwise, returns null
	reveal(user_id, draw_id)
	{
		if(user_id in this.hidden)
			if(draw_id in this.hidden[user_id])
			{
				var els = this.hidden[user_id][draw_id];
				this.visible[user_id][draw_id] = els;
				for(var i = 0 ; i < els.length ; ++i)
					els[i].hidden = false;
				delete this.hidden[user_id][draw_id];
				for(var i = 0 ; i < els.length ; ++i)
					this.grid.add(els[i]);
				return(els);
			}
		return [];
	}

	// Hide elements without removing them.
	hide(user_id, draw_id)
	{
		if(user_id in this.visible)
			if(draw_id in this.visible[user_id])
			{
				var els = this.visible[user_id][draw_id];
				if(!(user_id in this.hidden))
					this.hidden[user_id] = {};
				
				this.hidden[user_id][draw_id] = els;
				for(var i = 0; i < els.length ; ++i)
				{
					els[i].hidden = true;
				}
				delete this.visible[user_id][draw_id];
			}
	}

	hideElements(drawings)
	{
		for(var i = 0 ; i < drawings.length ; ++i)
		{
			this.hide(drawings[i].user_id, drawings[i].draw_id);
		}
	}

	// Hide all elements within the given rectangle.
	hideWithinRectangle(x,y,width,height)
	{
		var els = this.withinRectangle(x,y,width,height);
		for(var i = 0; i < els.length ; ++i)
			this.hide(els[i].user_id, els[i].draw_id);
		return(els);
	}

	// Hide all elements within the given distance of the given line.
	hideWithinLine(p0,p1,distance)
	{
		var els = this.withinLine(p0[0], p0[1], p1[0],p1[1], distance);
		for(var i = 0; i < els.length ; ++i)
			this.hide(els[i].user_id, els[i].draw_id);
		return(els);
	}

	// Returns all visible elements within the given rectangle.
	withinRectangle(x,y,width,height)
	{
		return (this.grid.withinRectangle(x,y,width,height));
	}

	// Returns all visible elements within the given distance of the given line.
	withinLine(x0,y0,x1,y1,distance)
	{
		return(this.grid.withinLineDistance(x0,y0,x1,y1,distance));
	}

	clear()
	{
		this.hidden = {};
		this.visible= {};
		this.grid.clear();
		this.numElements = 0;
		this.boundbing_box = {x0 : 0 , y0 : 0 , x1 : 0, y1 : 0};
	}

	getDrawings(ids)
	{
		var drawings = [];
		for(var i = 0 ; i < ids.length ; ++i)
		{
			var user_id = ids[i].user_id;
			var draw_id = ids[i].draw_id;
			if(user_id in this.visible)
			{
				if(draw_id in this.visible[user_id])
				{
					for(var i = 0; i < this.visible[user_id][draw_id].length ; ++i)
					{
						drawings.push(this.visible[user_id][draw_id][i].drawing);
					}
				}
			}
		}
		return drawings;
	}
}

