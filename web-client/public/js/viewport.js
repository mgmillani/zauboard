export class Viewport
{
	constructor(x, y, width, height, zoom)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.zoom = zoom;
		this.zoom_log = Math.log2(zoom);
		this.tracking = false;
		this.track_margin = 0.05;
		this.tracking_user_id = -1;
		var now = new Date();
		this.last_movement = now.getTime();
		this.tracking_time_frame = 2000; // 2 seconds
	}

	getCoords(pointerEvent)
	{
		var screen = {};
		screen.x = pointerEvent.clientX;
		screen.y = pointerEvent.clientY;
		//screen.x = (pointerEvent.offsetX || pointerEvent.pageX - $(pointerEvent.target).offset().left) + 1;
		//screen.y = pointerEvent.currY || (pointerEvent.offsetY || pointerEvent.pageY - $(pointerEvent.target).offset().top);
		var world = {};
		world.x = (screen.x - this.width/2)/ this.zoom + this.x;
		world.y = (screen.y - this.height/2) / this.zoom + this.y;
		return {screen, world};
	}

	resize(width, height)
	{
		this.width = width;
		this.height = height;
	}
	toScreenCoords(x,y)
	{
		var screenX = (x - this.x)*this.zoom + this.width/2;
		var screenY = (y - this.y)*this.zoom + this.height/2;
		return {screenX, screenY};
	}

	toggleTracking()
	{
		this.tracking = !this.tracking;
		return(this.tracking);
	}

	track(world, user_id)
	{
		var now = new Date()
		if(user_id !== this.tracking_user_id && now.getTime() - this.last_movement < this.tracking_time_frame)
		{
			return false;
		}
		this.last_movement = now.getTime();
		this.tracking_user_id = user_id;
		let {screenX, screenY} = this.toScreenCoords(world.x, world.y);
		var w0 = this.width * this.track_margin;
		var w1 = this.width * (1 - this.track_margin);
		var dx = 0;
		if(screenX < w0)
			dx = screenX - 3*w0;
		else if(screenX > w1)
			dx = screenX - w1 + 2*w0;
		var h0 = this.height * this.track_margin;
		var h1 = this.height * (1 - this.track_margin);
		var dy = 0;
		if(screenY < h0)
			dy = screenY - 3*h0;
		else if(screenY > h1)
			dy = screenY - h1 + 2*h0;
		return({dx : -dx / this.zoom, dy : -dy / this.zoom});
	}
}
