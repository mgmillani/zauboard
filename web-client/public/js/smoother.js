export class Smoother1D
{
	constructor(maxAccelleration)
	{
		if(typeof bufferSize === 'number')
			this.maxAcc = maxAcceleration;
		else
			this.maxAcc = 0.01;
		this.velocity = 0;
		this.position = 0;
		this.target = 0;
		this.d = 150;
		this.last_call = undefined;
	}
	step(now)
	{
		if(this.last_call === undefined)
		{
			this.last_call = now;
			return;
		}
		var dt = now - this.last_call;
		var dp = this.velocity * dt;
		// stop on target
		if(  (this.position < this.target && this.position + dp > this.target)
			|| (this.position > this.target && this.position + dp < this.target))
		{
			this.position = this.target;
			this.velocity = 0;
			return;
		}
		this.position += dp;
		this.last_call = now;
		var dv = (this.target - this.position) / this.d - this.velocity;
		if(dv > this.maxAcc)
			dv = this.maxAcc;
		else if(dv < -this.maxAcc)
			dv = -this.maxAcc;
		this.velocity += dv;
	}
	reset()
	{
		this.last_call = undefined;
		this.velocity = 0;
	}
}

export class Smoother2D
{
	constructor(maxAccelleration)
	{
		if(typeof maxAccelleration === 'number')
			this.maxAcc = maxAcceleration;
		else
			this.maxAcc = 1000;
		this.velocity = [0,0];
		this.position = [0,0];
		this.target = [0,0];
		this.d = 100;
		this.last_call = undefined;
	}
	step(now)
	{
		if(this.last_call === undefined)
		{
			this.last_call = now;
			return;
		}
		var dt = now - this.last_call;
		var stop = false;
		// find closest point to target in order to prevent passing through target
		var r = Math.sqrt(this.velocity[0]*this.velocity[0] + this.velocity[1]*this.velocity[1]);
		var ux = this.velocity[0] / r;
		var uy = this.velocity[1] / r;
		//console.log(this.velocity);
		if(r > 0.01)
		{
			var closestT;
			// handle degenerate cases
			if(Math.abs(ux) < 0.0001)
			{
				closestT = (this.target[1] - this.position[1])/this.velocity[1];
			}
			else if(Math.abs(uy) < 0.0001)
			{
				closestT = (this.target[0] - this.position[0])/this.velocity[0];
			}
			else
			{
				var p2 = this.target;
				var p1 = this.position;
				var a = (ux/uy + uy/ux)*r;
				//console.log(p1 + " -- " + p2);
				closestT = (((p2[0] - p1[0]) / uy) + ((p2[1] - p1[1])/ux))/a;
			}
			closestT = Math.max(0, closestT);
			if(closestT < dt)
			{
				dt = closestT;
				stop = true;
			}
		}
		var dp = [this.velocity[0] * dt, this.velocity[1] * dt];
		this.position = [this.position[0] + dp[0], this.position[1] + dp[1]];
		this.last_call = now;
		if(stop)
		{
			this.velocity[0] = 0;
			this.velocity[1] = 0;
		}
		var dv =
			[ (this.target[0] - this.position[0]) / this.d - this.velocity[0]
			, (this.target[1] - this.position[1]) / this.d - this.velocity[1]];
		var dd = dv[0]*dv[0] + dv[1]*dv[1];
		if(dd > this.maxAcc)
		{
			dv[0] *= this.maxAcc/dd;
			dv[1] *= this.maxAcc/dd;
		}
		this.velocity[0] += dv[0];
		this.velocity[1] += dv[1];
	}
	reset()
	{
		this.last_call = undefined;
		this.velocity = [0,0];
	}
	getVelocityMagnitudeSquared()
	{
		return this.velocity[0]*this.velocity[0] + this.velocity[1] * this.velocity[1];
	}
}
