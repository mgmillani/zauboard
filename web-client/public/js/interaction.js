import {ActionType, DrawingElement, makePathElement, makeCircleElement, boundingBoxList, isPointInsideRectangle, translateDrawing} from "./drawingElement.js";
import {Smoother1D, Smoother2D} from "./smoother.js";

export const ToolType = Object.freeze({"pen":0, "eraser":1, "circle":2, "line":3, "rectangle":4, "text":5, "pointer":6, "boxSelection":7 });

export class Cursor
{
	constructor(size, color, cursorContainer)
	{
		this.size = size;
		this.color = color;
		this.screen = {'x':0, 'y':0};
		this.rim = false;
		this.element = document.createElement('div');
		this.element.setAttribute("id","ownCursor");
		this.svgns = "http://www.w3.org/2000/svg";
		this.pointer = document.createElementNS(this.svgns, 'circle');
		this.pointer.setAttribute("fill","#000000");
		this.pointer.setAttribute("stroke","#000000");
		this.pointer.setAttribute("stroke-width","0");
		this.pointer.setAttribute("r","10");
		this.pointer.setAttribute("cx","10");
		this.pointer.setAttribute("cy","10");
		this.hint_svg = document.createElementNS(this.svgns, 'svg');
		this.hint = document.createElementNS(this.svgns, 'circle');
		this.hint_size = 5;
		this.hint_color = "#000000";
		this.hint.setAttribute("r",this.hint_size);
		this.hint.setAttribute("stroke-width","1.0");
		this.hint.setAttribute("fill","none");
		this.hint.setAttribute("stroke",this.hint_color);
		this.hint.setAttribute("cx","10");
		this.hint.setAttribute("cy","10");
		this.hint_svg.append(this.hint);
		this.hint_svg.append(this.pointer);
		this.redraw();
		cursorContainer.append(this.element);
		this.element.append(this.hint_svg);
	}
	setPos(x,y)
	{
		this.screen.x = x;
		this.screen.y = y;
		var c = this.size < this.hint_size ? this.hint_size : this.size;
		this.pointer.setAttribute("cx",c);
		this.pointer.setAttribute("cy",c);
		this.hint.setAttribute("cx",c);
		this.hint.setAttribute("cy",c);
		var left = this.screen.x - c;
		var top = this.screen.y - c;
		this.element.style.left = left + "px";
		this.element.style.top = top + "px";
	}
	setRim(flag)
	{
		if(this.rim === flag)
			return;
		this.rim = flag;
		if(this.rim === true)
		{
			this.pointer.setAttribute("stroke-width","1");
		}
		else
		{
			this.pointer.setAttribute("stroke-width","0");
			this.size = this.size + 1;
		}
	}
	setSize(size)
	{
		if(this.rim === true)
			this.size = size - 1;
		else
			this.size = size;
		this.pointer.setAttribute("r",size/2);
		this.setPos(this.screen.x, this.screen.y);
	}
	setColor(color)
	{
		this.color = color;
		this.pointer.setAttribute("fill", color);
	}
	setHintColor(color)
	{
		this.hint_color = color;
		this.hint.setAttribute("stroke", color);
	}
	hideHint()
	{
		this.hint.setAttribute("stroke","none");
	}
	showHint()
	{
		this.hint.setAttribute("stroke", this.hint_color);
	}
	redraw()
	{
		var left = this.screen.x - this.size/2;
		var top = this.screen.y - this.size/2;
		this.element.setAttribute("style", "position:absolute;top:" + top + "px;left:" + left + "px;");
	}
	hide()
	{
		this.element.style.width = "0px";
		this.element.style.height = "0px";
		this.element.style.left = "0px";
		this.element.style.top = "0px";
	}
}

export class ZoomAndPanHandler
{
	constructor(viewport, pointers, bounding_box)
	{
		this.pointers = pointers;
		this.viewport = viewport;
		this.zoomSmoother = new Smoother1D();
		this.zoomSmoother.position = viewport.zoom_log;
		this.zoomSmoother.target   = viewport.zoom_log;
		this.positionSmoother = new Smoother2D();
		this.positionSmoother.position = [0,0];
		this.positionSmoother.target = [0,0];
		this.acceleration = [0,0];
		this.zoom_acceleration = 0;
		this.zoom_center = {x:0,y:0};
		this.zooming = true;
		this.center_on_drawing = false;
		this.bounding_box = bounding_box;
		this.margin = 400;
	}

	start()
	{
		this.zoomSmoother.reset();
		this.zoomSmoother.position = this.viewport.zoom_log;
		this.zoomSmoother.target = this.zoomSmoother.position;
		this.initialCamera = { x: this.viewport.x, y: this.viewport.y};
		this.positionSmoother.reset();
		this.positionSmoother.position = [0,0];
		this.positionSmoother.target = this.positionSmoother.position;
	}

	step()
	{
		var now = new Date();
		this.applyMove(this.acceleration[0], this.acceleration[1]);
		if(this.zoom_acceleration !== 0)
			this.applyZoom(this.zoom_acceleration, this.zoom_center);
		this.zoomSmoother.step(now.getTime());
		this.positionSmoother.position = [0,0];
		this.positionSmoother.step(now.getTime());
		this.positionSmoother.target =
			[ this.positionSmoother.target[0] - this.positionSmoother.position[0]
			, this.positionSmoother.target[1] - this.positionSmoother.position[1]];
	}

	setAcceleration(dx,dy)
	{
		this.acceleration = [dx,dy];
	}

	setZoomAcceleration(zoom_diff, zoom_center)
	{
		this.zoom_acceleration = zoom_diff
		this.zoom_center = zoom_center;
	}

	toggleZooming()
	{
		this.zooming = !this.zooming;
		return(this.zooming);
	}

	toggleCenterOnDrawing()
	{
		this.center_on_drawing = !this.center_on_drawing;
		if(this.center_on_drawing)
		{
			this.centerOnDrawing();
		}
		return(this.center_on_drawing);
	}

	applyMove(dx,dy)
	{
		if(Math.abs(dx + dy) > 0.01)
		{
			window.debug.logValues("panning",{dx : dx, dy : dy});
		}
		this.positionSmoother.target =
			[ this.positionSmoother.target[0] + dx
			, this.positionSmoother.target[1] + dy];
		if(this.center_on_drawing)
		{
			this.centerOnDrawing();
		}
	}

	centerOnDrawing()
	{
		var vx0 = this.viewport.x - this.viewport.width / (this.viewport.zoom * 2);
		var vx1 = this.viewport.x + this.viewport.width / (this.viewport.zoom * 2);
		var vy0 = this.viewport.y - this.viewport.height / (this.viewport.zoom * 2);
		var vy1 = this.viewport.y + this.viewport.height / (this.viewport.zoom * 2);
		// if bounding box is smaller than the viewport, move to the center of the drawing
		if(this.bounding_box.x1 - this.bounding_box.x0 < this.viewport.width / (this.viewport.zoom) - this.margin)
		{
			this.positionSmoother.target[0] = this.viewport.x - (this.bounding_box.x1 + this.bounding_box.x0)/2;
		}
		else
		{
			if(vx1 - this.positionSmoother.target[0] > this.bounding_box.x1 + this.margin)
			{
				this.positionSmoother.target[0] = vx1 - (this.bounding_box.x1 + this.margin)
			}
			else if(vx0 - this.positionSmoother.target[0] < this.bounding_box.x0 - this.margin)
			{
				this.positionSmoother.target[0] = vx0 - (this.bounding_box.x0 - this.margin);
			}
		}

		// if bounding box is smaller than the viewport, move to the center of the drawing
		if(this.bounding_box.y1 - this.bounding_box.y0 < this.viewport.height / (this.viewport.zoom) - this.margin)
		{
			this.positionSmoother.target[1] = this.viewport.y - (this.bounding_box.y1 + this.bounding_box.y0)/2;
		}
		else
		{
			//
			if(vy1 - this.positionSmoother.target[1] > this.bounding_box.y1 + this.margin)
			{
				this.positionSmoother.target[1] = vy1 - (this.bounding_box.y1 + this.margin);
			}
			else if(vy0 - this.positionSmoother.target[1] < this.bounding_box.y0 - this.margin)
			{
				this.positionSmoother.target[1] = vy0 - (this.bounding_box.y0 - this.margin);
			}
		}
	}

	moveTo(x,y)
	{
		this.positionSmoother.target = [x,y];
	}

	applyZoom(zoomDiff, zoomCenter)
	{
		if(this.zooming)
		{
			this.pinchCenter = {x:zoomCenter.x, y:zoomCenter.y};
			this.zoomSmoother.target = this.zoomSmoother.target + zoomDiff;
			if(this.zoomSmoother.target > 3)
				this.zoomSmoother.target = 3;
			else if(this.zoomSmoother.target < -3)
				this.zoomSmoother.target = -3;
		}
	}

	getNewCamera()
	{
		var pos = this.positionSmoother.position;
		var pinch = this.pinchCenter;
		var zoom = this.zoomSmoother.position;
		var newCamera =
			{ pinch: pinch
			, dx:pos[0], dy:pos[1]
			, zoom_log:zoom};
		return newCamera;
	}
}

class FloatingButtons
{
	constructor(svg_container, buttons)
	{
		this.visible = false;
		this.svgns = "http://www.w3.org/2000/svg";
		this.svg_container = svg_container;
		this.buttons = {};
		for(let b in buttons)
		{
			var id = buttons[b].id;
			var dom_id = "FloatingButton-" + id;
			var bg_el = document.createElementNS(this.svgns, 'circle');
			var bg_radius = 15;
			bg_el.setAttribute('id', dom_id);
			bg_el.setAttribute('class', 'FloatingButton');
			//var center = [ this.box_start[0] + this.box_size[0] + this.duplicate_button_radius + 10
			//															 , this.box_start[1] + this.duplicate_button_radius
			//															 ]
			//this.duplicate_button_element.setAttribute('cx', this.duplicate_button_center[0]);
			//this.duplicate_button_element.setAttribute('cy', this.duplicate_button_center[1]);
			bg_el.setAttribute('r', bg_radius);
			this.buttons[id] = 
				{ 'fg' : buttons[b].fg
				, 'radius' : bg_radius
				, 'bg' : bg_el
				, 'center' : [0,0]
				};
		}
	}

	show(world, zoom)
	{
		if(!this.visible)
		{
      var scale = 1/zoom;
			this.visible = true;
			var h = 0;
			for(let b in this.buttons)
			{
				var button = this.buttons[b];
				var r = button.radius * scale;
				h += r;
				button.center = [world.x + r, world.y + h ];
				h += (button.radius + 5) * scale;
				button.bg.setAttribute('class','FloatingButton');
				button.bg.setAttribute('cx', 0);
				button.bg.setAttribute('cy', 0);
				button.bg.setAttribute('transform', 'translate(' + button.center[0] + ', ' + button.center[1] + ') scale(' + scale + ')');
				button.fg.setAttribute('transform', 'translate(' + button.center[0] + ', ' + button.center[1] + ') scale(' + scale + ')');
				button.fg.setAttribute('class','FloatingButtonFG');
				this.svg_container.append(button.bg);
				this.svg_container.append(button.fg);
			}
		}
	}

	hide()
	{
		if(this.visible)
		{
			this.visible = false;
			for(let b in this.buttons)
			{
				this.svg_container.removeChild(this.buttons[b].bg);
				this.svg_container.removeChild(this.buttons[b].fg);
			}
		}
	}

	hover(world)
	{
		var found = null;
		if(this.visible)
		{
			for(let b in this.buttons)
			{
				var button = this.buttons[b];
				var r = button.radius;
				var dx = world.x - button.center[0];
				var dy = world.y - button.center[1];
				if(dx*dx + dy*dy < r*r)
				{
					button.bg.setAttribute('class','FloatingButtonSelected');
					button.fg.setAttribute('class','FloatingButtonFGSelected');
					found = b;
				}
				else
				{
					button.bg.setAttribute('class','FloatingButton');
					button.fg.setAttribute('class','FloatingButtonFG');
				}
			}
		}
		return(found);
	}
}

class BoxSelectionTool
{
	constructor(cursor, viewport, user, svg_container, drawing_container)
	{
		this.svgns = "http://www.w3.org/2000/svg";
		this.cursor = cursor;
		this.viewport = viewport;
		this.user = user;
		this.svg_container = svg_container;
		this.svg_element = null;
		this.duplicate_button_element = null;
		this.drawing_container = drawing_container;
		this.state = 'unselected';
		this.selected_elements = [];
		this.box_end = [0,0];
		this.box_size = [0,0];
		this.box_start = [0,0];
    this.duplicate_time = null;
    this.epsilon_t = 350;
    this.epsilon_d = 5;
		this.button_selected = null;
		this.isPaused = false;

		var plus_svg = document.createElementNS(this.svgns,'line');
		plus_svg.setAttribute('x1',0);
		plus_svg.setAttribute('y1',-8);
		plus_svg.setAttribute('x2',0);
		plus_svg.setAttribute('y2',8);
		plus_svg.setAttribute('class','FloatingButton');
		var plus_svg_2 = document.createElementNS(this.svgns,'line');
		plus_svg_2.setAttribute('x1',-8);
		plus_svg_2.setAttribute('y1',0);
		plus_svg_2.setAttribute('x2',8);
		plus_svg_2.setAttribute('y2',0);
		plus_svg_2.setAttribute('class','FloatingButton');
		var duplicate_svg = document.createElementNS(this.svgns,'g');
		duplicate_svg.append(plus_svg);
		duplicate_svg.append(plus_svg_2);

		var cross_svg = document.createElementNS(this.svgns,'line');
		cross_svg.setAttribute('x1',-6);
		cross_svg.setAttribute('y1',-6);
		cross_svg.setAttribute('x2',6);
		cross_svg.setAttribute('y2',6);
		cross_svg.setAttribute('class','FloatingButton');
		var cross_svg_2 = document.createElementNS(this.svgns,'line');
		cross_svg_2.setAttribute('x1',6);
		cross_svg_2.setAttribute('y1',-6);
		cross_svg_2.setAttribute('x2',-6);
		cross_svg_2.setAttribute('y2',6);
		cross_svg_2.setAttribute('class','FloatingButton');
		var delete_svg = document.createElementNS(this.svgns, 'g');
		delete_svg.append(cross_svg);
		delete_svg.append(cross_svg_2);
		this.floating_buttons = new FloatingButtons(svg_container,
			[ {'id' : 'duplicate', 'fg' : duplicate_svg}
			, {'id' : 'delete', 'fg' : delete_svg}
			]);
	}

	pointerDown(screen, world, pressure, pressureFactor)
	{
		var svgns = "http://www.w3.org/2000/svg";
		switch(this.state)
		{
			case 'unselected':
				this.state = 'selecting';
				this.box_start = [world.x, world.y];
				if(this.svg_element === null)
				{
					this.svg_element = document.createElementNS(svgns, 'rect');
					this.svg_container.append(this.svg_element);
				}
				// this.svg_container.getElementById("SelectionBox").remove();
				this.svg_element.setAttribute('id', 'SelectionBox');
				this.svg_element.setAttribute('stroke', '#7373e6');
				this.svg_element.setAttribute('fill', '#7373e6');
				this.svg_element.setAttribute('stroke-width', '1');
				this.svg_element.setAttribute('stroke-dasharray', '5, 5');
				this.svg_element.setAttribute('style', 'fill-opacity:0.1;');
				this.svg_element.setAttribute('x', world.x);
				this.svg_element.setAttribute('y', world.y);
				this.svg_element.setAttribute('width', 0);
				this.svg_element.setAttribute('height', 0);

				break;
			case 'selected':
				this.button_selected = this.floating_buttons.hover(world);
				if(isPointInsideRectangle(world.x, world.y, this.box_start[0], this.box_start[1], this.box_size[0], this.box_size[1]))
				{
					this.state = 'moving';
          this.move_start = [world.x , world.y];
          this.move_diff = [this.box_start[0] - world.x, this.box_start[1] - world.y];
					this.floating_buttons.hide();
				}
				else if(this.button_selected !== null)
				{
					if(this.button_selected === 'duplicate')
					{
						this.selected_elements = this.user.duplicateDrawings(this.selected_elements);
						this.state = 'moving';
						this.move_start = [world.x , world.y];
						this.move_diff = [this.box_start[0] - world.x, this.box_start[1] - world.y];
						this.floating_buttons.hide();
            this.duplicate_time = (new Date()).getTime();
					}
					else if(this.button_selected === 'delete')
					{
						this.user.hideDrawings(this.selected_elements);
						this.selected_elements = [];
						this.svg_element.remove();
						this.svg_element = null;
						this.state = 'unselected';
						this.floating_buttons.hide();
					}
					this.button_selected = null;
				}
				else
				{
					this.selected_elements = [];
					this.svg_element.remove();
					this.svg_element = null;
					this.state = 'selecting';
					this.floating_buttons.hide();
					this.box_start = [world.x, world.y];
					if(this.svg_element === null)
					{
						this.svg_element = document.createElementNS(svgns, 'rect');
						this.svg_container.append(this.svg_element);
					}
					// this.svg_container.getElementById("SelectionBox").remove();
					this.svg_element.setAttribute('id', 'SelectionBox');
					this.svg_element.setAttribute('stroke', '#7373e6');
					this.svg_element.setAttribute('fill', '#7373e6');
					this.svg_element.setAttribute('stroke-width', '1');
					this.svg_element.setAttribute('stroke-dasharray', '5, 5');
					this.svg_element.setAttribute('style', 'fill-opacity:0.1;');
					this.svg_element.setAttribute('x', world.x);
					this.svg_element.setAttribute('y', world.y);
					this.svg_element.setAttribute('width', 0);
					this.svg_element.setAttribute('height', 0);
				}
				break;
		}
	}
	pointerMove(screen, world, pressure, pressureFactor)
	{
		switch(this.state)
		{
			case 'selected':
				this.button_selected = this.floating_buttons.hover(world);
				break;
			case 'selecting':
				var width = Math.abs(world.x - this.box_start[0]);
				var height = Math.abs(world.y - this.box_start[1]);
				var x = world.x < this.box_start[0] ? world.x : this.box_start[0];
				var y = world.y < this.box_start[1] ? world.y : this.box_start[1];
				var coords = this.viewport.toScreenCoords(x,y);
				this.svg_element.setAttribute('x', x);
				this.svg_element.setAttribute('y', y);
				this.svg_element.setAttribute('width', width);
				this.svg_element.setAttribute('height', height);
				break;
      case 'moving':
        this.box_start[0] = world.x + this.move_diff[0];
        this.box_start[1] = world.y + this.move_diff[1];
        this.moveSelectionBox();
        break;
		}
	}
	pointerUp(screen, world)
	{
		var svgns = "http://www.w3.org/2000/svg";
		switch(this.state)
		{
			case 'selecting':
				this.state = 'selected';
				var x0,y0,w,h;
				if(this.box_start[0] > world.x)
				{
					x0 = world.x;
					w = this.box_start[0] - x0;
				}
				else
				{
					x0 = this.box_start[0];
					w = world.x - x0;
				}
				if(this.box_start[1] > world.y)
				{
					y0 = world.y;
					h = this.box_start[1] - y0;
				}
				else
				{
					y0 = this.box_start[1];
					h = world.y - y0;
				}
				this.selected_elements = this.drawing_container.withinRectangle(x0,y0,w,h);
				if(this.selected_elements.length > 0)
				{
					var bbox = boundingBoxList(this.selected_elements);
					if(bbox !== undefined)
					{
						this.svg_element.setAttribute('x', bbox.x0);
						this.svg_element.setAttribute('y', bbox.y0);
						this.svg_element.setAttribute('width', bbox.x1 - bbox.x0);
						this.svg_element.setAttribute('height', bbox.y1 - bbox.y0);
					}
					this.box_start[0] = bbox.x0;
					this.box_start[1] = bbox.y0;
					this.box_size[0] = bbox.x1 - bbox.x0;
					this.box_size[1] = bbox.y1 - bbox.y0;

          this.buttons_position = {'x' : this.box_start[0] + 10 * this.viewport.zoom + this.box_size[0], 'y' : this.box_start[1]};
					this.floating_buttons.show(this.buttons_position, this.viewport.zoom);

				}
				else
				{
					this.state = 'unselected';
				}
				break;
			case 'moving':
        var dx = world.x - this.move_start[0];
        var dy = world.y - this.move_start[1];
        var now = (new Date()).getTime();
        // user just clicked the duplicate button
        if(this.duplicate_time !== null)
        {
          if(now - this.duplicate_time < this.epsilon_t && Math.abs(dx) + Math.abs(dy) < this.epsilon_d)
          {
            dx += this.epsilon_d * 2;
            dy += this.epsilon_d * 2;
            this.move_diff[0] += this.epsilon_d * 2;
            this.move_diff[1] += this.epsilon_d * 2;
          }
          this.duplicate_time = null;
        }
        this.selected_elements = this.user.moveDrawings(this.selected_elements, dx, dy);
        this.box_start[0] = world.x + this.move_diff[0];
        this.box_start[1] = world.y + this.move_diff[1];
        this.moveSelectionBox();
				this.state = 'selected';
        this.buttons_position = {'x' : this.box_start[0] + 10 * this.viewport.zoom + this.box_size[0], 'y' : this.box_start[1]};
				this.floating_buttons.show(this.buttons_position, this.viewport.zoom);
				break;
		}
	}
  moveSelectionBox()
  {
    this.svg_element.setAttribute('x', this.box_start[0]);
    this.svg_element.setAttribute('y', this.box_start[1]);
  }

	showButtons()
	{
		var svgns = "http://www.w3.org/2000/svg";
		// Add duplicate button
		if(this.duplicate_button_element === null)
		{
			this.duplicate_button_element = document.createElementNS(svgns, 'circle');
			this.svg_container.append(this.duplicate_button_element);
		}
		this.duplicate_button_radius = 15;
		this.duplicate_button_element.setAttribute('id', 'DuplicateSelection');
		this.duplicate_button_element.setAttribute('class', 'FloatingButton');
		this.duplicate_button_center = [ this.box_start[0] + this.box_size[0] + this.duplicate_button_radius + 10
																	 , this.box_start[1] + this.duplicate_button_radius
																	 ]
		this.duplicate_button_element.setAttribute('cx', this.duplicate_button_center[0]);
		this.duplicate_button_element.setAttribute('cy', this.duplicate_button_center[1]);
		this.duplicate_button_element.setAttribute('r', this.duplicate_button_radius);
	}
	hideButtons()
	{
		this.duplicate_button_element.setAttribute('r','0px');
	}

	updateCursor(pressureFactor)
	{
	}

	hasDrawing()
	{
		return false;
	}

	commit()
	{
	}

	abort()
	{
		this.state = 'unselected';
		if(this.svg_element !== null)
		{
			this.svg_element.remove();
			this.svg_element = null;
		}
		this.selected_elements = [];
		this.floating_buttons.hide();
	}

	pause()
	{
		if(this.state === 'selected')
		{
			this.floating_buttons.hide();
		}
		this.isPaused = true;
	}

	resume()
	{
		if(this.state === 'selected')
		{
			this.floating_buttons.show(this.buttons_position, this.viewport.zoom);
		}
		this.isPaused = false;
	}

	turnModifierOn()
	{
	}
	turnModifierOff()
	{
	}
}

class EraserTool
{
	constructor(baseThickness, cursor, viewport, user)
	{
		this.baseThickness = baseThickness;
		this.cursor = cursor;
		this.viewport = viewport;
		this.user = user;
		this.drawing = false;
		this.position = [0,0];
		this.positionSmoother = new Smoother2D();
		this.isPaused = false;
	}
	setThickness(thickness)
	{
		this.baseThickness = thickness;
		this.cursor.setSize(this.baseThickness * this.viewport.zoom);
	}
	setColor(color){ };
	pointerUp()
	{
		this.drawing = false;
	}
	pointerDown(screen, world, pressure, pressureFactor)
	{
		this.drawing = true;
		this.position[0] = world.x;
		this.position[1] = world.y;
	}
	pointerMove(screen, world, pressure, pressureFactor)
	{
		var thickness = this.baseThickness * pressureFactor;
		if(this.drawing)
		{
			this.user.eraseAroundLine(this.position, [world.x, world.y], thickness/2);
			this.position[0] = world.x;
			this.position[1] = world.y;
		}
		this.cursor.setPos(screen.x, screen.y);
		this.cursor.setSize(thickness * this.viewport.zoom);
	}
	commit(readyElements){}
	abort()
	{
		this.drawing = false;
	}
	turnModifierOn(){}
	turnModifierOff(){}
	hasDrawing()
	{
		return false;
	}
	updateCursor(pressureFactor)
	{
		var width = pressureFactor * this.baseThickness * this.viewport.zoom;
		this.cursor.setRim(true);
		this.cursor.setSize(width);
		this.cursor.setColor("#FFFFFF");
	}
	pause()
	{
		this.drawing = false;
	}
	resume(){}
}

export class PathTool
{
	constructor(type, baseThickness, cursor, screenPainter, viewport, user)
	{
		this.activePath = [];
		this.baseThickness = baseThickness;
		this.thicknessFactor = 1;
		this.cursor = cursor;
		this.drawing = false;
		this.minPathLength = 20;
		this.positionSmoother = new Smoother2D();
		this.readyElements = [];
		this.screenPainter = screenPainter;
		this.thicknesses = [];
		this.type = type;
		this.viewport = viewport;
		this.user = user;
		this.isPaused = false;
		this.epsilon = 1.0;
		this.referenceEpsilon = 0.75;
		this.drawing_time = (new Date()).getTime();
		this.drawing_minimum_delta = 200;
		this.minimum_distance_square = 1.0;
	}

	setThickness(thickness)
	{
		this.baseThickness = thickness;
		this.cursor.setSize(this.baseThickness * this.thicknessFactor * this.viewport.zoom);
	}
	setColor(color)
	{
		this.color = color;
		this.cursor.setColor(this.color);
	}

	pointerUp(screen, world)
	{
		this.cursor.showHint();
		if(this.activePath.length > 0)
		{
			if(this.type === ActionType.curve)
			{
				// add last point twice for calculation of last control point
				this.activePath.push(this.activePath[this.activePath.length - 1]);
				this.simplifyActivePath(1, this.activePath.length - 2);
			}
			else
			{
				this.simplifyActivePath(1, this.activePath.length - 1);
			}
			this.finishPath();
		}
		this.activePath.splice(0, this.activePath.length);
		this.thicknesses.splice(0, this.thicknesses.length);
		this.drawing = false;
		this.screenPainter.clearWhiteboard();
	}

	pointerDown(screen, world, pressure, pressureFactor)
	{
		this.cursor.hideHint();
		this.drawing = true;
		this.drawing_time = (new Date()).getTime();
		// add first point and first control point
		if(this.type === ActionType.curve)
			this.activePath.push([world.x, world.y], [world.x, world.y]);
		else
			this.activePath.push([world.x, world.y]);
		this.thicknesses.push(this.thicknessFactor * this.baseThickness * pressureFactor);
		this.thicknesses.push(this.thicknessFactor * this.baseThickness * pressureFactor);
	}

	pointerMove(screen, world, pressure, pressureFactor)
	{
		var thickness = this.thicknessFactor * this.baseThickness * pressureFactor;
		if(this.drawing)
		{
			var n = this.activePath.length;
			var dx = this.activePath[n-1][0] - world.x;
			var dy = this.activePath[n-1][1] - world.y;
			var distanceSquare = dx*dx + dy*dy;
			if(dx !== 0.0 || dy !== 0.0)
			{
				this.screenPainter.transformContext();
				this.screenPainter.drawPenLine(this.activePath[n-1][0], this.activePath[n-1][1], world.x, world.y, this.color, thickness);
			}
			if(distanceSquare >= this.minimum_distance_square * thickness * thickness)
			{
				// add point to path
				this.activePath.push([world.x, world.y]);
				this.thicknesses.push(thickness);
			}
		}
		this.cursor.setPos(screen.x, screen.y);
		this.cursor.setSize(thickness * this.viewport.zoom)
	}

	commit()
	{
		if(this.type === ActionType.curve)
			this.simplifyActivePath(1, this.activePath.length - 1);
		else
			this.simplifyActivePath(0, this.activePath.length - 1);
		if(this.activePath.length > this.minPathLength && (new Date()).getTime() - this.drawing_time > this.drawing_minimum_delta)
		{
			this.finishPath();
		}
	}

	finishPath(preserveLast)
	{
		var el = makePathElement(this.type, this.activePath.slice(0, this.activePath.length), this.color, this.thicknesses.slice(0, this.thicknesses.length));
		if(this.type === ActionType.curve)
		{
			this.activePath.splice(0, this.activePath.length - 2);
			this.activePath.unshift(el.controlPoints[el.controlPoints.length - 1]);
		}
		else
			this.activePath.splice(0, this.activePath.length - 1);
		this.thicknesses.splice(0, this.thicknesses.length - 2);
		if(el.points.length > 1)
		{
			this.user.insertDrawing(el);
		}
	}

	simplifyActivePath(first, last)
	{
		if(last <= first + 1)
			return;
		var tLen = this.thicknesses.length;
		var p0 = this.activePath[first];
		var pn = this.activePath[last];
		var dx = pn[0] - p0[0];
		var dy = pn[1] - p0[1];
		var l = Math.sqrt(dx*dx + dy*dy);
		var ux = dx / l;
		var uy = dy / l;
		// perpendicular vector
		var wx = uy;
		var wy = -ux;

		// find point furthest away from line
		var d = 0;
		var i_max = first;
		if(l === 0)
		{
			for(var i = first + 1 ; i < last ; ++i)
			{
				var dx0 = p0[0] - this.activePath[i][0];
				var dy0 = p0[1] - this.activePath[i][1];
				// var t1 = Math.sqrt(dx0*dx0 + dy0*dy0) - (this.thicknesses[i % tLen] + this.thicknesses[first % tLen])/2;
				var t1 = Math.sqrt(dx0*dx0 + dy0*dy0);
				if(t1 > d)
				{
					d = t1;
					i_max = i;
				}
			}
		}
		else if(wx === 0)
		{
			for(var i = first + 1 ; i < last ; ++i)
			{
				// var t0 = (this.activePath[i][0] - p0[0]) / ux;
				// var t1 = (this.activePath[i][1] - p0[1]) * wy;
				var t1 = (this.activePath[i][1] - p0[1]);
				var at1 = Math.abs(t1);
				//var at1 = Math.abs(t1) - (this.thicknesses[i % tLen] + this.thicknesses[first % tLen])/2;
				if(at1 > d)
				{
					d = at1;
					i_max = i;
				}
			}
		}
		else
		{
			for(var i = first + 1 ; i < last ; ++i)
			{
				var a = wy / wx;
				var t0 = (p0[1] - this.activePath[i][1] + (this.activePath[i][0] - p0[0]) * a) / (ux * a - uy);
				var t1 = (p0[0] - this.activePath[i][0] + (t0 * ux)) / wx;
				// var tPercent = (t0 / l);
				// var at1 = Math.abs(t1) - (this.thicknesses[i % tLen] + (this.thicknesses[first % tLen] * (1 - tPercent) + tPercent * this.thicknesses[last % tLen]))/2;
				var at1 = Math.abs(t1);
				if(at1 > d)
				{
					d = at1;
					i_max = i;
				}
			}
		}

		// replace subpath with a line if d is small
		if(d < this.epsilon)
		{
			this.activePath.splice(first+1, last - first - 1);
			this.thicknesses.splice(first+1, last - first - 1);
		}
		// otherwise, recurse first on the right side of activePath to avoid shifiting indices
		else
		{
			this.simplifyActivePath(i_max, last);
			this.simplifyActivePath(first, i_max);
		}

	}
	abort()
	{
		this.activePath.splice(0, this.activePath.length);
		this.thicknesses.splice(0, this.thicknesses.length);
		this.drawing = false;
	}

	setEpsilon(zoom)
	{
		this.epsilon = this.referenceEpsilon / zoom;
	}
	turnModifierOn(){}
	turnModifierOff(){}

	hasDrawing()
	{
		return (this.activePath.length > 0);
	}
	updateCursor(pressureFactor)
	{
		var width = pressureFactor * this.baseThickness * this.viewport.zoom * this.thicknessFactor;
		this.cursor.setRim(false);
		this.cursor.setSize(width);
		this.cursor.setColor(this.color);
	}
	pause()
	{
		this.abort();
	}
	resume(){}
}

class ShapeTool
{
	constructor(type, baseThickness, viewport, screenPainter, svgContainer, user)
	{
		this.viewport = viewport;
		this.screenPainter = screenPainter;
		this.svgContainer = svgContainer;
		this.type = type;
		this.readyElements = [];
		this.shapeStart = undefined;
		this.pressureFactor = 2;
		this.minPressure = 0.15;
		this.baseThickness = baseThickness;
		this.thickness = baseThickness;
		this.modifierOn = false;
		this.svgElement = undefined;
		this.user = user;
		this.isPaused = false;
	}

	setThickness(thickness)
	{
		this.baseThickness = thickness;
	}

	pointerDown(screen, world, pressure)
	{
		this.drawing = true;
		this.shapeStart = [world.x, world.y];

		var svgns = "http://www.w3.org/2000/svg";
		switch(this.type)
		{
			case ToolType.line:
				this.svgElement = document.createElementNS(svgns, 'line');
				this.svgElement.setAttribute('stroke', 'gray');
				this.svgElement.setAttribute('stroke-dasharray', '5, 5');
				this.svgElement.setAttribute('x1', screen.x);
				this.svgElement.setAttribute('y1', screen.y);
				this.svgElement.setAttribute('x2', screen.x);
				this.svgElement.setAttribute('y2', screen.y);
				this.svgContainer.append(this.svgElement);
				break;
			case ToolType.circle:
				this.svgElement = document.createElementNS(svgns, 'circle');
				this.svgElement.setAttribute('stroke', 'gray');
				this.svgElement.setAttribute('stroke-dasharray', '5, 5');
				this.svgElement.setAttribute('style', 'fill-opacity:0.0;');
				this.svgElement.setAttribute('cx', screen.x);
				this.svgElement.setAttribute('cy', screen.y);
				this.svgElement.setAttribute('r', 0);
				this.svgContainer.append(this.svgElement);
				break;
			case ToolType.rectangle:
				this.svgContainer.find("rect").remove();
				this.svgElement = document.createElementNS(svgns, 'rect');
				this.svgElement.setAttribute('stroke', 'gray');
				this.svgElement.setAttribute('stroke-dasharray', '5, 5');
				this.svgElement.setAttribute('style', 'fill-opacity:0.0;');
				this.svgElement.setAttribute('x', screen.x);
				this.svgElement.setAttribute('y', screen.y);
				this.svgElement.setAttribute('width', 0);
				this.svgElement.setAttribute('height', 0);
				this.svgContainer.append(this.svgElement);
				break;
		}
	}

	pointerUp(screen, world)
	{
		if(this.shapeStart === undefined)
			return;
		var to_x = world.x;
		var to_y = world.y;
		var dx = to_x - this.shapeStart[0];
		var dy = to_y - this.shapeStart[1];
		switch(this.type)
		{
			case ToolType.circle:
				var radius = Math.sqrt(dx*dx + dy*dy);
				this.svgContainer.find("circle").remove();
				this.user.insertDrawing(makeCircleElement(this.shapeStart.slice(0,2), radius, this.color, this.thickness));
				break;
			case ToolType.line:
				if (this.modifierOn) {
					var angs = getRoundedAngles(dx, dy);
					to_x = angs.x + this.shapeStart[0];
					to_y = angs.y + this.shapeStart[1];
				}
				this.user.insertDrawing(makePathElement(ActionType.path, [[this.shapeStart[0], this.shapeStart[1]], [to_x, to_y]], this.color, [this.thickness]));
				//
				this.svgContainer.find("line").remove();
				break;
			case ToolType.rectangle:
				if (this.modifierOn) {
					if ((to_y - this.shapeStart[1]) * (to_x - this.shapeStart[0]) > 0) {
						to_y = this.shapeStart[1] + (to_x - this.shapeStart[0]);
					} else {
						to_x = this.shapeStart[1] - (to_x - this.shapeStart[0]);
					}
				}
				this.svgContainer.find("rect").remove();
				this.user.insertDrawing(makePathElement(ActionType.path,
					[ [this.shapeStart[0], this.shapeStart[1]]
					, [this.shapeStart[0], to_y]
					, [to_x, to_y]
					, [to_x, this.shapeStart[1]]
					, [this.shapeStart[0], this.shapeStart[1]]
					], this.color, [this.thickness])); 
				break;
		}
		this.shapeStart = undefined;
		this.thickness = 0;
	}

	pointerMove(screen, world, pressure, pressureFactor)
	{
		if(this.shapeStart !== undefined)
			this.thickness = Math.max(this.thickness, this.baseThickness * pressureFactor);
		var x = screen.x;
		var y = screen.y;
		if(this.shapeStart !== undefined)
		{
			switch(this.type)
			{
				case ToolType.line:
					var coords;
					if (this.modifierOn)
					{
						var angs = getRoundedAngles(world.x - this.shapeStart[0], world.y - this.shapeStart[1]);
						x = angs.x + this.shapeStart[0];
						y = angs.y + this.shapeStart[1];
						coords = this.viewport.toScreenCoords(x,y);
					}
					else
					{
						coords = {screenX : x, screenY : y};
					}
					this.svgElement.setAttribute('x2', coords.screenX);
					this.svgElement.setAttribute('y2', coords.screenY);
					break;
				case ToolType.rectangle:
					var width = Math.abs(world.x - this.shapeStart[0]);
					var height = Math.abs(world.y - this.shapeStart[1]);
					if (this.modifierOn)
					{
						height = width;
						x = world.x < this.shapeStart[0] ? this.shapeStart[0] - width : this.shapeStart[0];
						y = world.y < this.shapeStart[1] ? this.shapeStart[1] - width : this.shapeStart[1];
					}
					else
					{
						x = world.x < this.shapeStart[0] ? world.x : this.shapeStart[0];
						y = world.y < this.shapeStart[1] ? world.y : this.shapeStart[1];
					}
					var coords = this.viewport.toScreenCoords(x,y);
					this.svgElement.setAttribute('x', coords.screenX);
					this.svgElement.setAttribute('y', coords.screenY);
					this.svgElement.setAttribute('width', width * this.viewport.zoom);
					this.svgElement.setAttribute('height', height * this.viewport.zoom);
					break;
				case ToolType.circle:
					var a = world.x - this.shapeStart[0];
					var b = world.y - this.shapeStart[1];
					var r = Math.sqrt(a * a + b * b);
					this.svgElement.setAttribute('r', r * this.viewport.zoom);
					break;
			}
		}
	}

	turnModifierOn(){ this.modifierOn = true; }
	turnModifierOff(){ this.modifierOn = false; }

	commit(readyElements)
	{
		if(this.readyElements.length > 0)
		{
			for(var i = 0 ; i < this.readyElements.length ; ++i)
				readyElements.push(this.readyElements[i]);
			this.readyElements.splice(0,this.readyElements.length);
		}
	}

	abort()
	{
		this.shapeStart = undefined;
	}
	hasDrawing()
	{
		return this.readyElements.length > 0;
	}
	updateCursor(){}
	pause()
	{
		this.abort();
	}
	resume(){}
}

class PointerTool
{
	constructor(){this.isPaused = false;}
	pointerMove(){}
	pointerUp(){}
	pointerDown(){}
	turnModifierOn(){}
	turnModifierOff(){}
	commit(){}
	abort(){}
	updateCursor(){}
	hasDrawing(){return false;}
	pause(){}
	resume(){}
}

export class PointerInteraction
{
	constructor(tool, pointers, zoomAndPan, viewport, screenPainter, svgContainer, overlayContainer, cursor, user, whiteboard)
	{
		this.baseThickness = 4;
		this.viewport = viewport;
		this.zoomAndPan = zoomAndPan;
		this.cursor = cursor;
		this.user = user;
		this.eraser = new EraserTool(4, cursor, viewport, user);
		this.pen = new PathTool(ActionType.curve, 4, cursor, screenPainter, viewport, user);
		this.pen.setEpsilon(viewport.zoom);
		this.shape = new ShapeTool('rectangle', 4, viewport, screenPainter, svgContainer, user);
		this.laserPointer = new PointerTool();
		this.box_select = new BoxSelectionTool(cursor, viewport, user, overlayContainer, whiteboard.drawingContainer);
		this.pointers = pointers;
		this.rawPointers = [];
		this.readyElements = [];
		this.activeTool = undefined;
		this.maxPressureFactor = 2.5;
		this.minPressureFactor = 0.5;
		this.initialPinchLength = 0;
		this.initialZoom = 1;
		this.pinchCenter = {};
		this.previousPointers = [];
		//
		this.setThickness(this.baseThickness);
		this.setTool(tool);
	}

	getPressureFactor(pressure)
	{
		return (pressure * (this.maxPressureFactor - this.minPressureFactor) + this.minPressureFactor);
	}
	pointerUp(ev)
	{
		for( var i = 0; i < this.rawPointers.length; ++i)
		{
			if(this.rawPointers[i].pointerId === ev.pointerId)
			{
				this.rawPointers.splice(i,1);
				this.pointers.splice(i,1);
				break;
			}
		}
		let {screen, world} = this.viewport.getCoords(ev);
		if(this.activeTool.isPaused)
		{
			if(this.pointers.length === 0)
				this.activeTool.resume();
		}
		else
			this.activeTool.pointerUp(screen, world);
	}

	pointerDown(ev)
	{
		window.debug.logValues("pointer", {ev : ev});
		if(this.pointers.length !== 2)
		{
			this.rawPointers.push(ev);
			let {screen, world} = this.viewport.getCoords(ev);
			this.pointers.push({screen:screen, world:world});
			var pressure = 0.5;
			if(typeof ev.pressure === 'number' && ev.pointerType !== 'touch')
				pressure = ev.pressure;
			if(this.pointers.length === 2)
			{
				this.initialZoom = this.viewport.zoom;
				var sdx = this.pointers[0].screen.x - this.pointers[1].screen.x;
				var sdy = this.pointers[0].screen.y - this.pointers[1].screen.y;
				this.initialPinchLength = Math.sqrt(sdx*sdx + sdy*sdy);
				this.pinchCenter =
					{ x: (this.pointers[0].world.x + this.pointers[1].world.x)/2
					, y: (this.pointers[0].world.y + this.pointers[1].world.y)/2};
				this.previousPointers =
					[ { x : this.pointers[0].screen.x, y : this.pointers[0].screen.y}
					, { x : this.pointers[1].screen.x, y : this.pointers[1].screen.y}
					];
				this.activeTool.pause();
				this.zoomAndPan.start();
			}
			else
			{
				this.pen.setEpsilon(this.viewport.zoom);
				this.activeTool.pointerDown(screen, world, pressure, this.getPressureFactor(pressure));
			}
		}
	}

	pointerMove(ev)
	{
		let {screen, world} = this.viewport.getCoords(ev);

		window.debug.logValues("pointer", {'ev-type' : ev.type, pointerId : ev.pointerId, 'screen-x' : screen.x});
		window.debug.logValues("pointer", {'screen-x' : screen.x, 'screen-y' : screen.y, 'world-x' : world.x, 'world-y' : world.y});
		for(var i = 0 ; i < this.rawPointers.length ; ++i)
		{
			window.debug.logValues("pointer", {'this-rawPointer-id' : this.rawPointers[i].pointerId, i : i});
			if(this.rawPointers[i].pointerId === ev.pointerId)
			{
				this.rawPointers[i] = ev;
				this.pointers[i] = {screen : {x : screen.x, y : screen.y}, world : world};
				break;
			}
		}
		var pressure = 0.5;
		if(typeof ev.pressure === 'number' && ev.pointerType !== 'touch' && this.pointers.length > 0)
			pressure = ev.pressure;
		if(this.pointers.length === 2)
		{
			// get pan amount
			var dx0 = this.pointers[0].screen.x - this.previousPointers[0].x;
			var dy0 = this.pointers[0].screen.y - this.previousPointers[0].y;
			var dx1 = this.pointers[1].screen.x - this.previousPointers[1].x;
			var dy1 = this.pointers[1].screen.y - this.previousPointers[1].y;
			this.pinchCenter =
				{ x: (this.pointers[0].world.x + this.pointers[1].world.x)/2
				, y: (this.pointers[0].world.y + this.pointers[1].world.y)/2};
			var dx = (dx0 + dx1) / (2 * this.viewport.zoom);
			var dy = (dy0 + dy1) / (2 * this.viewport.zoom);
			this.previousPointers =
				[ { x : this.pointers[0].screen.x, y : this.pointers[0].screen.y}
				, { x : this.pointers[1].screen.x, y : this.pointers[1].screen.y}
				];

			// get pinch zoom
			var sdx = this.pointers[0].screen.x - this.pointers[1].screen.x;
			var sdy = this.pointers[0].screen.y - this.pointers[1].screen.y;
			var currentPinchLength = Math.sqrt(sdx*sdx + sdy*sdy);
			var zoomTarget = this.initialZoom * (currentPinchLength / this.initialPinchLength);
			var zoomDiff = Math.log2(zoomTarget) - this.zoomAndPan.zoomSmoother.target;

			this.zoomAndPan.applyMove(dx, dy);
			this.zoomAndPan.applyZoom(zoomDiff, this.pinchCenter);
		}
		else
		{
			this.activeTool.pointerMove(screen, world, pressure, this.getPressureFactor(pressure));
		}
		window.debug.logValues("pointer", {'ev-type' : ev.type, pointerId : ev.pointerId, 'end' : 1});
	}

	hasElements()
	{
		return this.activeTool.hasDrawing();
	}

	isReady()
	{
		this.activeTool.commit(this.readyElements);
		return (this.readyElements.length > 0);
	}

	getElements()
	{
		var els = this.readyElements.slice();
		this.readyElements.splice(0, this.readyElements.length);
		return els;
	}

	abort()
	{
		this.readyElements.splice(0, this.readyElements.length);
		this.activeTool.abort();
	}

	turnModifierOn() {this.activeTool.turnModifierOn();}
	turnModifierOff(){this.activeTool.turnModifierOff();}

	setTool(tool)
	{
		if(typeof this.activeTool !== 'undefined')
			this.activeTool.abort();
		switch(tool)
		{
			case ToolType.eraser:
				this.activeTool = this.eraser;
				break
			case ToolType.pen:
				this.activeTool = this.pen;
				break
			case ToolType.circle:
			case ToolType.line:
			case ToolType.rectangle:
				this.activeTool = this.shape;
				this.shape.type = tool;
				this.cursor.hide();
				break
			case ToolType.pointer:
				this.activeTool = this.laserPointer
				this.cursor.hide();
				break;
			case ToolType.boxSelection:
				this.activeTool = this.box_select;
				this.cursor.hide();
				break;
		}
		this.activeTool.abort();
		this.activeTool.updateCursor(this.getPressureFactor(0.5));
	}
	setColor(color)
	{
		this.pen.setColor(color);
		this.shape.color = color;
	}
	setThickness(thickness)
	{
		this.baseThickness = thickness;
		this.pen.setThickness(thickness);
		this.eraser.setThickness(thickness);
		this.shape.setThickness(thickness);
	}
	updateCursor()
	{
		this.activeTool.updateCursor(this.getPressureFactor(0.5));
	}
}

export class EventHandler
{
	constructor(whiteboard, viewport, pointerInteraction, cameraControl, communication)
	{
		this.whiteboard = whiteboard;
		this.viewport = viewport;
		this.communication = communication;
		this.pointerInteraction = pointerInteraction;
		this.cameraControl = cameraControl;
		this.movement_state = {up:0, down:0, left:0, right:0};
		this.zoom_state = {in : 0, out : 0};
		this.rawPointers = [];
		this.activePointers = 0;
		this.mouse_pos = {x:0, y:0};
	}

	setupListeners(custom_mouse_overlay)
	{
		var _this = this;
		// Moving with arrow keys
		document.addEventListener('keydown'
			, function(l){return function(e){l.keyDown(e);}}(_this));
		// Stop moving with arrow keys
		document.addEventListener('keyup'
			, function(l){return function(e){l.keyUp(e);}}(_this));
		// Zooming with the mouse wheel
		var mouse_overlay;
		if(typeof custom_mouse_overlay === 'undefined')
		{
			mouse_overlay = document.getElementById("mouseoverlay");
		}
		else
		{
			mouse_overlay = custom_mouse_overlay;
		}
		mouse_overlay.addEventListener("wheel"
			,	function(l){return function(e){l.wheel(e);}}(_this), false);
		mouse_overlay.addEventListener("pointerdown"
			, function(l){return function(e){l.pointerDown(e);}}(_this), false);
		mouse_overlay.addEventListener("touchstart"
			, function(l){return function(e){e.preventDefault();}}(_this), false);
		mouse_overlay.addEventListener("pointermove"
			, function(l){return function(e){l.pointerMove(e);}}(_this), false);
		// Pointer up events
		mouse_overlay.addEventListener("pointerup",     function(e){_this.pointerUp(e);}, false);
		mouse_overlay.addEventListener("pointercancel", function(e){_this.pointerUp(e);}, false);
		mouse_overlay.addEventListener("pointerout",    function(e){_this.pointerUp(e);}, false);
		mouse_overlay.addEventListener("pointerleave",  function(e){_this.pointerUp(e);}, false);
	}

	applyCameraAcceleration()
	{
		var move_x = this.movement_state.left - this.movement_state.right;
		var move_y = this.movement_state.up - this.movement_state.down;
		this.cameraControl.setAcceleration(move_x, move_y);
	}

	applyZoom()
	{
		var zoom_diff = this.zoom_state.in - this.zoom_state.out;
		this.cameraControl.setZoomAcceleration(zoom_diff, this.mouse_pos);
	}

	pointerUp(e)
	{
		for( var i = 0 ; i < this.rawPointers.length ; ++i)
		{
			if(this.rawPointers[i].pointerId === e.pointerId)
			{
				this.rawPointers.splice(i,1);
				break;
			}
		}
		if (this.imgDragActive) {
			return;
		}
		this.pointerInteraction.pointerUp(e);
		//this.communication.pointerUp(e);
		this.whiteboard.pointerUp(e);
		this.activePointers--;
		if(this.activePointers < 0)
			this.activePointers = 0;
		if(this.activePointers === 0)
			this.drawFlag = false;
	}

	pointerMove(e)
	{
		window.debug.logValues("pointer", {'e-type' : e.type, pointerId : e.pointerId, line : 1519});
		for(var i = 0 ; i < this.rawPointers.length ; ++i)
		{
			if(this.rawPointers[i].pointerId === e.pointerId)
			{
				this.rawPointers[i] = e;
				break;
			}
		}
		this.pointerInteraction.pointerMove(e);
		let {screen1, world1} = this.getMouseCoords(e);
		this.mouse_pos = world1;
		this.whiteboard.pointerMove(screen1, world1);
		if(this.activePointers <= 1)
			this.communication.cursorUpdate(world1.x, world1.y);
	}

	pointerDown(e)
	{
		window.debug.logValues("pointer", {'e-type' : e.type, pointerId : e.pointerId, line : 1541});
		this.activePointers++;
		this.rawPointers.push(e);
		if(this.activePointers > 1)
			this.drawFlag = false;
		else if ((this.imgDragActive || this.drawFlag))
			return;
		this.pointerInteraction.pointerDown(e);
	}

	wheel(e)
	{
		e.preventDefault();
		let {screen1, world1} = this.getMouseCoords(e);
		var direction = e.deltaY > 0 ? -1 : 1;
		var zoomDiff = direction * 0.8;
		this.cameraControl.applyZoom(zoomDiff, world1);
		this.whiteboard.setRedrawTimer();
	}

	keyDown(e)
	{
		var move_amount = 30 / this.viewport.zoom;
		var moved = false;
		var zoomed = false;
		switch(e.key)
		{
			case "ArrowRight":
				this.movement_state.right = move_amount;
				moved = true;
				break;
			case "ArrowUp":
				this.movement_state.up = move_amount;
				moved = true;
				break;
			case "ArrowDown":
				this.movement_state.down = move_amount;
				moved = true;
				break;
			case "ArrowLeft":
				this.movement_state.left = move_amount;
				moved = true;
				break;
			case "Shift":
				this.pointerInteraction.turnModifierOn();
				break;
			case "+":
				this.zoom_state.in = 0.1;
				zoomed = true;
				break;
			case "-":
				this.zoom_state.out = 0.1;
				zoomed = true;
				break;
			case ".":
				this.whiteboard.centerAtCursor(this.mouse_pos);
				break;
		}
		if(moved)
			this.applyCameraAcceleration();
		if(zoomed)
			this.applyZoom();
	}

	keyUp(e)
	{
		var moved = false;
		var zoomed = false;
		switch(e.key)
		{
			case "ArrowRight":
				this.movement_state.right = 0;
				moved = true;
				break;
			case "ArrowUp":
				this.movement_state.up = 0;
				moved = true;
				break;
			case "ArrowDown":
				this.movement_state.down = 0;
				moved = true;
				break;
			case "ArrowLeft":
				this.movement_state.left = 0;
				moved = true;
				break;
			case "Shift":
				this.pointerInteraction.turnModifierOff();
				break;
			case "+":
				this.zoom_state.in = 0;
				zoomed = true;
				break;
			case "-":
				this.zoom_state.out = 0;
				zoomed = true;
				break;
		}
		if(moved)
			this.applyCameraAcceleration();
		if(zoomed)
			this.applyZoom();
	}
	
	triggerMouseMove(e)
	{
		var _this = this;
		if (_this.imgDragActive) {
			return;
		}
		if(this.activePointers > 1)
		{
			return;
		}
		let {screen1, world1} = this.getMouseCoords(e);
		var currX = world1.x;
		var currY = world1.y;
		this.cursorScreenX = screen1.x;
		this.cursorScreenY = screen1.y;
		this.cursorWorldX = world1.x;
		this.cursorWorldY = world1.y;
		if(typeof this.prevX !== 'number')
		{
			this.prevX = currX;
			this.prevY = currY;
			return;
		}

		if(!this.drawFlag && this.activePointers === 1 && this.tool !== ToolType.pointer)
		{
			var dx = currX - _this.prevX;
			var dy = currY - _this.prevY;
		
			this.drawFlag = true;
		}
	}
	triggerMouseOver()
	{
		/*
		var _this = this;
		if (_this.imgDragActive) {
			return;
		}
		if (!_this.mouseover) {
			this.resizeCursor();
		}
		_this.mouseover = true;*/
	}
	triggerMouseOut()
	{
		//this.communication. sendFunction({ "t": ActionType.cursor, "event": "out" });
	}
	getPointersCoords ()
	{
		let {screen, world} = this.getPointerCoords(this.rawPointers[0]);
		var screen1 = screen;
		var world1 = world;
		var screen2;
		var world2;
		if(this.rawPointers.length > 1)
		{
			let {screen, world} = this.getPointerCoords(this.rawPointers[1]);
			screen2 = screen;
			world2 = world;
		}
		return {screen1, world1, screen2, world2};

	}
	getPointerCoords(pointerEvent)
	{
		var screen = {};
		screen.x = pointerEvent.clientX;//(pointerEvent.offsetX || pointerEvent.pageX - $(pointerEvent.target).offset().left) + 1;
		screen.y = pointerEvent.clientY;//pointerEvent.currY || (pointerEvent.offsetY || pointerEvent.pageY - $(pointerEvent.target).offset().top);
		var world = {};
		world.x = (screen.x - this.viewport.width/2)/ this.viewport.zoom + this.viewport.x;
		world.y = (screen.y - this.viewport.height/2) / this.viewport.zoom + this.viewport.y;
		return {screen, world};

	}
	getMouseCoords(mouseEvent)
	{
		let {screen, world} = this.getPointerCoords(mouseEvent);
		var screen1 = screen;
		var world1 = world;
		var screen2;
		var world2;
		for(var i = 0; i < this.rawPointers.length ; ++i)
		{
			if(this.rawPointers[i].pointerId !== mouseEvent.pointerId)
			{
				let {screen, world} = this.getPointerCoords(this.rawPointers[i]);
				screen2 = screen;
				world2 = world;
			}
		}
		return {screen1, world1, screen2, world2};
	}
}

export class Communication
{
	constructor(sendFunction, requestFunction, settings)
	{
		this.sendFunction = sendFunction;
		this.requestFunction = requestFunction;
		this.settings = settings;
	}

	pointerUp()
	{
		//this.sendPackage({ "t": ActionType.cursor, "event": "out", "username": this.settings.username });
	}

	pointerOut()
	{
		this.sendPackage({ "t": ActionType.cursor, "event": "out" });
	}

	sendCursorMove(cx,cy)
	{
		this.sendPackage({ "t": ActionType.cursor, "event": "move", "d": [cx, cy], "badge_bg" : this.settings.badge_bg, "badge_fg" : this.settings.badge_fg });
	}
	sendDrawing(content)
	{
		this.sendPackage(content);
	}
	sendAction(type, data)
	{
		if(typeof data !== 'undefined')
			this.sendPackage({"t":type, "d":data});
		else
			this.sendPackage({"t":type});
	}
	clearWhiteboard()
	{
		this.sendAction(ActionType.clear);
	}
	sendBG(draw, url, width, height, left, top)
	{
		this.sendPackage({ "t": ActionType.addImgBG, "draw": draw, "url": url, "d": [width, height, left, top] });
	}
	sendPackage(content)
	{
		content["username"] = this.settings.username;
		content["user_id"] = this.settings.user_id;

		if (typeof this.settings.sendFunction !== 'undefined') {
			this.settings.sendFunction(content);
		}
	}
	insertDrawing(drawing)
	{
		var content = drawing.pack();
		this.sendPackage(content);
	}
	hideDrawing(drawings)
	{
		var data = [];
		var j = 0;
		for(var i = 0 ; i < drawings.length ; ++i , j+=2)
		{
			data[j] = drawings[i].user_id;
			data[j+1] = drawings[i].draw_id;
		}
		this.sendPackage({"t" : ActionType.remove, "d" : data});
	}
	restoreDrawing(drawings)
	{
		var data = [];
		var j = 0;
		for(var i = 0 ; i < drawings.length ; ++i , j+=2)
		{
			data[j] = drawings[i].user_id;
			data[j+1] = drawings[i].draw_id;
		}
		this.sendPackage({"t" : ActionType.restore, "d" : data});
	}
	requestDrawings(ids)
	{
		this.requestFunction(ids);
	}
}

export class RemoteUser
{
	constructor(user_id, user_name, viewport, whiteboard, badge_fg, badge_bg, cursorContainer)
	{
		this.user_id = user_id;
		if(typeof user_name !== 'undefined')
			this.user_name = user_name;
		this.badge_fg = badge_fg;
		this.badge_bg = badge_bg;
		this.screen = {x:0, y:0};
		this.world = {x:0, y:0};
		this.viewport = viewport;
		this.cursorContainer = cursorContainer;
		this.draw_id = 0;
		if(this.badge_fg !== undefined)
		{
			this.initCursor();
		}
		else
		{
			this.hidden = true;
		}
		this.whiteboard = whiteboard;
		var now = new Date();
		this.last_activity = now.getTime();
	}
	updateDrawId(draw_id)
	{
		if(this.draw_id <= draw_id)
			this.draw_id = draw_id + 1;
	}
	initCursor()
	{
		this.cursorElement = document.createElement('div');
		this.cursorElement.setAttribute("class", "userbadge");
		this.cursorElement.setAttribute("style",'background:' + this.badge_bg + '; color:' + this.badge_fg + ';');
		var pointer = document.createElement('div');
		pointer.setAttribute("style", "width:4px; height:4px; background:gray; position:absolute; top:13px; left:-2px; border-radius:50%;");
		this.cursorElement.append(pointer);
		this.nameElement = document.createElement('span');
		this.nameElement.innerHTML = decodeURIComponent(this.user_name);
		this.cursorElement.append(this.nameElement);
		this.cursorContainer.append(this.cursorElement);
		this.hidden = false;
	}

	setPosition(world, badge_fg, badge_bg, username)
	{
		var now = new Date();
		this.world.x = world.x;
		this.world.y = world.y;
		this.last_activity = now.getTime();
		if(this.hidden)
			this.initCursor();
		this.redraw();
		this.whiteboard.cursorMove(world.x, world.y);
	}

	setTag(tag)
	{
		if(typeof tag.fg !== 'undefined')
			this.badge_fg = tag.fg;
		if(typeof tag.bg !== 'undefined')
			this.badge_bg = tag.bg;
		if(typeof tag.username !== 'undefined')
		{
			this.user_name = tag.username;
			if(!this.hidden)
				this.nameElement.innerHTML = decodeURIComponent(this.user_name);
		}
	}

	redraw()
	{
		if(this.hidden)
			return;
		let {screenX, screenY} = this.viewport.toScreenCoords(this.world.x, this.world.y);
		this.screen.x = screenX;
		this.screen.y = screenY;

		var backgroundColor = this.badge_bg;
		var foregroundColor = this.badge_fg;
		if(screenX > this.viewport.width)
		{
			screenX = this.viewport.width - 60;
			backgroundColor = this.badge_fg;
			foregroundColor = this.badge_bg;
		}
		else if(screenX < 0)
		{
			screenX = 0;
			backgroundColor = this.badge_fg;
			foregroundColor = this.badge_bg;
		}
		if(screenY > this.viewport.height)
		{
			screenY = this.viewport.height;
			backgroundColor = this.badge_fg;
			foregroundColor = this.badge_bg;
		}
		else if(screenY < 0)
		{
			screenY = 15;
			backgroundColor = this.badge_fg;
			foregroundColor = this.badge_bg;
		}

		this.cursorElement.style.background = backgroundColor;
		this.cursorElement.style.color= foregroundColor;
		this.cursorElement.style.left = screenX + "px";
		this.cursorElement.style.top = (screenY - 15) + "px";
	}

	hideCursor()
	{
		if(this.cursorContainer === undefined)
			return;
		if(this.hidden)
			return;
		this.cursorContainer.removeChild(this.cursorElement);
		this.hidden = true;
	}
}

export class LocalUser
{
	constructor(user_id, user_name, viewport, whiteboard)
	{
		this.user_id = user_id;
		this.user_name = user_name;
		this.draw_id = 0;
		this.history = [];
		this.deletions = {};
		this.viewport = viewport;
		this.whiteboard = whiteboard;
	}

	updateDrawId(draw_id)
	{
		if(this.draw_id <= draw_id)
			this.draw_id = draw_id + 1;
	}

	setPosition(world)
	{
		this.whiteboard.cursorMove(world.x, world.y);
	}

	eraseAroundLine(p0, p1, thickness)
	{
		var els = this.whiteboard.eraseAroundLine(p0, p1, thickness);
		if(els.length > 0)
		{
			this.deletions[this.draw_id] = els;
			this.history.push([this.draw_id]);
			this.draw_id++;		
		}
	}

	hideDrawings(drawings)
	{
		this.deletions[this.draw_id] = drawings;
		this.history.push([this.draw_id]);
		this.draw_id++;		
		this.whiteboard.eraseDrawings(drawings);
	}

	insertDrawing(el)
	{
		el.draw_id = this.draw_id;
		el.user_id = this.user_id;
		this.draw_id++;
		this.history.push([el.draw_id]);
		this.whiteboard.insertDrawing(el);
	}

  moveDrawings(drawings, dx,dy)
  {
    var new_drawings = [];
		var new_drawings_ids = [];
    for(let d in drawings)
    {
      var di = translateDrawing(drawings[d], dx, dy);
			di.user_id = this.user_id;
			di.draw_id = this.draw_id;
			this.draw_id++;
      new_drawings.push(di)
			new_drawings_ids.push(di.draw_id);
			new_drawings_ids.push(drawings[d].draw_id);
			this.deletions[drawings[d].draw_id] = [drawings[d]];
    }
		this.history.push(new_drawings_ids);
		this.whiteboard.insertDrawings(new_drawings);
		this.whiteboard.eraseDrawings(drawings);
		return(new_drawings);
  }

	duplicateDrawings(drawings)
	{
    var new_drawings = [];
		var new_drawings_ids = [];
    for(let d in drawings)
    {
      var di = drawings[d].copy();
			di.user_id = this.user_id;
			di.draw_id = this.draw_id;
			this.draw_id++;
      new_drawings.push(di)
			new_drawings_ids.push(di.draw_id);
    }
		this.history.push(new_drawings_ids);
		this.whiteboard.insertDrawings(new_drawings);
		return(new_drawings);

	}

	clear()
	{
		this.whiteboard.clearWhiteboard();
	}

	undo()
	{
		var i = this.history.length - 1;
		if(i >= 0)
		{
			var hs = this.history[i];
			var k = 1;
      for(let h in hs)
      {
        if(hs[h] in this.deletions)
        {
          this.whiteboard.restoreDrawings(this.deletions[hs[h]]);
        }
        else
        {
          this.whiteboard.eraseDrawings([{user_id :this.user_id, draw_id : hs[h]}]);
        }
      }
			this.history.splice(i,1);
		}
	}
}

function getRoundedAngles(x, y) { //For drawing lines at 0,45,90° ....
	var angle = Math.atan2(x, y) * (180 / Math.PI);
	var angle45 = Math.round(angle / 45) * 45;
	var currX = x;
	var currY = y;
	if (angle45 % 90 === 0) {
		if (Math.abs(x) > Math.abs(y)) {
			currY = 0
		} else {
			currX = 0
		}
	} else {
		if (y * x > 0) {
			currX = currY;
		} else {
			currX = -currY;
		}
	}
	return { "x": currX, "y": currY };
}
