#!/usr/bin/env fish

set Sizes 48 32 16
set Icons

for Size in $Sizes
	convert -density (math "$Size / 32 * 96") -geometry "$Size"x"$Size" -flatten -background none Zauboard-Hat-blue.svg icon-"$Size"x"$Size".ico
	set Icons $Icons icon-"$Size"x"$Size".ico
end

convert $Icons favicon.ico
rm $Icons
