
Following icons were made by [https://www.flaticon.com/authors/freepik](Freepik) from [https://www.flaticon.com/](Flaticon).

	* `pen.svg`, `share.svg`

Following icons were made by [https://www.flaticon.com/authors/pixel-perfect](Pixel perfect) from [https://www.flaticon.com/](Flaticon)

  * `eraser.svg`

Following icons were made by [https://www.flaticon.com/authors/pixelmeetup](Pixelmeetup) from [https://www.flaticon.com/](Flaticon)

	* `trash.svg`

Following icons were made by [https://www.flaticon.com/authors/dimitry-miroliubov](Dimitry Miroliubov) from [https://www.flaticon.com/](Flaticon)

	* `pdf.svg`

Following icons were made by [https://www.flaticon.com/authors/smashicons](Smashicons) from [https://www.flaticon.com/](Flaticon)

	* `json.svg`
