{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad
import Control.Monad.Trans.State
import Control.Exception
import Data.List
import Data.Maybe
import Network.Socket (withSocketsDo)
import System.Environment
import Zauboard.Package
import qualified Data.Text           as T
import qualified Data.Text.IO        as T
import qualified Network.WebSockets  as WS
import qualified Wuss                as WSS
import Text.Read (readMaybe)

data Config = Config
  { cfURL        :: Maybe String
  , cfUsername   :: Maybe String
  , cfPassword   :: Maybe String
  , cfToken      :: Maybe String
  , cfBoardKey   :: Maybe String
  , cfOutputFile :: Maybe FilePath
  , cfInputFile  :: Maybe FilePath
  , cfError      :: Maybe String
  }

emptyConfig = Config
  { cfURL = Nothing
  , cfUsername = Nothing
  , cfPassword = Nothing
  , cfToken = Nothing
  , cfBoardKey = Nothing
  , cfOutputFile = Nothing
  , cfInputFile = Nothing
  , cfError = Nothing
  }

data Reply a = Reply a | NoReply | Quit

main = do
  args <- getArgs
  let conf = parseArgs args
  case cfError conf of
    Just err -> putStrLn err
    Nothing -> connect conf

parseArgs args = parseArgs' emptyConfig args
  where
    parseArgs' conf [] = conf
    parseArgs' conf args = 
      let (conf', args') = case args of
                             ("--username":uname:args1) -> (conf{cfUsername = Just uname},   args1)
                             ("--password":pwd:args1)   -> (conf{cfPassword = Just pwd},     args1)
                             ("--token":token:args1)    -> (conf{cfToken = Just token},      args1)
                             ("--output":file:args1)    -> (conf{cfOutputFile = Just file},  args1)
                             ("--input":file:args1)     -> (conf{cfInputFile = Just file},   args1)
                             (('-':arg):args1)          -> (conf{cfError = Just $ "Unknown option " ++ '-':arg}, [])
                             (str:args1)                -> (conf{cfURL = Just str},          args1)
      in parseArgs' conf' args'
      

client :: Config -> WS.ClientApp ()
client conf conn = do
  quitM <- newEmptyMVar
  let outF = case cfOutputFile conf of
                  Just fl -> \str -> appendFile fl $ str ++ "\n"
                  Nothing -> putStrLn
  forkFinally 
    (do
        cmds <- fmap lines getContents
        let actions = userInput conf cmds
        forM actions $ \a -> case a of
          Reply pkg -> WS.sendTextData conn pkg
          NoReply   -> return ()
          Quit      -> return ()          
    )
    ( \e -> case e of
      Left err -> do
        putStrLn $ show (err :: SomeException)
        putMVar quitM ()
      Right _ -> putMVar quitM ()
    )
  forkIO $ forever $ do
    pkg <- WS.receiveData conn
    -- let pkg = parsePackage msg
    forkIO $ serverOutput conf $ show (pkg :: Package)
    forkIO $ outF $ show pkg
  takeMVar quitM
  putStrLn "quitting"
  WS.sendClose conn ("" :: T.Text)

userInput conf lns = do
  loop lns
  where
    loop (ln:lns)
      | ln == "quit" = [Quit]
      | otherwise = do
          let (pkgLns, lns') = span (/="") lns
              pkgStr = intercalate "\n" (ln:pkgLns)
              pkg = parsePackage pkgStr
          case pkg of
            Nothing -> loop $ drop 1 lns'
            Just pkg' -> Reply pkg' : (loop $ drop 1 lns')
    loop [] = []

serverOutput conf pkg = return ()

connect conf
  | isNothing $ cfURL conf = putStrLn "Please provide the URL of a Zauboard whiteboard."
  | otherwise = do
    let Just url = cfURL conf
        (port, domain, path, queryVars) = parseURL url
    print (port, domain, path, queryVars)
    withSocketsDo $ 
      if port == 443 then
        WSS.runSecureClient domain (fromIntegral port) path (client conf)
      else
        WS.runClient domain port path (client conf)

parseURL :: String -> (Int, String, String, [(String, String)])
parseURL = evalState (liftM4 (,,,) protocol domain path queryVars)

protocol = do
  str <- get
  let (prot, rs) = span (/=':') str
      domPort = readMaybe $ takeWhile (/='/') $ dropWhile (/=':') rs
  put $ dropWhile (=='/') $ tail rs
  return $ case domPort of
    Just p -> p
    Nothing -> case prot of
                 "https" -> 443
                 _ -> 80
domain = do
  str <- get
  let (dm, rs) = span (/='/') str
  put rs
  return $ takeWhile (/=':') dm

path = do
  str <- get
  let (pth, rs) = span (/='?') str
  put $ drop 1 rs
  return pth

queryVars = do
  str <- get
  let (var, rs) = span (/='=') str
  if null var then
    return []
  else do
    let (val, rs') = span (/='&') rs
    put $ drop 1 rs'
    vars <- queryVars
    return $ (var, drop 1val) : vars

  
