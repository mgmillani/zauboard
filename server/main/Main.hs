{-# LANGUAGE OverloadedStrings #-}

module Main where

import System.Environment
import Zauboard.Server

main = do
  args <- getArgs
  start args
