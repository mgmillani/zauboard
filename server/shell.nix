let pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
   packages = with pkgs; [
     (haskellPackages.ghcWithPackages (p: 
      [ p.base
        p.aeson
        p.base64-bytestring
        p.bytestring
        p.case-insensitive
        p.containers
        p.cryptonite
        p.descrilo
        p.directory
        p.filepath
        p.hasql
        p.http-types
        p.HUnit
        p.mtl
        p.network-uri
        p.process
        p.sqlite-simple
        p.strict-concurrency
        p.text
        p.time
        p.transformers
        p.unordered-containers
        p.utf8-string
        p.vector
        p.wai
        p.wai-websockets
        p.warp
        p.websockets
      ]
     ))
     pdf2svg
     pdftk
     xz
     poppler_utils
     librsvg
   ];
}
