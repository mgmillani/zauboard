{ stdenv, lib, makeWrapper, librsvg, pdf2svg, pdftk, poppler_utils, haskellPackages, xz, zauboard-web-client
}:

let zauboard = haskellPackages.callPackage ./zauboard.nix {};
in
stdenv.mkDerivation
{
	pname = "zauboard-server";
	version = "2.5";
	src = ./.;

	buildInputs = [ makeWrapper ];

	installPhase = ''
		makeWrapper ${zauboard}/bin/zauboard $out/bin/zauboard \
			--prefix PATH : ${lib.makeBinPath [librsvg pdf2svg pdftk xz poppler_utils]} \
			--add-flags "--public-directory ${zauboard-web-client}/share/zauboard/public"
	'';
}

