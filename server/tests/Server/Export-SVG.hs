module Main where

import Zauboard.Export.SVG
import Zauboard

import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)

tests = TestList                         
  [ TestLabel "Control Points" $ TestCase
    ( do
      assertEqual "Line 2" ([(1,1)]) (controlPoints [(0,0), (1,1)])
      assertEqual "Line 3" ([(0.0,0.0), (0.5,0.5), (0.5,1), (1.5,1), (1.5, 0.5), (2.0,0), (2,0)]) (controlPoints [(0,0), (0,0), (1,1), (2,0), (2,0)])
      assertEqual "Line 4" ([(0.5,1), (1.5,1), (1.5,0), (2.5, 0), (3,1)]) (controlPoints [(0,0), (1,1), (2,0), (3,1)])
      let pathHeader = "<path style=\"fill:none;stroke:#AABBCC;stroke-linecap:round;stroke-width:"
      assertEqual "Line 3 SVG"
                  (  pathHeader ++ "1.0;\" d = \"M 0.0,0.0 C 0.5,0.5 0.5,1.0 1.0,1.0\"/>"
                  ++ pathHeader ++ "2.0;\" d = \"M 1.0,1.0 C 1.5,1.0 1.5,0.5 2.0,0.0\"/>"
                  )
                  (toSVG $ Path [(0,0), (1,1), (2,0)] [1,2] "#AABBCC" (Just ((0,0), (2,0))))
    )
  , TestLabel "Empty" $ TestCase
    ( do
      assertEqual "Empty path" "" (toSVG $ Path [] [] "#AABBCC" Nothing)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
