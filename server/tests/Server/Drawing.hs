module Main where

import Zauboard.Drawing
import Zauboard

import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)
import qualified Data.Text          as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Lazy          as TL
import qualified Data.Text.Lazy.Encoding as TL
import Data.List

tests = TestList                         
  [ TestLabel "Pack drawings" $ TestCase
    ( do
      let c0 = Circle (0,0) 1 "#FF0000" 10
          d0 = Drawing (ElementID 0 0, c0)
          d0Str = "{\"d\":0,\"u\":0,\"i\":0,\"x\":0.0,\"y\":0.0,\"r\":10.0,\"c\":\"#FF0000\",\"h\":1.0}"
          p0 = Path [(0,1), (2,4)] [5] "#00FF00" (Just ((0,1), (2,4)))
          d1 = Drawing (ElementID 0 1, p0)
          d1Str = "{\"d\":1,\"u\":0,\"i\":1,\"p\":[0.0,1.0,2.0,4.0],\"o\":[0.0,1.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[5.0]}"
          p1 = Path [(1,0), (4,2), (2,4)] [8,7] "#00FF00" Nothing
          d2 = Drawing (ElementID 1 2, p1)
          d2Str = "{\"d\":2,\"u\":1,\"i\":2,\"p\":[1.0,0.0,4.0,2.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[8.0,7.0]}"
          svg3 = RawSVG (0,0) 1.0 10 10 $ T.encodeUtf8 $ T.pack "<g>\"\"</g>"
          d3 = Drawing (ElementID 1 3, svg3)
          d3Str = "{\"d\":3,\"u\":1,\"i\":3,\"s\":\"<g>\\\"\\\"</g>\",\"x\":0.0,\"y\":0.0,\"c\":1.0,\"w\":10.0,\"h\":10.0}"
      assertEqual "Pack circle 0"
                  ("[" ++ d0Str ++ "]")
                  (packDrawings [d0])
      assertEqual "Pack path 0"
                  ("[" ++ d1Str ++ "]")
                  (packDrawings [d1])
      assertEqual "Pack path 1"
                  ("[" ++ d2Str ++ "]")
                  (packDrawings [d2])
      assertEqual "Pack svg 2"
                  ("[" ++ d3Str ++ "]")
                  (packDrawings [d3])
      assertEqual "Pack all"
                  ("[" ++ intercalate "," [d0Str,d1Str,d2Str] ++ "]")
                  (packDrawings [d0,d1,d2])
    )
  , TestLabel "Load drawings v0" $ TestCase
    ( do
      let p1 = Path [(0,0), (1,1)] [5] "#FFFFFF" Nothing
          d1 = Drawing (ElementID 0 0, p1)
          d1Str = "{\"t\":2,\"d\":[0,0,1,1],\"th\":\"5\",\"user_id\":0,\"draw_id\":0}"
          d2Str = "{\"t\":0,\"d\":[0,1,2,3],\"c\":\"black\",\"th\":4.4,\"user_id\":0,\"draw_id\":1}"
          d3Str = "{\"t\":0,\"d\":[1,1,2,2],\"c\":\"#000000\",\"th\":1,\"user_id\":3}"
      assertEqual "" (Just [d1]) (loadDrawings "0" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d1Str ++ "]")
      assertEqual "" (Just [Drawing (ElementID 0 0
                                    , Path [(0,1), (2,3)] [4.4] "black" Nothing)])
                     (loadDrawings "0" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d2Str ++ "]")
      assertEqual "" (Just [Drawing (ElementID 3 0
                                    , Path [(1,1), (2,2)] [1] "#000000" Nothing)])
                     (loadDrawings "0" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d3Str ++ "]")
      assertEqual "" (Just [ Drawing (ElementID 0 0
                                    , Path [(0,1), (2,3)] [4.4] "black" Nothing)
                           , Drawing (ElementID 3 1
                                    , Path [(1,1), (2,2)] [1] "#000000" Nothing)
                           ])
                     (loadDrawings "0" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d2Str ++ "," ++ d3Str ++ "]")
    )
  , TestLabel "Load drawings v1" $ TestCase
    ( do
      let c0 = Circle (0,0) 1 "#FF0000" 10
          d0 = Drawing (ElementID 0 0, c0)
          d0Str = "{\"d\":0,\"u\":0,\"i\":0,\"x\":0.0,\"y\":0.0,\"r\":10.0,\"c\":\"#FF0000\",\"h\":1.0}"
          p0 = Path [(0,1), (2,4)] [5] "#00FF00" (Just ((0,1), (2,4)))
          d1 = Drawing (ElementID 0 1, p0)
          d1Str = "{\"d\":1,\"u\":0,\"i\":1,\"p\":[0.0,1.0,2.0,4.0],\"o\":[0.0,1.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[5.0]}"
          p1 = Path [(1,0), (4,2), (2,4)] [8,7] "#00FF00" Nothing
          d2 = Drawing (ElementID 1 2, p1)
          d2Str = "{\"d\":2,\"u\":1,\"i\":2,\"p\":[1.0,0.0,4.0,2.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[8.0,7.0]}"
          p2 = Path [(0,0), (1,1), (2,2), (3,3), (4,4)] [3,3,3] "black" Nothing
          d3 = Drawing (ElementID (-1) 0, p2)
          d3Str = "{\"d\":2,\"i\":0,\"c\":\"black\",\"h\":[3,3,3],\"p\":[0,0,1,1,2,2,3,3,4,4],\"u\":-1}"
          svg4 = RawSVG (0,0) 1.0 10 10 $ T.encodeUtf8 $ T.pack $ "<g>\"\"</g>"
          d4 = Drawing (ElementID 1 3, svg4)
          d4Str = "{\"d\":3,\"u\":1,\"i\":3,\"s\":\"<g>\\\"\\\"</g>\",\"x\":0.0,\"y\":0.0,\"w\":10.0,\"h\":10.0,\"c\":1.0}"
          svg5 = RawSVG (0,0) 1.0 10 10 $ T.encodeUtf8 $ T.pack $ "<g></g>"
          d5 = Drawing (ElementID 1 3, svg5)
          d5Str = "{\"d\":3,\"u\":1,\"i\":3,\"s\":\"<g></g>\",\"x\":0.0,\"y\":0.0,\"c\":1.0,\"w\":10.0,\"h\":10.0}"
      assertEqual "Unpack circle 0"
                  (Just [d0])
                  (loadDrawings "1" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d0Str ++ "]")
      assertEqual "Unpack path 0"
                  (Just [d1])
                  (loadDrawings "1" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d1Str ++ "]")
      assertEqual "Unpack path 1"
                  (Just [d2])
                  (loadDrawings "1" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d2Str ++ "]")
      assertEqual "Unpack path 2"
                  (Just [d3])
                  (loadDrawings "1" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d3Str ++ "]")
      assertEqual "Unpack svg 5"
                  (Just [d5])
                  (loadDrawings "1" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d5Str ++ "]")
      assertEqual "Unpack svg 4"
                  (Just [d4])
                  (loadDrawings "1" $ TL.encodeUtf8 $ TL.pack $ "[" ++ d4Str ++ "]")
      assertEqual "Unpack all"
                  (Just [d0,d1,d2])
                  (loadDrawings "1" $ TL.encodeUtf8 $ TL.pack $ "[" ++ intercalate "," [d0Str, d1Str, d2Str] ++ "]")
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
