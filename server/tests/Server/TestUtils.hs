module TestUtils where

import Test.HUnit
import Test.HUnit.Lang
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad.Except
import Control.Exception

timeout t name f = do
  doneM <- newEmptyMVar
  tid <- forkFinally f $ \e ->
            case e of
              Left err -> do
                putMVar doneM $ Just err
              Right () -> putMVar doneM Nothing
  watchdogId <- forkIO $ do
    threadDelay (t * 10^3)
    killThread tid
    putMVar doneM Nothing
  success <- takeMVar doneM
  killThread watchdogId
  case success of 
    Nothing -> return ()
    Just failure -> do
      throw failure
  -- assertBool ("Time out: " ++ name) success

