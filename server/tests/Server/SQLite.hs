module Main where

import Zauboard.Database.SQLite
import Zauboard.Database.Interface
import Zauboard

import Control.Monad.Except
import Control.Monad (mplus, when, forM)
import Data.Time.Calendar
import Data.Time.Clock
import Data.Time.Clock.System
import System.Directory
import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)

tests = TestList                         
  [ TestLabel "User 0" $ TestCase
    ( do
      let dbName = "tests/Server/test-0.db"
      doesFileExist dbName >>= flip when (removeFile dbName)
      eSQL <- runExceptT $ open dbName
      runDatabase dbName eSQL $ \sql -> do
        createUser "test-user" "password-hash" sql
        getUserIDFromName "test-user" sql >>= assertEqual "ID" (Just 0)
        getPasswordHash 0 sql       >>= assertEqual "Password hash" (Just "password-hash")
        getUsername 0 sql           >>= assertEqual "Username" (Just "test-user")
        updatePasswordHash 0 "new hash" sql
        getPasswordHash 0 sql       >>= assertEqual "Password hash" (Just "new hash")
        createUser "test-user-2" "password-hash-2" sql
        getUserIDFromName "test-user-2" sql >>= assertEqual "ID 2" (Just 1)
        listUsers sql >>= assertEqual "User list 2" ["test-user", "test-user-2"]
        deleteUser 1 sql
        getUserIDFromName "test-user-2" sql >>= assertEqual "no ID 2" Nothing
    )
  , TestLabel "User 1" $ TestCase
    ( do
      let dbName = "tests/Server/test-1.db"
          t0 = systemToUTCTime MkSystemTime{systemSeconds = 0, systemNanoseconds = 0}
          t1 = systemToUTCTime MkSystemTime{systemSeconds = 24*60*30, systemNanoseconds = 0}
      doesFileExist dbName >>= flip when (removeFile dbName)
      eSQL <- runExceptT $ open dbName
      runDatabase dbName eSQL $ \sql -> do
        createUser "test-user" "password-hash" sql
        getUserIDFromName "test-user" sql           >>= assertEqual "ID" (Just 0)
        createUserSession 0 "session-key" t1 (24*60*15) sql
        isUserKeyValid "session-key" t0 sql          >>= assertBool "Key 0"
        getUserIDFromKey "session-key" sql >>= assertEqual "ID 0" (Just 0)
        isUserKeyValid "session-key-wrong" t0 sql    >>= (assertBool "Key 1" . not)
        isUserKeyValid "session-key" t1 sql >>= (assertBool "Key 0 expired" . not)
        renewUserSession "session-key" t0 sql
        isUserKeyValid "session-key" t0 sql          >>= (assertBool "Key 0")
        deleteUserSession "session-key" sql
        isUserKeyValid "session-key" t0 sql          >>= (assertBool "Key 0" . not)
    )
  , TestLabel "Whiteboard 0" $ TestCase
    ( do
      let dbName = "tests/Server/test-2.db"
      doesFileExist dbName >>= flip when (removeFile dbName)
      let now = UTCTime{utctDay = fromGregorian 2021 05 16, utctDayTime = secondsToDiffTime 0}
          t1 = UTCTime{utctDay = fromGregorian 2021 05 17, utctDayTime = secondsToDiffTime 0}
          t2 = UTCTime{utctDay = fromGregorian 2021 05 18, utctDayTime = secondsToDiffTime 0}
          info0 = WhiteboardInfo
                  { wiName  = "my whiteboard"
                  , wiOwner = Just "test-user"
                  , wiKey   = "Key"
                  , wiModified = now
                  , wiPages = 1
                  , wiPublic = False
                  , wiArchived = False
                  }
          info1 = WhiteboardInfo
                  { wiName  = "my whiteboard 2"
                  , wiOwner = Nothing
                  , wiKey   = "Key 2"
                  , wiModified = now
                  , wiPages = 1
                  , wiPublic = True
                  , wiArchived = False
                  }
          info2 = WhiteboardInfo
                  { wiName  = "public whiteboard"
                  , wiOwner = Nothing
                  , wiKey   = "KeyP"
                  , wiModified = now
                  , wiPages = 1
                  , wiPublic = True
                  , wiArchived = False
                  }
      eSQL <- runExceptT $ open dbName
      runDatabase dbName eSQL $ \sql -> do
        createUser "test-user" "password-hash" sql
        createUser "test-user-2" "password-hash" sql
        getUserIDFromName "test-user"   sql     >>= assertEqual "User ID" (Just 0)
        getUserIDFromName "test-user-2" sql     >>= assertEqual "User ID" (Just 1)
        createWhiteboard "my whiteboard" "Key" 0 now False sql
        getWhiteboardInfo (WhiteboardTopLevelID 0) sql >>= 
          assertEqual "whiteboard info" (Just info0)
        getWhiteboardInfo (WhiteboardPageID 0 1) sql >>= 
          assertEqual "whiteboard info" (Just info0)
        createWhiteboard "my whiteboard 2" "Key 2" 3 now True sql
        getWhiteboardInfo (WhiteboardTopLevelID 1) sql >>= 
          assertEqual "whiteboard info" (Just info1)
        deleteWhiteboardIfOrphan (WhiteboardTopLevelID 1) now sql
        giveWhiteboardAccess 0 (WhiteboardTopLevelID 0) sql
        hasWhiteboardAccess 0 (WhiteboardTopLevelID 0) sql >>= assertBool "Access"
        boardExists "Key" sql >>= assertBool  "Whiteboard exists"
        getWhiteboardID  "Key" sql >>= assertEqual "Whiteboard ID" (Just $ WhiteboardTopLevelID 0)
        getWhiteboardInfo (WhiteboardTopLevelID 0) sql >>= assertEqual "Whiteboard info" (Just info0)
        accessibleWhiteboards 1 now sql >>= assertEqual "Accessible boards" []
        listWhiteboards sql >>= assertEqual "whiteboard0" [info0]
        giveWhiteboardAccess 1 (WhiteboardTopLevelID 0) sql
        accessibleWhiteboards 1 now sql >>=
          assertEqual "Accessible boards" [info0]
        accessibleWhiteboards 0 now sql >>=
          assertEqual "Accessible boards" [info0]
        removeWhiteboardAccess 1 (WhiteboardTopLevelID 0) sql
        accessibleWhiteboards 1 now sql >>= assertEqual "Accessible boards" []
        deleteWhiteboardIfOrphan (WhiteboardTopLevelID 0) now sql
        accessibleWhiteboards 0 now sql >>= assertEqual "Accessible boards" [info0]
        archivedWhiteboards 0 now sql >>= assertEqual "Archived boards" []
        removeWhiteboardAccess 0 (WhiteboardTopLevelID 0) sql
        accessibleWhiteboards 0 now sql >>= assertEqual "Accessible boards" []
        archiveWhiteboardAccess 0 (WhiteboardTopLevelID 0) t1 sql
        accessibleWhiteboards 0 now sql >>= assertEqual "Accessible boards" [info0{wiArchived = True}]
        archivedWhiteboards 0 now sql >>= assertEqual "Archived boards" [info0{wiArchived = True}]
        cleanArchive now sql
        archivedWhiteboards 0 now sql >>= assertEqual "Archived boards" [info0{wiArchived = True}]
        cleanArchive t2 sql
        createWhiteboard "public whiteboard" "KeyP" 0 now True sql
        accessibleWhiteboards 0 t2 sql >>= assertEqual "Accessible boards"
          [ info2 ]
        removeWhiteboardAccess 0 (WhiteboardTopLevelID 1) sql
        accessibleWhiteboards 0 t2 sql >>= assertEqual "Accessible boards" [ info2 ]
        deleteWhiteboardIfOrphan (WhiteboardTopLevelID 0) t2 sql
        boardExists "Key" sql >>= \ex -> assertBool  "Whiteboard 0 exists" (not ex)
        deleteWhiteboardIfOrphan (WhiteboardTopLevelID 1) t2 sql
        createWhiteboard "my whiteboard" "Key" (-1) t2 True sql
        listWhiteboards sql >>= assertEqual "whiteboard0" [info0{wiModified = t2, wiPublic = True, wiOwner = Nothing}]
        deleteWhiteboardIfOrphan (WhiteboardTopLevelID 0) t2 sql
        listWhiteboards sql >>= assertEqual "no whiteboards" []
        createWhiteboard "my whiteboard" "Key" 0 now False sql
        listWhiteboards sql >>= assertEqual "no whiteboards" [info0]
        removeWhiteboardAccess 0 (WhiteboardTopLevelID 0) sql
        archiveWhiteboardAccess 0 (WhiteboardTopLevelID 0) t1 sql
        cleanArchive t2 sql
        accessibleWhiteboards 0 t2 sql >>= assertEqual "Accessible boards" [ ]
    )
  ]

runDatabase dbName eSQL f = 
  case eSQL of
    Left err -> do
      removeFile dbName
      assertBool err False
    Right sql -> do
      f sql
      close sql
      removeFile dbName
main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
