module Main where

import Zauboard.Package
import Zauboard

import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)
import Data.Time.Clock.System

tests = TestList                         
  [ TestLabel "Parse login" $ TestCase
    ( do
      assertEqual "Login as Guest"
                  (Just LoginAsGuest)
                  (parseLogin "loginasguest")
      assertEqual "Login as Guest"
                  (Just LoginAsGuest)
                  (parseLogin $ show LoginAsGuest)
      assertEqual "Login with user token"
                  (Just $ LoginWithUserToken "user token")
                  (parseLogin "loginwithusertoken\nuser token")
      assertEqual "Login with user password"
                  (Just $ LoginWithUserPassword "user 1" "password")
                  (parseLogin "loginwithuserpassword\nuser 1\npassword")
      assertEqual "Login with global password"
                  (Just $ LoginWithGlobalPassword "secret password")
                  (parseLogin "loginwithglobalpassword\nsecret password")
    )
  , TestLabel "Parse package" $ TestCase
    ( do
      assertEqual "Login as Guest"
                  (Just LoginAsGuest)
                  (parsePackage "loginasguest")
      assertEqual "Login with user token"
                  (Just $ LoginWithUserToken "user token")
                  (parsePackage "loginwithusertoken\nuser token")
      assertEqual "Login with global password"
                  (Just $ LoginWithGlobalPassword "secret password")
                  (parsePackage "loginwithglobalpassword\nsecret password")
      assertEqual "Switch page"
                  (Just $ SwitchPage 1)
                  (parsePackage "switchpage\n1")
      assertEqual "Drawings"
                  (Just $ Drawings [Drawing (ElementID 0 0, Circle (0,0) 1 "#000000" 10)])
                  (parsePackage "drawings\n1\n[{\"u\":0,\"i\":0,\"d\":0,\"c\":\"#000000\",\"r\":10,\"h\":1,\"x\":0,\"y\":0}]")
      assertEqual "Drawings"
                  (Just $ Drawings [Drawing (ElementID 1 2, Circle (4,4) 1 "#FF0000" 10)])
                  (parsePackage $ show $ Drawings [Drawing (ElementID 1 2, Circle (4,4) 1 "#FF0000" 10)])
      assertEqual "Change password"
                  (Just $ ChangePassword "oldpass" "newpass")
                  (parsePackage "changepassword\noldpass\nnewpass")
      assertEqual "CursorUpdate"
                  (Just $ CursorUpdate (0,0) Nothing)
                  (parsePackage "cursorupdate\n0 0")
      assertEqual "CursorUpdate"
                  (Just $ CursorUpdate (619,519) (Just 1))
                  (parsePackage "cursorupdate\n619 519 1")
      assertEqual "Restore"
                  (Just $ Restore [ElementID 0 0])
                  (parsePackage "restore\n0 0")
      assertEqual "Restore"
                  (Just $ Restore [ElementID 0 0, ElementID 1 2])
                  (parsePackage "restore\n0 0\n1 2")
      assertEqual "Erase"
                  (Just $ Erase [ElementID 0 0])
                  (parsePackage "erase\n0 0")
      assertEqual "Request"
                  (Just $ Request [ElementID 0 0])
                  (parsePackage "request\n0 0")
      assertEqual "Provide"
                  (Just $ Provide [Drawing (ElementID 0 0, Circle (0,0) 1 "#000000" 10)])
                  (parsePackage "provide\n1\n[{\"u\":0,\"i\":0,\"d\":0,\"c\":\"#000000\",\"r\":10,\"h\":1,\"x\":0,\"y\":0}]")
      assertEqual "Provide"
                  (Just $ Provide [Drawing (ElementID 1 2, Circle (4,4) 1 "#FF0000" 10)])
                  (parsePackage $ show $ Provide [Drawing (ElementID 1 2, Circle (4,4) 1 "#FF0000" 10)])
      assertEqual "SetUserID"
                  (Just $ SetUserID 0)
                  (parsePackage "setuserid\n0")
      assertEqual "SetUserID"
                  (Just $ SetUserID 0)
                  (parsePackage $ show (SetUserID 0))
      assertEqual "Success"
                  (Just $ Success)
                  (parsePackage "success")
      assertEqual "JoinWhiteboard"
                  (Just $ JoinWhiteboard "Key" 1)
                  (parsePackage "joinwhiteboard\nKey\n1")
      assertEqual "ListWhiteboards"
                  (Just $ ListWhiteboards)
                  (parsePackage "listwhiteboards")
      let t0 = systemToUTCTime MkSystemTime{systemSeconds = 43200, systemNanoseconds = 0}
      let wb0 = WhiteboardInfo{wiKey = "Key", wiName = "my whiteboard", wiOwner = Nothing, wiModified = t0, wiPublic = True, wiPages = 1, wiArchived = False}
      assertEqual "Whiteboards"
                  (Just $ Whiteboards [wb0])
                  (parsePackage "whiteboards\nKey 1 43200 true false my whiteboard\nuser")
      assertEqual "CreateWhiteboard"
                  (Just $ CreateWhiteboard "my whiteboard" True)
                  (parsePackage "createwhiteboard\nmy whiteboard\ntrue")
      assertEqual "CreateWhiteboard"
                  (Just $ CreateWhiteboard "New board" True)
                  (parsePackage "createwhiteboard\nNew board\ntrue")
      assertEqual "SetTag"
                  (Just $ SetTag UserTag{uUserName = "username", uTagBG = "#FF0000", uTagFG = "#00FF00", uID = Nothing})
                  (parsePackage "settag\nusername\n#00FF00\n#FF0000")
      assertEqual "SetTag"
                  (Just $ SetTag UserTag{uUserName = "username", uTagBG = "#FF0000", uTagFG = "#00FF00", uID = Just 1})
                  (parsePackage "settag\nusername\n#00FF00\n#FF0000\n1")
      assertEqual "Clear"
                  (Just $ Clear)
                  (parsePackage "clear")
      let importPDF = ImportPDF PDFImport
                            { pdfID = "930f272c19150aed4f060a2d2c7437ba19ddc7b4"
                            , pdfToWhiteboardPage = 1
                            , pdfSinglePage = False
                            , pdfPages = [PageRange (Just 1) (Just 1)]
                            , pdfScale = 1.0
                            }
      assertEqual "Import PDF"
                  (Just $ importPDF)
                  (parsePackage "importpdf\n930f272c19150aed4f060a2d2c7437ba19ddc7b4\n1\n1\n1\nfalse")
      assertEqual "Import PDF"
                  (Just $ importPDF)
                  (parsePackage $ show importPDF)
      assertEqual "Syntax error"
                  (Nothing)
                  (parsePackage "claer")

    )
  , TestLabel "Is broadcast" $ TestCase
    ( do
      assertBool "Drawings" (isBroadcast $ Drawings [])
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
