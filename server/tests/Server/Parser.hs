module Main where

import Zauboard.Parser
import Zauboard

import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)
import Data.List

tests = TestList                         
  [ TestLabel "Parse login" $ TestCase
    ( do
      assertEqual "Login as Guest"
                  (Just LoginAsGuest)
                  (parseLogin "loginasguest")
      assertEqual "Login with user token"
                  (Just $ LoginWithUserToken "user token")
                  (parseLogin "loginwithusertoken\nuser token")
      assertEqual "Login with global password"
                  (Just $ LoginWithGlobalPassword "secret password")
                  (parseLogin "loginwithglobalpassword\nsecret password")
    )
  , TestLabel "Parse package" $ TestCase
    ( do
      assertEqual "Login as Guest"
                  (Just LoginAsGuest)
                  (parsePackage "loginasguest")
      assertEqual "Login with user token"
                  (Just $ LoginWithUserToken "user token")
                  (parsePackage "loginwithusertoken\nuser token")
      assertEqual "Login with global password"
                  (Just $ LoginWithGlobalPassword "secret password")
                  (parsePackage "loginwithglobalpassword\nsecret password")
      assertEqual "Switch page"
                  (Just $ SwitchPage 1)
                  (parsePackage "switchpage\n1")
      assertEqual "Drawings"
                  (Just $ Drawings [Drawing (ElementID 0 0, Circle (0,0) 1 "#000000" 10)])
                  (parsePackage "drawings\n[{\"u\":0,\"i\":0,\"d\":0,\"c\":\"#000000\",\"r\":10,\"h\":1,\"x\":0,\"y\":0}]")
      assertEqual "CursorUpdate"
                  (Just $ CursorUpdate (0,0))
                  (parsePackage "cursorupdate\n0 0")
      assertEqual "Restore"
                  (Just $ Restore [ElementID 0 0])
                  (parsePackage "restore\n0 0")
      assertEqual "Restore"
                  (Just $ Restore [ElementID 0 0, ElementID 1 2])
                  (parsePackage "restore\n0 0\n1 2")
      assertEqual "Erase"
                  (Just $ Erase [ElementID 0 0])
                  (parsePackage "erase\n0 0")
      assertEqual "Request"
                  (Just $ Request [ElementID 0 0])
                  (parsePackage "request\n0 0")
      assertEqual "Provide"
                  (Just $ Provide [Drawing (ElementID 0 0, Circle (0,0) 1 "#000000" 10)])
                  (parsePackage "provide\n[{\"u\":0,\"i\":0,\"d\":0,\"c\":\"#000000\",\"r\":10,\"h\":1,\"x\":0,\"y\":0}]")
      assertEqual "SetUserID"
                  (Just $ SetUserID 0)
                  (parsePackage "setuserid\n0")
      assertEqual "JoinWhiteboard"
                  (Just $ JoinWhiteboard "Key" 1)
                  (parsePackage "joinwhiteboard\nKey\n1")
      assertEqual "SetTag"
                  (Just $ SetTag UserTag{uUserName = "username", uTagBG = "#FF0000", uTagFG = "#00FF00"} Nothing)
                  (parsePackage "settag\nusername\n#00FF00\n#FF0000")
      assertEqual "Clear"
                  (Just $ Clear)
                  (parsePackage "clear")
      assertEqual "Syntax error"
                  (Nothing)
                  (parsePackage "claer")

    )
  , TestLabel "Pack drawings" $ TestCase
    ( do
      let c0 = Circle (0,0) 1 "#FF0000" 10
          d0 = Drawing (ElementID 0 0, c0)
          d0Str = "{\"d\":0,\"u\":0,\"i\":0,\"x\":0.0,\"y\":0.0,\"r\":10.0,\"c\":\"#FF0000\",\"h\":1.0}"
          p0 = Path [(0,1), (2,4)] [5] "#00FF00" True
          d1 = Drawing (ElementID 0 1, p0)
          d1Str = "{\"d\":1,\"u\":0,\"i\":1,\"p\":[0.0,1.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[5.0]}"
          p1 = Path [(1,0), (4,2), (2,4)] [8,7] "#00FF00" False
          d2 = Drawing (ElementID 1 2, p1)
          d2Str = "{\"d\":2,\"u\":1,\"i\":2,\"p\":[1.0,0.0,4.0,2.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[8.0,7.0]}"
      assertEqual "Pack circle 0"
                  ("[" ++ d0Str ++ "]")
                  (packDrawings [d0])
      assertEqual "Pack path 0"
                  ("[" ++ d1Str ++ "]")
                  (packDrawings [d1])
      assertEqual "Pack path 1"
                  ("[" ++ d2Str ++ "]")
                  (packDrawings [d2])
      assertEqual "Pack all"
                  ("[" ++ intercalate "," [d0Str,d1Str,d2Str] ++ "]")
                  (packDrawings [d0,d1,d2])
    )
  , TestLabel "Load drawings v1" $ TestCase
    ( do
      let c0 = Circle (0,0) 1 "#FF0000" 10
          d0 = Drawing (ElementID 0 0, c0)
          d0Str = "{\"d\":0,\"u\":0,\"i\":0,\"x\":0.0,\"y\":0.0,\"r\":10.0,\"c\":\"#FF0000\",\"h\":1.0}"
          p0 = Path [(0,1), (2,4)] [5] "#00FF00" True
          d1 = Drawing (ElementID 0 1, p0)
          d1Str = "{\"d\":1,\"u\":0,\"i\":1,\"p\":[0.0,1.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[5.0]}"
          p1 = Path [(1,0), (4,2), (2,4)] [8,7] "#00FF00" False
          d2 = Drawing (ElementID 1 2, p1)
          d2Str = "{\"d\":2,\"u\":1,\"i\":2,\"p\":[1.0,0.0,4.0,2.0,2.0,4.0],\"c\":\"#00FF00\",\"h\":[8.0,7.0]}"
      assertEqual "Unpack circle 0"
                  (Just [d0])
                  (loadDrawings "1" $ "[" ++ d0Str ++ "]")
      assertEqual "Unpack path 0"
                  (Just [d1])
                  (loadDrawings "1" $ "[" ++ d1Str ++ "]")
      assertEqual "Unpack path 1"
                  (Just [d2])
                  (loadDrawings "1" $ "[" ++ d2Str ++ "]")
      assertEqual "Unpack all"
                  (Just [d0,d1,d2])
                  (loadDrawings "1" $ "[" ++ intercalate "," [d0Str, d1Str, d2Str] ++ "]")
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
