module Zauboard.Export
       ( Context
       , Resource
       , request
       , free
       , update
       , takeValue
       , produce
       , create
       )
where

import Control.Concurrent
import qualified Data.Map as M
import           Control.Monad.State
import           Control.Exception 

data Context a b = Context
  { cxResources :: MVar (M.Map a (Resource b))
  , cxToUpdate  :: MVar (M.Map a (Resource b))
  }

data Resource a = 
  Resource
  { resConsumers :: MVar Int
  , resValue     :: MVar a
  , resProducer  :: MVar ()
  }

create = do
  resources <- newMVar M.empty
  updates   <- newMVar M.empty
  return $ Context{cxResources = resources, cxToUpdate = updates}

takeValue res = readMVar (resValue res)

produce f res = do
  takeMVar $ resProducer res
  v' <- try f
  case v' of
    Left err -> do
      putStrLn "Error while producing resource:"
      print (err :: SomeException)
    Right v -> do
      putMVar (resValue res) v
  putMVar (resProducer res) ()

request :: (Eq a, Ord a) => a -> b -> StateT (Context a b) IO (Resource b)
request key defValue = do
  cx <- get
  liftIO $ do
    resources <- takeMVar (cxResources cx)
    case M.lookup key resources of
      Just res -> do
        putMVar (cxResources cx) resources
        modifyMVar_ (resConsumers res) (\x -> return $ x + 1)
        return res
      Nothing -> do
        consumers <- newMVar 1
        value     <- newMVar defValue
        producer  <- newEmptyMVar
        let res = Resource{resConsumers = consumers, resValue = value, resProducer = producer}
        putMVar (cxResources cx) $ M.insert key res resources
        return res

free :: (Eq a, Ord a) => a -> (Resource b) -> StateT (Context a b) IO ()
free key res = do
  cx <- get
  liftIO $ do
    resources <- takeMVar $ cxResources cx
    consumers <- takeMVar $ resConsumers res
    if consumers == 1 then do
      updates <- takeMVar $ cxToUpdate cx
      case M.lookup key updates of
        Just res' -> do
          putMVar (resProducer res') ()
          putMVar (cxToUpdate cx) $ M.delete key updates
          putMVar (cxResources cx) $ M.insert key res' resources
        Nothing -> do
          putMVar (cxToUpdate cx) updates
          putMVar (cxResources cx) $ M.delete key resources
      putMVar (resConsumers res) 0
    else do
      putMVar (cxResources cx) resources
      putMVar (resConsumers res) (consumers - 1)

update :: (Eq a, Ord a) => a -> StateT (Context a b) IO (Resource b)
update key = do
  cx <- get
  liftIO $ do
    updates <- takeMVar (cxToUpdate cx)
    case M.lookup key updates of
      Just res' -> do
        putMVar (cxToUpdate cx) updates
        modifyMVar_ (resConsumers res') (\x -> return $ x + 1)
        return res'
      Nothing -> do
        resources <- takeMVar (cxResources cx)
        consumers <- newMVar 1
        value <- newEmptyMVar
        producer <- newMVar ()
        let res = Resource{resConsumers = consumers, resValue = value, resProducer = producer}
        case M.lookup key resources of
          Just _ -> do
            putMVar (cxToUpdate cx) $ M.insert key res updates
            putMVar (cxResources cx) resources
          Nothing -> do
            putMVar (cxToUpdate cx) updates
            putMVar (cxResources cx) $ M.insert key res resources
        return res
