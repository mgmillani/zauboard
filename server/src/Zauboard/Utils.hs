module Zauboard.Utils
      ( escapeNewlines
      , escapeQuotes
      , mhead
      , mlast
      , parseAttributeList
      , readUnit
      , repeatLoop
      , secondsToUTC
      , unescapeString
      , secondsToPico
      , secondsToMicro
      )
where

import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.Trans.State
import Data.Char
import Data.Fixed
import Data.List
import Data.Time
import Data.Time.Clock.System
import Text.Read (readMaybe)

mhead [] = Nothing
mhead (x:_) = Just x

mlast [] = Nothing
mlast xs = Just $ last xs

repeatLoop interval f = do
  now <- getCurrentTime
  loop now
  where
    loop lastCall = do
      now <- getCurrentTime
      let tdiff = diffUTCTime now lastCall
          MkFixed intervalPico = nominalDiffTimeToSeconds $ tdiff
          delay = fromIntegral $ min (picoToMicro intervalPico) (secondsToMicro 60) :: Int
      if (tdiff > interval) then do
        result <- try f
        case result of
          Left err -> do
            putStrLn "Repeat loop"
            print (err :: SomeException)
            threadDelay delay
            loop now
          Right reiterate -> do
            when reiterate $ do
              threadDelay delay
              loop now
      else do
        threadDelay delay
        loop lastCall
-- TODO: Use proper type which contains unit as well
readUnit :: String -> Maybe Double
readUnit str = 
  let (n, u) = span (\c -> isNumber c || c =='.' || c == '-') str
  in case map toLower u of
      "pt" -> fmap (*1.333333) $ readMaybe n
      ""   -> fmap (*1.333333) $ readMaybe n
      "px" -> readMaybe n
      _ -> Nothing -- TODO: Other units

escapeQuotes [] = []
escapeQuotes ('"':cs) = '\\':'"' : escapeQuotes cs
escapeQuotes (c:cs) = c : escapeQuotes cs

escapeNewlines [] = []
escapeNewlines ('\n':cs) = '\\':'n' : escapeNewlines cs
escapeNewlines (c:cs) = c : escapeNewlines cs

unescapeString str
  | null str = ""
  | "\\\"" `isPrefixOf` str = '"' : unescapeString (drop 3 str)
  | "\\n" `isPrefixOf` str = '\n' : unescapeString (drop 2 str)
  | otherwise = (head str) : (unescapeString $ tail str)

parseAttributeList = evalState attrList

attrList :: State String [(String, String)]
attrList = 
  whenNotEmpty [] $ do
    consumeWhile isSpace
    var <- consumeWhile (/='=')
    consumeWhile (=='=')
    val <- quoted
    fmap ((var,val):) attrList

quoted :: State String String
quoted = do
  consumeWhile (=='"')
  str <- consumeWhile (/='"')
  consumeWhile (=='"')
  return str

consumeWhile :: (Char -> Bool) -> State String String
consumeWhile f = do
  str <- get
  let (x,r) = span f str
  put r
  return x

whenNotEmpty e x = do
  s <- get
  if s == "" then return e else x

-- picoToSeconds = (`div` (10^12))
picoToMicro :: Integral a => a -> a
picoToMicro = (`div` (10^6))
secondsToMicro :: Integral a => a -> a
secondsToMicro = (*(10^6))
secondsToPico  = (*(10^12))
  
secondsToUTC s = systemToUTCTime $
                    MkSystemTime 
                      { systemSeconds = s
                      , systemNanoseconds = 0}

