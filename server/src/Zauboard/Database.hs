module Zauboard.Database
       ( Database(..)
       , ZauboardDB(..)
       , openPostGreSQL
       , openSQLite
       )
where

import Zauboard.Database.Interface
import Zauboard.Database.PostGreSQL as PG
import Zauboard.Database.SQLite     as SQ
import Control.Monad.Except

data Database = PostGreSQL PG.PostGreDB | SQLite SQ.SQLiteDB

openPostGreSQL :: String -> Int -> String -> String -> String -> ExceptT String IO Database
openPostGreSQL host port user pass database = (fmap PostGreSQL) $ PG.open host port user pass database
openSQLite :: FilePath -> ExceptT String IO Database
openSQLite fl  = (fmap SQLite) $ SQ.open fl

-- TODO: Use template haskell for this

instance ZauboardDB Database where
  accessibleWhiteboards ui now (PostGreSQL db)        = accessibleWhiteboards ui now db
  accessibleWhiteboards ui now (SQLite db)            = accessibleWhiteboards ui now db
  archivedWhiteboards ui now (PostGreSQL db)          = archivedWhiteboards ui now db
  archivedWhiteboards ui now (SQLite db)              = archivedWhiteboards ui now db
  archiveWhiteboardAccess ui bk now (PostGreSQL db)   = archiveWhiteboardAccess ui bk now db
  archiveWhiteboardAccess ui bk now (SQLite db)       = archiveWhiteboardAccess ui bk now db
  boardExists bk (PostGreSQL db)                      = boardExists bk db
  boardExists bk (SQLite db)                          = boardExists bk db
  cleanArchive now (SQLite db)                        = cleanArchive now db
  cleanArchive now (PostGreSQL db)                    = cleanArchive now db
  createUserSession ui uk t d (PostGreSQL db)         = createUserSession ui uk t d db
  createUserSession ui uk t d (SQLite db)             = createUserSession ui uk t d db
  createUser un ph (PostGreSQL db)                    = createUser un ph db
  createUser un ph (SQLite db)                        = createUser un ph db
  createWhiteboard bn bk ui t p (PostGreSQL db)       = createWhiteboard bn bk ui t p db
  createWhiteboard bn bk ui t p (SQLite db)           = createWhiteboard bn bk ui t p db
  deleteUserSession uk (PostGreSQL db)                = deleteUserSession uk db
  deleteUserSession uk (SQLite db)                    = deleteUserSession uk db
  deleteUser ui (PostGreSQL db)                       = deleteUser ui db
  deleteUser ui (SQLite db)                           = deleteUser ui db
  deleteWhiteboard bi (PostGreSQL db)                 = deleteWhiteboard bi db
  deleteWhiteboard bi (SQLite db)                     = deleteWhiteboard bi db
  getWhiteboardID bk (PostGreSQL db)                  = getWhiteboardID bk db
  getWhiteboardID bk (SQLite db)                      = getWhiteboardID bk db
  getWhiteboardInfo bk (PostGreSQL db)                = getWhiteboardInfo bk db
  getWhiteboardInfo bk (SQLite db)                    = getWhiteboardInfo bk db
  getPasswordHash ui (PostGreSQL db)                  = getPasswordHash ui db
  getPasswordHash ui (SQLite db)                      = getPasswordHash ui db
  getUserIDFromKey uk (PostGreSQL db)                 = getUserIDFromKey uk db
  getUserIDFromKey uk (SQLite db)                     = getUserIDFromKey uk db
  getUserIDFromName un (PostGreSQL db)                = getUserIDFromName un db
  getUserIDFromName un (SQLite db)                    = getUserIDFromName un db
  getUsername uk (PostGreSQL db)                      = getUsername uk db
  getUsername uk (SQLite db)                          = getUsername uk db
  getModificationDate bi (PostGreSQL db)              = getModificationDate bi db
  getModificationDate bi (SQLite db)                  = getModificationDate bi db
  giveWhiteboardAccess uk bk (PostGreSQL db)          = giveWhiteboardAccess uk bk db
  giveWhiteboardAccess uk bk (SQLite db)              = giveWhiteboardAccess uk bk db
  isUserKeyValid uk t (PostGreSQL db)                 = isUserKeyValid uk t db
  isUserKeyValid uk t (SQLite db)                     = isUserKeyValid uk t db
  isWhiteboardArchived ui bi (PostGreSQL db)          = isWhiteboardArchived ui bi db
  isWhiteboardArchived ui bi (SQLite db)              = isWhiteboardArchived ui bi db
  listUsers       (PostGreSQL db)                     = listUsers db
  listUsers       (SQLite db)                         = listUsers db
  listWhiteboards (PostGreSQL db)                     = listWhiteboards db
  listWhiteboards (SQLite db)                         = listWhiteboards db
  removeArchiveAccess ui bi (PostGreSQL db)           = removeArchiveAccess ui bi db
  removeArchiveAccess ui bi (SQLite db)               = removeArchiveAccess ui bi db
  removeExpiredData now (SQLite db)                   = removeExpiredData now db
  removeExpiredData now (PostGreSQL db)               = removeExpiredData now db
  removeWhiteboardAccess ui bi (PostGreSQL db)        = removeWhiteboardAccess ui bi db
  removeWhiteboardAccess ui bi (SQLite db)            = removeWhiteboardAccess ui bi db
  deleteWhiteboardIfOrphan bi now (PostGreSQL db)     = deleteWhiteboardIfOrphan bi now db
  deleteWhiteboardIfOrphan bi now (SQLite db)         = deleteWhiteboardIfOrphan bi now db
  updateArchiveExpirationDate ui bi t (PostGreSQL db) = updateArchiveExpirationDate ui bi t db
  updateArchiveExpirationDate ui bi t (SQLite db)     = updateArchiveExpirationDate ui bi t db
  updateModificationDate bk t (PostGreSQL db)         = updateModificationDate bk t db
  updateModificationDate bk t (SQLite db)             = updateModificationDate bk t db
  updateNumberOfPages bk n (PostGreSQL db)            = updateNumberOfPages bk n db
  updateNumberOfPages bk n (SQLite db)                = updateNumberOfPages bk n db
  updatePasswordHash ui ph (PostGreSQL db)            = updatePasswordHash ui ph db
  updatePasswordHash ui ph (SQLite db)                = updatePasswordHash ui ph db
  hasWhiteboardAccess ui bi (PostGreSQL db)           = hasWhiteboardAccess ui bi db
  hasWhiteboardAccess ui bi (SQLite db)               = hasWhiteboardAccess ui bi db
  renewUserSession uk t (PostGreSQL db)               = renewUserSession uk t db
  renewUserSession uk t (SQLite db)                   = renewUserSession uk t db
  close (PostGreSQL db)                               = close db
  close (SQLite db)                                   = close db
