module Zauboard.Export.PDF
       ( generate
       )
where

import qualified Zauboard.Export.SVG as SVG
import qualified Zauboard.Export     as E
import qualified Zauboard.Whiteboard as W
import           Zauboard
import qualified Data.Map            as M
import qualified Data.Set            as S
import Control.Concurrent
import Control.Monad
import Control.Monad.Trans.State
import Control.Exception
import Data.Int
import Data.Maybe
import System.Directory
import System.FilePath
import System.Process
import System.IO

generate pdfDest context whiteboard numberOfPages wbID pageRanges = do
  let pageDir = W.cxPageDir whiteboard
      wid = topLevelID wbID
  createDirectoryIfMissing True (pageDir </> show wid </> "pdf")
  createDirectoryIfMissing True (pageDir </> show wid </> "svg")
  let pdfDir = pageDir </> (show wid) </> "pdf"
      pages = pageList numberOfPages pageRanges
      pageRequests = S.toList $ S.fromList pages
  pdfResources <- forM pageRequests $ \p -> do
    let pageID = WhiteboardPageID wid p
    let pdfPath = pdfDir </> show p <.> "pdf"
        svgPath = pdfDir </> show p <.> "svg"
    tPage <- evalStateT (W.pageModificationDate pageID) whiteboard
    tPdf <- catch (getModificationTime pdfPath >>= return . Just)
                  (\e -> seq (e :: SomeException) $ return Nothing)
    let tDiff = liftM2 (<=) (Just tPage) tPdf
    res <- case tDiff of
      Just True -> do
        res <- evalStateT (E.request (WhiteboardPageID wid p) pdfPath) context
        return res
      _ -> do
        res <- evalStateT (E.update (WhiteboardPageID wid p)) context
        forkIO $ E.produce (generatePage svgPath pdfPath whiteboard pageID) res
        return res
    return (res, p) :: IO (E.Resource FilePath, Int64)
  pdfs <- fmap M.fromList $ mapM (\(res,p) -> fmap (\v -> (p,v)) $ E.takeValue res) pdfResources
  let pageSelection = map (pdfs M.!) pages
  joinPdfs pageSelection (pdfDir </> pdfDest)
  mapM_ (\(res, p) -> evalStateT (E.free (WhiteboardPageID wid p) res) context) pdfResources
  return $ Just (pdfDir </> pdfDest)
 
generatePage svgPath pdfPath whiteboard w@(WhiteboardPageID _ _) = do
  lastModified <- evalStateT (W.pageModificationDate w) whiteboard
  drawings <- evalStateT (W.takeAllOnce w) whiteboard
  writeFile svgPath (SVG.drawingsToSVG $ map drawing drawings)
  result <- try $ do
    svgToPdf svgPath pdfPath
    setModificationTime pdfPath lastModified
  case result of
    Left err -> do
      putStrLn $ "PDF Export on " ++ show w ++ ":"
      print (err :: SomeException)
      writeFile svgPath (SVG.drawingsToSVG [])
      svgToPdf svgPath pdfPath
      setModificationTime pdfPath lastModified
    Right _ -> return ()
  removeFile svgPath
  return pdfPath

svgToPdf :: String -> String -> IO ()
svgToPdf svgPath pdfPath = do
  (_,Just sout, Just serr, ph) <- createProcess (proc "rsvg-convert" ["--format", "pdf", "--output", pdfPath, svgPath]){std_out = CreatePipe, std_err = CreatePipe}
  forkIO $ do
    outStr <- hGetContents sout
    seq (length outStr) $ return ()
  forkIO $ do
    errStr <- hGetContents serr
    seq (length errStr) $ return ()
  waitForProcess ph
  return ()

joinPdfs pdfs pdfPath = do
  readProcess "pdftk" (pdfs ++ ["cat", "output", pdfPath]) ""
  return ()

pageList :: Int64 -> [PageSelection] -> [Int64]
pageList _ [] = []
pageList numPages (PageRange p0m p1m:rs) =
  let p0 = fromMaybe 1 p0m :: Int64
      p1 = fromMaybe numPages p1m :: Int64
  in [p0..p1] ++ pageList numPages rs
