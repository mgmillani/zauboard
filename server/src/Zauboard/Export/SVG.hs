module Zauboard.Export.SVG
       ( drawingsToSVG
       , controlPoints
       , toSVG
       )
where

import Zauboard
import Control.Monad.State
import Data.List
import qualified Data.Text          as T
import qualified Data.Text.Encoding as T

svgHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<svg "
svgFooter = "</svg>"

data BoundingBox = 
  Box
  { bxTop    :: Double
  , bxBottom :: Double
  , bxLeft   :: Double
  , bxRight  :: Double
  }

emptyBox = Box{bxTop = 0, bxBottom = 0, bxLeft = 0, bxRight = 0}

drawingsToSVG [] = svgHeader ++ (viewport 20 emptyBox) ++ ">" ++ svgFooter
drawingsToSVG (d:ds) =
  let (body, box) = runState (mapM drawingToSVG (d:ds)) (boundingBox d)
  in svgHeader ++ (viewport 20 box) ++ ">" ++ (concat body) ++ svgFooter

viewport margin box = 
  "width=\"" ++ show (width )
  ++ "px\" height=\"" ++ show (height )
  ++ "px\" viewBox=\"" ++ show (bxLeft box - margin)
  ++ " " ++ show (bxTop box - margin)
  ++ " " ++ show width
  ++ " " ++ show height ++ "\""  
  where
    width  = (bxRight box) - (bxLeft box) + 2 * margin
    height =  (bxBottom box) - (bxTop box) + 2 * margin

drawingToSVG :: DrawingElement -> State BoundingBox String
drawingToSVG dr  = do
  updateBoundingBox $ boundingBox dr
  return $ toSVG dr

toSVG (Circle (x,y) thickness color r) =
  concat 
    [ "<circle style=\"fill:none;stroke:"
    , encodeColor color
    , ";stroke-width:"
    , show thickness
    , ";\" cx=\""
    , show x
    , "\" cy=\""
    , show y
    , "\" r=\""
    , show r
    , "\"/>"
    ]
toSVG (Path points thickness color Nothing) = concat $ 
  zipWith3 segment points (tail points) (cycle thickness)
  where
    segment (x0,y0) (x1,y1) th =
      lineHead
      ++ "stroke-width:"
      ++ show th
      ++ ";\" d = \"M"
      ++ show x0 ++ "," ++ show y0 ++ " " ++ show x1 ++ "," ++ show y1 ++ "\"/>"
    lineHead = concat
        [ "<path style=\"fill:none;stroke:"
        , encodeColor color
        , ";stroke-linecap:round;"
        ]
toSVG (Path points thickness color (Just (q0,qn))) = concat $ 
  segments points controls (cycle thickness)
  where
    segments [ ] _ _ = []
    segments [_] _ _ = []
    segments (p0:p1:ps) (c0:c1:cs) (th:ths) = 
      segment (p0, c0) (p1,c1) th : segments (p1:ps) cs ths
    segment ((x0,y0), (cx0, cy0)) ((x1,y1), (cx1, cy1)) th =
      lineHead
      ++ "stroke-width:"
      ++ show th
      ++ ";\" d = \"M "
      ++ show x0 ++ "," ++ show y0 ++ " C "
      ++ (intercalate " " $ map (intercalate "," . map show) [[cx0, cy0], [cx1, cy1], [x1, y1]])
      ++ "\"/>"
    lineHead = concat
        [ "<path style=\"fill:none;stroke:"
        , encodeColor color
        , ";stroke-linecap:round;"
        ]
    controls = drop 1 $ controlPoints ((q0:points) ++ [qn])
toSVG (RawSVG (x,y) scale w h svg) = 
  "<g transform=\"translate(" ++ show x ++ "," ++ show y ++ ") scale(" ++ show scale ++ ")\" viewBox=\"" ++ show x ++ " " ++ show y ++ " " ++ show (w + x) ++ " " ++ show (h + y) ++ "\">" ++ (T.unpack $ T.decodeUtf8 svg) ++ "</g>"

controlPoints qs = controlPoints' qs
  where
    controlPoints' (p0:p1:p2:ps) 
      | l10 == 0 = p0 : controlPoints' (p1:p2:ps)
      | otherwise =
        (fst p1 + ux * l0 / l10, snd p1 + uy * l0 / l10)
        : (fst p1 - ux * l1 / l10, snd p1 - uy * l1 / l10)
        : controlPoints' (p1:p2:ps)
      where
        -- v is the tangential vector of the curve at p1
        vx = (fst p2 - fst p0) / 2
        vy = (snd p2 - snd p0) / 2
        ux = -vx
        uy = -vy
        -- length of segments p0p1 and p1p2
        dx0 = fst p1 - fst p0
        dy0 = snd p1 - snd p0
        dx1 = fst p2 - fst p1
        dy1 = snd p2 - snd p1
        l0  = sqrt $ dx0*dx0 + dy0*dy0
        l1  = sqrt $ dx1*dx1 + dy1*dy1
        -- lavg = (l0 + l1) / 2
        l10 = l1 + l0
    controlPoints' ps = [last ps]

boundingBox (Circle (x,y) _ _ r) = Box{bxTop = y - r, bxBottom = y + r, bxLeft = x - r, bxRight = x + r}
boundingBox (Path [] _ _ _)      = Box{bxTop = 0, bxBottom = 0, bxLeft = 0, bxRight = 0}
boundingBox (Path points _ _ _)  = 
  let xs = map fst points
      ys = map snd points
  in Box{bxTop = minimum ys, bxBottom = maximum ys, bxLeft = minimum xs, bxRight = maximum xs}
boundingBox (RawSVG (x,y) scale w h _) = Box{bxTop = y, bxBottom = y + h * scale / 1.3333, bxLeft = x, bxRight = x + w * scale  / 1.3333} -- TODO: Properly fix that 1.333. The issue is in the conversion between pt and px

updateBoundingBox :: BoundingBox -> State BoundingBox ()
updateBoundingBox box1 = do
  box0 <- get
  put Box{ bxTop    = min (bxTop box0)    (bxTop box1)
         , bxBottom = max (bxBottom box0) (bxBottom box1)
         , bxLeft   = min (bxLeft box0)   (bxLeft box1)
         , bxRight  = max (bxRight box0)  (bxRight box1)
         }

encodeColor "black" = "#000"
encodeColor color = color
