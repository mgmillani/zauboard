module Zauboard.Package.Definition where

import Zauboard

import Data.Int

data Package = 
    AccessDenied
  | AccessGranted
  | ChangePassword String String
  | Clear
  | CreateWhiteboard WhiteboardName Bool
  | CursorUpdate   Cursor (Maybe UserID)
  | DeleteWhiteboard WhiteboardName
  | Drawings       [Drawing]
  | Erase          [ElementID]
  | Error ErrorMessage
  | ExportPDF Int [PageSelection]
  | FileReady Int String
  | GetWhiteboardInfo WhiteboardName
  | ImportPDF PDFImport
  | JoinWhiteboard WhiteboardName PageID
  | ListWhiteboards
  | LoginAsGuest
  | LoginWithGlobalPassword Password
  | LoginWithUserPassword String String
  | LoginWithUserToken   Password
  | PDFSummary String Int
  | Provide        [Drawing]
  | ProvideUserToken Password
  | Request        [ElementID]
  | RequestUserToken
  | Restore        [ElementID]
  | RestoreWhiteboard WhiteboardName
  | SetTag UserTag
  | SetUserID      UserID
  | Success
  | SwitchPage     PageID
  | SyntaxError
  | UploadPDF String
  | Whiteboards [WhiteboardInfo]
  deriving (Eq, Ord)

data ErrorMessage =
  IncorrectPassword
  deriving (Eq, Ord)

instance Show ErrorMessage where
  show IncorrectPassword = "incorrectpassword"

isBroadcast pkg = case pkg of
  SwitchPage _ -> True
  Drawings _ -> True
  CursorUpdate _ _ -> True
  Restore _ -> True
  Erase _ -> True
  Clear -> True
  SetTag _ -> True
  _ -> False

data PDFImport = PDFImport
  { pdfID    :: String
  , pdfPages :: [PageSelection]
  , pdfToWhiteboardPage :: Int64
  , pdfScale :: Double
  , pdfSinglePage :: Bool
  } deriving (Eq, Ord)

