{-# LANGUAGE OverloadedStrings #-}
module Zauboard.Drawing
       ( packDrawings
       , loadDrawings
       , parseElementIDs
       )
where

import Control.Monad
import Data.Aeson
import qualified Data.HashMap.Strict as H
import Data.Aeson.Types as A
import qualified Data.Aeson.KeyMap as A
import Data.Aeson.Parser
import Data.List
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.Text               as T
import qualified Data.Text.Lazy          as TL
import qualified Data.Text.Encoding      as T
import qualified Data.Text.Lazy.Encoding as TL
import qualified Data.Vector as V
import Text.Read
import Zauboard
import Zauboard.Utils

parseElementIDs str = 
  let eIDs = [ meID
             | ln <- lines str
             , let meID = case words ln of
                             [userIDStr, drawIDStr] ->
                               let muid = readMaybe userIDStr :: Maybe UserID
                                   mdid = readMaybe drawIDStr :: Maybe DrawID
                               in liftM2 ElementID muid mdid
                             _ -> Nothing
             ]
  in sequence eIDs

loadDrawings :: String -> BS.ByteString -> Maybe [Drawing]
loadDrawings "0" = decodeWith json (parse parseDrawingsV0)
loadDrawings "1" = decodeWith json (parse parseDrawingsV1)
loadDrawings _   = \_ -> Nothing

parseDrawingsV1 = withArray "Drawing list" $ mapM parseDrawingV1 . V.toList

parseDrawingV1 = withObject "Drawing" $ \v -> (curry Drawing)
    <$> (parseElementID v)
    <*> parseDrawing v
  where
    parseDrawing :: Object -> Parser DrawingElement
    parseDrawing o = do
      dCode <- o .: "d" :: Parser Int
      case dCode of
        0 -> Circle
              <$> ((,) <$> (o .: "x") <*> (o .: "y"))
              <*> o .: "h"
              <*> o .: "c"
              <*> o .: "r"
        1 -> Path
              <$> (o .: "p" >>= toPositions)
              <*> o .: "h"
              <*> o .: "c"
              <*> (o .: "o" >>= toPositions >>= \ps ->
                            case ps of
                              [p0,p1] -> return $ Just (p0,p1)
                              _ -> fail "Wrong number of control points."
                  )
        2 -> Path
              <$> (o .: "p" >>= toPositions)
              <*> o .: "h"
              <*> o .: "c"
              <*> return Nothing
        3 -> RawSVG
              <$> ((,) <$> (o .: "x") <*> (o .: "y"))
              <*> o .: "c"
              <*> o .: "w"
              <*> o .: "h"
              <*> ((T.encodeUtf8 . T.pack) <$> (o .: "s"))
        _ -> fail $ "Invalid code" ++ show (dCode)
    parseElementID :: Object -> Parser ElementID
    parseElementID o = ElementID 
      <$> o .: "u"
      <*> o .: "i"

parseDrawingsV0 = withArray "Drawing list" $ mapM (uncurry parseDrawingV0) . zip [0..] . V.toList

parseDrawingV0 i = withObject "Drawing" $ \v -> (curry Drawing)
    <$> (parseElementID v)
    <*> parseDrawing v
  where
    parseDrawing o = do
      dCode <- o .: "t" :: Parser Int
      case dCode of
        1 -> Circle
              <$> ((,) <$> (o .: "x") <*> (o .: "y"))
              <*> o .: "h"
              <*> o .: "c"
              <*> o .: "r"
        -1 -> do
          pos <- o .: "d" >>= toPositions
          th <- o .: "th"
          c <- o .: "c"
          return $ Path (drop 1 pos) th c (liftM2 (,) (mhead pos) (mlast pos))
        0 -> Path
              <$> (o .: "d" >>= toPositions)
              <*> (parseThickness o)
              <*> o .: "c"
              <*> (pure Nothing)
        2 -> Path
              <$> (o .: "d" >>= toPositions)
              <*> (parseThickness o)
              <*> (pure "#FFFFFF")
              <*> (pure Nothing)
        _ -> fail "Wrong tool"
    parseElementID o = ElementID 
      <$> o .: "user_id"
      <*> (return i)
    parseThickness o = 
      case A.lookup (fromJust $ decode $ TL.encodeUtf8 $ TL.pack "\"th\"") o of
        Just a@(Array _)    -> parseJSON a
        Just n@(A.Number _) -> fmap (:[]) $ parseJSON n
        Just n@(A.String _) -> do
          str <- parseJSON n
          case (readMaybe str) :: Maybe Double of
            Just t -> return [t]
            Nothing -> fail "Wrong type."
        _ -> fail "Wrong type"

--parseDrawings str = case decode str >>= return . parse parseDrawingsV1 of
--  Just (Success r) -> Just r
--  _ -> Nothing
packDrawings drawings = '[' : (intercalate "," $ map packDrawing drawings) ++ "]"
  where
    packDrawing (Drawing (ElementID userID drawID, Circle pos thickness color r)) = intercalate ","
      [ "{\"d\":0"
      , "\"u\":" ++ show userID
      , "\"i\":" ++ show drawID
      , "\"x\":" ++ (show $ fst pos)
      , "\"y\":" ++ (show $ snd pos)
      , "\"r\":" ++ (show r)
      , "\"c\":\"" ++ color ++ "\""
      , "\"h\":" ++ (show thickness) ++ "}"
      ]
    packDrawing (Drawing (ElementID userID drawID, Path pos thickness color (Just ((p0x, p0y), (p1x, p1y)) ))) = intercalate ","
      [ "{\"d\":1"
      , "\"u\":" ++ show userID
      , "\"i\":" ++ show drawID
      , "\"p\":" ++ (packList $ linearize pos)
      , "\"o\":" ++ packList [p0x, p0y, p1x, p1y]
      , "\"c\":\"" ++ color ++ "\""
      , "\"h\":" ++ packList thickness ++ "}"
      ]
    packDrawing (Drawing (ElementID userID drawID, Path pos thickness color Nothing)) = intercalate ","
      [ "{\"d\":2"
      , "\"u\":" ++ show userID
      , "\"i\":" ++ show drawID
      , "\"p\":" ++ (packList $ linearize pos)
      , "\"c\":\"" ++ color ++ "\""
      , "\"h\":" ++ packList thickness ++ "}"
      ]
    packDrawing (Drawing (ElementID userID drawID, RawSVG pos scale width height svg)) = intercalate ","
      [ "{\"d\":3"
      , "\"u\":" ++ show userID
      , "\"i\":" ++ show drawID
      , "\"s\":\"" ++ escapeNewlines (escapeQuotes $ T.unpack $ T.decodeUtf8 svg) ++ "\""
      , "\"x\":" ++ show (fst pos)
      , "\"y\":" ++ show (snd pos)
      , "\"c\":" ++ show scale
      , "\"w\":" ++ show width
      , "\"h\":" ++ show height ++ "}"
      ]
    packList xs = '[' : (intercalate "," $ map show xs) ++ "]"
    linearize [] = []
    linearize ((a,b):ps) = a:b:linearize ps

toPositions = withArray "Position list" $ \v -> do
  ps <- mapM parsePosition $ V.toList v 
  let ps' = groupPairs ps
  maybe (fail "Wrong number of points.") return ps'

parsePosition :: Value -> Parser Double
parsePosition = parseJSON

groupPairs [] = Just []
groupPairs (x:y:ps) = fmap ((x,y) :) $ groupPairs ps
groupPairs (_:[]) = Nothing


